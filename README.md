# Repository of *GroupeB06*
This repository is made of a Java project named "The More You Know", using the JavaFX framework for graphic interfaces.
The project consist of a trivial video game inspired from the physical tabletop game ["Tu Te Mets Combien ?"](http://tutemetscombien.fr/).

This repository is purely for scholarly purposes.

## Collaborators of this repository
- Durieux Valentin
- Ingrande Lorenz
- Joly Lucien

---



## User stories
| #     | User story                                                            | To do | In progress | Done  |
|-------|-----------------------------------------------------------------------|:-----:|:-----------:|:-----:|
| US-01 | As a user, I can pick a question                                      |       |             | **X** |
| US-02 | As a user, I can choose how many player are going to play in the game |       |             | **X** |
| US-03 | As a user, I can choose my username                                   |       |             | **X** |
| US-04 | As a user, I can pick the color of my pawn                            |       |             | **X** |
| US-05 | As a user, I can choose the level of difficulty of the question       |       |             | **X** |
| US-06 | As a user, I can see how much time I have to answer the question      |       |             | **X** |
| US-07 | As a user, I can see my current position                              |       |             | **X** |
| US-08 | As a user, I can log in to gain access to the Admin Portal            |       |             | **X** |
| US-09 | As an administrator, I can add, edit or delete questions              |       |             | **X** |
| US-10 | As a user, I can win the game                                         |       |             | **X** |
| US-11 | As a user, I can see where my pawn is positioned on the game board    |       |             | **X** |

# UML
## Class diagram of the model
![](readme_assets/class_diagram_model.png)