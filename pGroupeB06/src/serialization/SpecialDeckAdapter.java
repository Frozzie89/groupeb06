package serialization;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import model.SpecialCard;
import model.SpecialDeck;
import model.SpecialQuestion;
import model.Theme;

public class SpecialDeckAdapter implements JsonDeserializer<SpecialDeck> {

    @Override
    public SpecialDeck deserialize(JsonElement json, Type type, JsonDeserializationContext context)
            throws JsonParseException {
        JsonObject jsonSpecialDeck = json.getAsJsonObject();

        SpecialDeck deserializedSpecialDeck = new SpecialDeck(
                Theme.getThemeFromString(jsonSpecialDeck.get("theme").getAsString()));

        // List of Special cards
        JsonArray jsonspecialCardsArrays = jsonSpecialDeck.get("cards").getAsJsonArray();

        // Loop through cards, create them and add them to the deck
        for (JsonElement jsonSpecialCardArray : jsonspecialCardsArrays) {
            JsonObject jsonSpecialCard = jsonSpecialCardArray.getAsJsonObject();
            SpecialCard specialCard = new SpecialCard(jsonSpecialCard.get("author").getAsString(),
                    jsonSpecialCard.get("subject").getAsString(),
                    Theme.getThemeFromString(jsonSpecialCard.get("theme").getAsString()));

            // Loop through questions, create them and add them to current card
            JsonArray jsonSpecialQuestionsArrays = jsonSpecialCard.get("questions").getAsJsonArray();
            for (JsonElement jsonSpecialQuestionArray : jsonSpecialQuestionsArrays) {
                JsonObject jsonSpecialQuestion = jsonSpecialQuestionArray.getAsJsonObject();
                SpecialQuestion specialQuestion = new SpecialQuestion(jsonSpecialQuestion.get("author").getAsString(),
                        jsonSpecialQuestion.get("subject").getAsString(),
                        jsonSpecialQuestion.get("challenge").getAsString(),
                        Theme.getThemeFromString(jsonSpecialQuestion.get("theme").getAsString()),
                        jsonSpecialQuestion.get("answer").getAsBoolean());

                specialCard.addQuestion(specialQuestion);
            }
            deserializedSpecialDeck.addCard(specialCard);
        }

        return deserializedSpecialDeck;
    }
}
