package serialization;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.reflect.TypeToken;

import exception.ObjectCannotBeSerializedException;
import model.Admin;

import model.BasicDeck;
import model.SpecialDeck;
import model.Tile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonSerializable {
    public JsonSerializable() {
    }

    /**
     * Serialize a Java list into json
     *
     * @param objectList  List to serialize
     * @param objectClass Class of the list
     * @throws ObjectCannotBeSerializedException
     */
    public static <T> void writeJsonFile(List<T> objectList, Class<T> objectClass)
            throws ObjectCannotBeSerializedException {

        // First check if the class of the object can be turned into a json file
        if (FileName.doesFileNameExists(objectClass.getSimpleName()) != null) {
            // Then try to turn the object into a json file
            try {
                FileWriter myWriter = new FileWriter("json/" + objectClass.getSimpleName() + ".json");

                Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
                myWriter.write(gson.toJson(objectList));
                myWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new ObjectCannotBeSerializedException(objectClass);
        }
    }

    /**
     * De-serialize a json file content into a Java list
     * 
     * @param fileName concerned file name
     * @return a Java list
     * @throws FileNotFoundException
     */
    public static <T> List<T> readJsonFile(String fileName) throws FileNotFoundException {
        // Get the file
        File myObj = new File("json/" + fileName + ".json");
        Scanner myReader = new Scanner(myObj);
        String result = "";

        // Read the file and save it into "result" as String
        while (myReader.hasNextLine()) {
            result += myReader.nextLine() + "\n";
        }

        myReader.close();

        Gson gson = null;

        // Return Java lists depending of the file name
        if (fileName.equals("BasicDeck")) {
            gson = new GsonBuilder().registerTypeAdapter(BasicDeck.class, new BasicDeckAdapter()).disableHtmlEscaping()
                    .create();

            return gson.fromJson(result, new TypeToken<ArrayList<BasicDeck>>() {
            }.getType());

        } else if (fileName.equals("SpecialDeck")) {
            gson = new GsonBuilder().registerTypeAdapter(SpecialDeck.class, new SpecialDeckAdapter())
                    .disableHtmlEscaping().create();

            return gson.fromJson(result, new TypeToken<ArrayList<SpecialDeck>>() {
            }.getType());

        } else if (fileName.equals("Admin")) {
            gson = new GsonBuilder().disableHtmlEscaping().create();
            return gson.fromJson(result, new TypeToken<ArrayList<Admin>>() {
            }.getType());

        } else if (fileName.equals("Tile")) {
            gson = new GsonBuilder().disableHtmlEscaping().create();
            return gson.fromJson(result, new TypeToken<ArrayList<Tile>>() {
            }.getType());
        } else {
            throw new FileNotFoundException(fileName);
        }
    }
}
