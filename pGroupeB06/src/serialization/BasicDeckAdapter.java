package serialization;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import model.BasicCard;
import model.BasicDeck;
import model.BasicQuestion;
import model.Theme;

public class BasicDeckAdapter implements JsonDeserializer<BasicDeck> {

    @Override
    public BasicDeck deserialize(JsonElement json, Type type, JsonDeserializationContext context)
            throws JsonParseException {

        JsonObject jsonBasicDeck = json.getAsJsonObject();

        BasicDeck deserializedBasicDeck = new BasicDeck(
                Theme.getThemeFromString(jsonBasicDeck.get("theme").getAsString()));

        // List of Basic cards
        JsonArray jsonBasicCardsArrays = jsonBasicDeck.get("cards").getAsJsonArray();

        // Loop through cards, create them and add them to the deck
        for (JsonElement jsonBasicCardArray : jsonBasicCardsArrays) {
            JsonObject jsonBasicCard = jsonBasicCardArray.getAsJsonObject();
            BasicCard basicCard = new BasicCard(jsonBasicCard.get("author").getAsString(),
                    jsonBasicCard.get("subject").getAsString(),
                    Theme.getThemeFromString(jsonBasicCard.get("theme").getAsString()));

            // Loop through questions, create them and add them to current card
            JsonArray jsonBasicQuestionsArrays = jsonBasicCard.get("questions").getAsJsonArray();
            for (JsonElement jsonBasicQuestionArray : jsonBasicQuestionsArrays) {
                JsonObject jsonBasicQuestion = jsonBasicQuestionArray.getAsJsonObject();
                BasicQuestion basicQuestion = new BasicQuestion(jsonBasicQuestion.get("author").getAsString(),
                        jsonBasicQuestion.get("subject").getAsString(),
                        jsonBasicQuestion.get("challenge").getAsString(),
                        Theme.getThemeFromString(jsonBasicQuestion.get("theme").getAsString()),
                        jsonBasicQuestion.get("answer").getAsString());

                basicCard.addQuestion(basicQuestion);
            }
            deserializedBasicDeck.addCard(basicCard);
        }

        return deserializedBasicDeck;
    }

}
