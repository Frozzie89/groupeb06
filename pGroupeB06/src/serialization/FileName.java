package serialization;

public enum FileName {
    BASIC_DECK("BasicDeck"), SPECIAL_DECK("SpecialDeck"), ADMIN("Admin"), TILE("Tile");

    private String label;

    private FileName(String label) {
        this.label = label;
    }

    public String gelLabel() {
        return label;
    }

    /**
     * Finds the FileName enum matching its String value
     * 
     * @param fileNameStr FileName value as String
     * @return the matching FileName, else null
     */
    public static FileName doesFileNameExists(String fileNameStr) {
        for (FileName f : values()) {
            if (fileNameStr.equals(f.gelLabel()))
                return f;
        }
        return null;
    }
}
