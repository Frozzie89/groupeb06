package exception;

public class ObjectCannotBeSerializedException extends Exception {
    private static final long serialVersionUID = -4685849555028255330L;

    public <T> ObjectCannotBeSerializedException(Class<T> objectClass) {
        super("Objects from " + objectClass + " cannot be serialized");
    }
}
