package exception;

public class EmptyDeckException extends Exception {
    private static final long serialVersionUID = 1L;

    public EmptyDeckException() {
        super("The deck is empty or is missing");
    }

}
