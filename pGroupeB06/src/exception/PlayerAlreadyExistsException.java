package exception;

import model.Player;

public class PlayerAlreadyExistsException extends Exception {
    private static final long serialVersionUID = 1L;

    public PlayerAlreadyExistsException(Player player) {
        super("Player " + player.getUserName() + " already exists");
    }

}
