package model;

public class SpecialQuestion extends Question {
	private boolean answer;

	public SpecialQuestion(String author, String subject, String challenge, Theme theme, boolean answer) {
		super(author, subject, challenge, theme);
		this.answer = answer;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SpecialQuestion) {
			SpecialQuestion sq = (SpecialQuestion) obj;
			return super.equals(sq) && super.getChallenge().equals(sq.getChallenge());
		}

		return false;
	}

	@Override
	public Question clone() {
		return new SpecialQuestion(getAuthor(), getSubject(), getChallenge(), getTheme(), answer);
	}

	@Override
	public String toString() {
		return "SpecialQuestion [ " + super.toString() + " answer=" + answer + "]";
	}

	/* Getters ans setters */
	public boolean isAnswer() {
		return answer;
	}

	public void setAnswer(boolean answer) {
		this.answer = answer;
	}

}
