package model;

import java.io.FileNotFoundException;
import java.util.List;

import security.Encryption;
import serialization.JsonSerializable;

public class Admin {
    // password is crypted with a SHA256 key
    private String userName, password;

    public Admin(String userName, String password) {
        this.userName = Encryption.sha256Encryption(userName);
        this.password = Encryption.sha256Encryption(password);
    }

    /**
     * Checks if args are similar to administrator info in Admin.json, if so,
     * returns this Admin, else return null
     * 
     * @param userName Admin's plain username
     * @param password Admin's plain password
     * @return an Admin object or null
     */
    public static Admin findAdminJson(String userName, String password) {
        List<Admin> adminList = null;

        try {
            adminList = JsonSerializable.readJsonFile("Admin");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Cypher args
        String encryptedUserName = Encryption.sha256Encryption(userName);
        String encryptedPassword = Encryption.sha256Encryption(password);

        // Loop through admins and check if cyphered args match an admin
        for (Admin admin : adminList) {
            if (admin.getUserName().equals(encryptedUserName) && admin.getPassword().equals(encryptedPassword))
                return admin;
        }

        return null;
    }

    public Admin clone() {
        return new Admin(userName, password);
    }

    @Override
    public String toString() {
        return "Admin [userName=" + userName + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Admin other = (Admin) obj;
        if (userName == null) {
            if (other.userName != null)
                return false;
        } else if (!userName.equals(other.userName))
            return false;
        return true;
    }

    /* getters and setters */
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = Encryption.sha256Encryption(userName);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = Encryption.sha256Encryption(password);
    }

}
