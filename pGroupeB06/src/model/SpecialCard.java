package model;

import java.util.List;

public class SpecialCard extends Card {

    public SpecialCard(String author, String subject, Theme theme) {
        super(author, subject, theme);
    }

    public SpecialCard(String author, String subject, Theme theme, List<Question> questions) {
        super(author, subject, theme, questions);
    }

    @Override
    public Card clone() {
        return new SpecialCard(getAuthor(), getSubject(), getTheme(), getQuestions());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SpecialCard)
            return super.equals((SpecialCard) obj);

        return false;
    }

    public Question findQuestion(Question question) {
        if (question instanceof SpecialQuestion)
            return super.findQuestion((SpecialQuestion) question);

        return null;
    }

    public boolean addQuestion(Question question) {
        if (question instanceof SpecialQuestion)
            return super.addQuestion((SpecialQuestion) question);

        return false;
    }

    public boolean removeQuestion(Question question) {
        if (question instanceof SpecialQuestion)
            return super.removeQuestion((SpecialQuestion) question);

        return false;
    }

    public boolean updateQuestion(Question oldQuestion, Question newQuestion) {
        if (oldQuestion instanceof SpecialQuestion && newQuestion instanceof SpecialQuestion)
            return super.updateQuestion((SpecialQuestion) oldQuestion, (SpecialQuestion) newQuestion);

        return false;
    }

    @Override
    public String toString() {
        return "SuperCard [" + super.toString() + "]";
    }
}
