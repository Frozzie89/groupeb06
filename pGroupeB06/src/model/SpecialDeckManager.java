package model;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import exception.ObjectCannotBeSerializedException;
import serialization.JsonSerializable;

public class SpecialDeckManager implements DeckManager {
    private List<SpecialDeck> specialDeckList;

    public SpecialDeckManager() {
        // deck is initialized from the static method "readJsonFile" from the
        // "JsonSerializable" class
        try {
            specialDeckList = JsonSerializable.readJsonFile("SpecialDeck");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // If the json file was empty, initialize it as a new ArrayList
        if (specialDeckList == null)
            specialDeckList = new ArrayList<>();

    }

    public SpecialDeckManager(List<SpecialDeck> specialDeckList) {
        this.specialDeckList = new ArrayList<>();

        for (SpecialDeck specialDeck : specialDeckList) {
            this.specialDeckList.add((SpecialDeck) specialDeck.clone());
        }
    }

    @Override
    public boolean areDecksCompliant() {

        for (Theme theme : Theme.values()) {
            SpecialDeck specialDeck = findDeck(theme);

            if (specialDeck == null || specialDeck.getCards().isEmpty())
                return false;
        }

        return true;
    }

    @Override
    public SpecialDeck findDeck(Theme theme) {
        // Loop through the list of decks
        for (SpecialDeck specialDeck : specialDeckList) {
            // If the theme in param correspond to the theme of the deck, return it
            if (specialDeck.getTheme().equals(theme))
                return (SpecialDeck) specialDeck;
        }

        // If no deck could be found, return null
        return null;

    }

    @Override
    public SpecialCard findCard(Question question) {
        if (question instanceof SpecialQuestion) {
            SpecialQuestion specialQuestion = (SpecialQuestion) question;

            SpecialDeck specialDeck = (SpecialDeck) findDeck(specialQuestion.getTheme());

            for (Card card : specialDeck.getCards()) {
                if (card.getSubject().equals(specialQuestion.getSubject()))
                    return (SpecialCard) card.clone();
            }
        }

        return null;
    }

    @Override
    public boolean addCard(Card card) {
        // Check if the card in param is an instance of SpecialCard
        if (card instanceof SpecialCard) {
            // If so, cast it to SpecialCard for security
            SpecialCard specialCard = (SpecialCard) card;

            // Find the deck related to the basic card
            Deck specialDeck = findDeck(specialCard.getTheme());

            // If a theme was found, append the basic card to it, else create a new deck an
            // append the basic card to this new deck
            if (specialDeck != null)
                return specialDeck.addCard(specialCard);
            else {
                SpecialDeck newSpecialDeck = new SpecialDeck(specialCard.getTheme());
                newSpecialDeck.addCard(specialCard);
                return specialDeckList.add(newSpecialDeck);
            }

        }

        // If the card in param is not an instance of SpecialCard, return false
        return false;
    }

    @Override
    public boolean removeCard(Card card) {
        // Check if the card in param is an instance of SpecialCard
        if (card instanceof SpecialCard) {
            // If so, cast it to SpecialCard for security
            SpecialCard specialCard = (SpecialCard) card;

            // Find the deck related to the basic card
            Deck specialDeck = findDeck(specialCard.getTheme());

            // If a deck could be found, try to remove the card and
            // return true if so, else false.
            if (specialDeck != null)
                return specialDeck.removeCard(specialCard);
        }

        // If the card in param is not an instance of SpecialCard or if no deck could be
        // found, return false
        return false;
    }

    @Override
    public boolean updateCard(Card oldCard, Card newCard) {
        // Check if the cards in param are an instance of SpecialCard
        if (oldCard instanceof SpecialCard && newCard instanceof SpecialCard) {
            // If so, cast them to SpecialCard for security
            SpecialCard oldSpecialCard = (SpecialCard) oldCard;
            SpecialCard newSpecialCard = (SpecialCard) newCard;

            // Find the deck related to the old basic card
            Deck speciaDeck = findDeck(oldSpecialCard.getTheme());

            // If a deck could be found, try to update the card and
            // return true if so, else false.
            if (speciaDeck != null)
                return speciaDeck.updateCard(oldSpecialCard, newSpecialCard);
        }

        // If the card in param is not an instance of SpecialCard or if no deck could be
        // found, return false
        return false;
    }

    @Override
    public void clearCardList() {
        specialDeckList.clear();
    }

    @Override
    public void writeDeckToJson() {
        // Calls the static method "writeJsonFile" from the "JsonSerializable" class,
        // add the deck list and its class in the method param
        try {
            JsonSerializable.writeJsonFile(specialDeckList, SpecialDeck.class);
        } catch (ObjectCannotBeSerializedException e) {
            e.printStackTrace();
        }
    }

    /* GETTERS AND SETTERS */
    public List<Deck> getDeckList() {
        // Clone the deck list and return it
        List<Deck> clonedSpecialDeckList = new ArrayList<>();

        for (SpecialDeck specialDeck : specialDeckList) {
            clonedSpecialDeckList.add((SpecialDeck) specialDeck.clone());
        }

        return clonedSpecialDeckList;
    }

    public void setDeckList(List<Deck> specialDeckList) {
        // Clear the list first
        this.specialDeckList.clear();

        // Clone the deck in param and and append it to the deck in attribute
        for (Deck specialDeck : specialDeckList) {
            this.specialDeckList.add((SpecialDeck) specialDeck.clone());
        }
    }

}
