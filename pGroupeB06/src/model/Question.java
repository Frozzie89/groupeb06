package model;

public abstract class Question {
    private String author, subject, challenge;
    private Theme theme;

    public Question(String author, String subject, String challenge, Theme theme) {
        this.author = author;
        this.subject = subject;
        this.challenge = challenge;
        this.theme = theme;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        result = prime * result + ((theme == null) ? 0 : theme.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Question other = (Question) obj;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        if (theme != other.theme)
            return false;
        return true;
    }

    public abstract Question clone();

    @Override
    public String toString() {
        return "author=" + author + ", challenge=" + challenge + ", subject=" + subject + ", theme=" + theme + ", ";
    }

    /* Getters and setters */
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

}
