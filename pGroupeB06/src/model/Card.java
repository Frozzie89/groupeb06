package model;

import java.util.ArrayList;
import java.util.List;

public abstract class Card {
    private String author, subject;
    private Theme theme;
    private List<Question> questions;

    public Card(String author, String subject, Theme theme) {
        this.author = author;
        this.subject = subject;
        this.theme = theme;
        this.questions = new ArrayList<>();
    }

    public Card(String author, String subject, Theme theme, List<Question> questions) {
        this.author = author;
        this.subject = subject;
        this.theme = theme;
        this.questions = new ArrayList<>();
        setQuestions(questions);
    }

    public abstract Card clone();

    /**
     * Check if a question exists in the card, if so return it, else return null
     * 
     * @param question question to find
     * @return the found Question or null
     */
    public Question findQuestion(Question question) {
        int indexQuestion = questions.indexOf(question);
        if (indexQuestion >= 0)
            return questions.get(indexQuestion).clone();

        return null;
    }

    /**
     * Adds a question into the card
     * 
     * @param question question to add
     * @return true if the question was added, else false
     */
    public boolean addQuestion(Question question) {
        if (!questions.contains(question) && question.getTheme().equals(theme))
            return questions.add(question.clone());

        return false;
    }

    /**
     * Removes a question from the card
     * 
     * @param question question to remove
     * @return true if the card was removed, else false
     */
    public boolean removeQuestion(Question question) {
        if (questions.contains(question))
            return questions.remove(question);

        return false;
    }

    /**
     * Updates a question from the card with another question
     * 
     * @param oldQuestion question to be replaced, contained in the card
     * @param newQuestion question that will replace "oldQuestion"
     * @return true if the question could be replaced, else false
     */
    public boolean updateQuestion(Question oldQuestion, Question newQuestion) {
        if (questions.contains(oldQuestion) && newQuestion.getTheme().equals(theme)) {
            questions.set(questions.indexOf(oldQuestion), newQuestion.clone());
            return true;
        }

        return false;
    }

    @Override

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        result = prime * result + ((theme == null) ? 0 : theme.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Card other = (Card) obj;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        if (theme != other.theme)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "author=" + author + ", questions=" + questions + ", subject=" + subject + ", theme=" + theme + ", ";
    }

    /* Getters and setters */
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public List<Question> getQuestions() {
        List<Question> clonedQuestions = new ArrayList<>();

        for (Question q : questions) {
            clonedQuestions.add(q.clone());
        }

        return clonedQuestions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions.clear();

        for (Question q : questions) {
            this.questions.add(q.clone());
        }
    }

}
