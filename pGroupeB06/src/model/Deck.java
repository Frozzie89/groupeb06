package model;

import java.util.ArrayList;
import java.util.List;

public abstract class Deck {
	private List<Card> cards;
	private Theme theme;

	public Deck(Theme theme) {
		this.theme = theme;
		cards = new ArrayList<>();
	}

	public Deck(Theme theme, List<Card> cards) {
		this.theme = theme;
		cards = new ArrayList<>();
		setCards(cards);
	}

	public abstract Deck clone();

	/**
	 * Creates a card
	 * 
	 * @param author  card author
	 * @param subject card subject
	 * @param theme   card theme
	 * @return a card that contains the inserted args as attributes
	 */
	public abstract Card createCard(String author, String subject, Theme theme);

	/**
	 * Creates a card
	 * 
	 * @param author    card author
	 * @param subject   card subject
	 * @param theme     card theme
	 * @param questions card questions
	 * @return a card that contains the inserted args as attributes
	 */
	public abstract Card createCard(String author, String subject, Theme theme, List<Question> questions);

	/**
	 * Check if a card exists in the deck, if so return it, else return null
	 * 
	 * @param card card to find
	 * @return the found Card or null
	 */
	public Card findCard(Card card) {
		int indexCard = cards.indexOf(card);
		if (indexCard >= 0)
			return cards.get(indexCard).clone();

		return null;
	}

	/**
	 * Adds a card into the deck
	 * 
	 * @param author  card author
	 * @param subject card subject
	 * @param theme   card theme
	 * @return true if the card was added, else false
	 */
	public boolean addCard(String author, String subject, Theme theme) {
		Card card = createCard(author, subject, theme);
		if (!cards.contains(card) && card.getTheme().equals(theme))
			return cards.add(card);

		return false;
	}

	/**
	 * Adds a card into the deck
	 * 
	 * @param card card to add
	 * @return true if the card was added, else false
	 */
	public boolean addCard(Card card) {
		if (!cards.contains(card) && card.getTheme().equals(theme))
			return cards.add(card.clone());

		return false;
	}

	/**
	 * Adds a card into the deck
	 * 
	 * @param author    card author
	 * @param subject   card subject
	 * @param theme     card theme
	 * @param questions card questions
	 * @return true if the card was added, else false
	 */
	public boolean addCard(String author, String subject, Theme theme, List<Question> questions) {
		Card card = createCard(author, subject, theme, questions);
		if (!cards.contains(card) && card.getTheme().equals(theme))
			return cards.add(card);

		return false;
	}

	/**
	 * Removes a card from the deck
	 * 
	 * @param card card to remove
	 * @return true if the card was removed, else false
	 */
	public boolean removeCard(Card card) {
		if (cards.contains(card))
			return cards.remove(card);

		return false;
	}

	/**
	 * Updates a card from the deck with another card
	 * 
	 * @param oldCard card to be replaced, contained in the deck
	 * @param newCard card that will replace "oldCard"
	 * @return true if the card could be replaced, else false
	 */
	public boolean updateCard(Card oldCard, Card newCard) {
		if (cards.contains(oldCard) && newCard.getTheme().equals(theme)) {
			cards.set(cards.indexOf(oldCard), newCard.clone());
			return true;
		}

		return false;
	}

	@Override
	public String toString() {
		return "cards=" + cards + ", theme=" + theme + ", ";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((theme == null) ? 0 : theme.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deck other = (Deck) obj;
		if (theme != other.theme)
			return false;
		return true;
	}

	/* Getters and setters */
	public List<Card> getCards() {
		List<Card> clonedCards = new ArrayList<>();

		for (Card c : cards) {
			clonedCards.add(c.clone());
		}

		return clonedCards;
	}

	public void setCards(List<Card> cards) {
		this.cards.clear();

		for (Card c : cards) {
			this.cards.add(c.clone());
		}
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}

}