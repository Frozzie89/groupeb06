package model;

public class BasicQuestion extends Question {
    private String answer;

    public BasicQuestion(String author, String subject, String challenge, Theme theme, String answer) {
        super(author, subject, challenge, theme);
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "BasicQuestion [ " + super.toString() + " answer=" + answer + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((answer == null) ? 0 : answer.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        BasicQuestion other = (BasicQuestion) obj;
        if (answer == null) {
            if (other.answer != null)
                return false;
        } else if (!answer.equals(other.answer))
            return false;
        return true;
    }

    @Override
    public Question clone() {
        return new BasicQuestion(getAuthor(), getSubject(), getChallenge(), getTheme(), answer);
    }

    /* Getters and setters */
    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
