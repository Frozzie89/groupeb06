package model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javafx.scene.paint.Color;

public enum Theme {
    IMPROBABLE("Improbable", Color.web("#b973fa")), PLEASURE("Pleasure", Color.web("#FFA500")),
    INFORMATICS("Informatics", Color.web("#5493f7")), SCHOOL("School", Color.web("#46c77b"));

    private String label;
    private Color color;

    private Theme(String label, Color c) {
        this.label = label;
        this.color = c;
    }

    public String getLabel() {
        return label;
    }

    public Color getColor() {
        return color;
    }

    /**
     * Finds the Theme enum matching its String value
     * 
     * @param themeStr Theme value as String
     * @return the matching theme, else null
     */
    public static Theme getThemeFromString(String themeStr) {
        for (Theme t : values()) {
            if (themeStr.equalsIgnoreCase(t.getLabel()))
                return t;
        }
        return null;
    }

    /**
     * Returns a random Theme enum
     * 
     * @return a Theme enum
     */
    public static Theme getRandomTheme() {
        final List<Theme> values = Collections.unmodifiableList(Arrays.asList(values()));
        return values.get(new Random().nextInt(values.size()));
    }
}
