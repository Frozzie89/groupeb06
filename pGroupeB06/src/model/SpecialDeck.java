package model;

import java.util.List;

public class SpecialDeck extends Deck {

    public SpecialDeck(Theme theme) {
        super(theme);
    }

    public SpecialDeck(Theme theme, List<Card> cards) {
        super(theme);
        setCards(cards);
    }

    public Card findCard(Card card) {
        if (card instanceof SpecialCard)
            return super.findCard((SpecialCard) card);

        return null;
    }
    
    public boolean addCard(Card card) {
        if (card instanceof SpecialCard && card.getTheme().equals(getTheme()))
            return super.addCard((SpecialCard) card);

        return false;
    }

    public boolean removeCard(Card card) {
        if (card instanceof SpecialCard)
            return super.removeCard((SpecialCard) card);

        return false;
    }

    public boolean updateCard(Card oldCard, Card newCard) {
        if (oldCard instanceof SpecialCard && newCard instanceof SpecialCard) {
            return super.updateCard((SpecialCard) oldCard, (SpecialCard) newCard);
        }

        return false;
    }

    @Override
    public Deck clone() {
        return new SpecialDeck(getTheme(), getCards());
    }

    @Override
    public String toString() {
        return "SpecialDeck [ " + super.toString() + " ]";
    }

	@Override
	public Card createCard(String author, String subject, Theme theme) {
		return new SpecialCard(author,subject,theme);
	}

	@Override
	public Card createCard(String author, String subject, Theme theme, List<Question> questions) {
		return new SpecialCard(author,subject,theme,questions);
	}

}
