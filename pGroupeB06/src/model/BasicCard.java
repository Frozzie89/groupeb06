package model;

import java.util.List;

public class BasicCard extends Card {

	public BasicCard(String author, String subject, Theme theme) {
		super(author, subject, theme);
	}

	public BasicCard(String author, String subject, Theme theme, List<Question> questions) {
		super(author, subject, theme, questions);
	}

	@Override
	public Card clone() {
		return new BasicCard(getAuthor(), getSubject(), getTheme(), getQuestions());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BasicCard)
			return super.equals((BasicCard) obj);

		return false;
	}

	public Question findQuestion(Question question) {
		if (question instanceof BasicQuestion)
			return super.findQuestion((BasicQuestion) question);

		return null;
	}

	public boolean addQuestion(Question question) {
		if (question instanceof BasicQuestion)
			return super.addQuestion((BasicQuestion) question);

		return false;
	}

	public boolean removeQuestion(Question question) {
		if (question instanceof BasicQuestion)
			return super.removeQuestion((BasicQuestion) question);

		return false;
	}

	public boolean updateQuestion(Question oldQuestion, Question newQuestion) {
		if (oldQuestion instanceof BasicQuestion && newQuestion instanceof BasicQuestion)
			return super.updateQuestion((BasicQuestion) oldQuestion, (BasicQuestion) newQuestion);

		return false;
	}

	@Override
	public String toString() {
		return "BasicCard [" + super.toString() + "]";
	}

	@Override
	public void setQuestions(List<Question> questions) {
		for (Question question : questions) {
			addQuestion(question);
		}
	}

}
