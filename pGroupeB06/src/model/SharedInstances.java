package model;

public interface SharedInstances {
    // Deck managers
    public DeckManager basicDeckManager = new BasicDeckManager();
    public DeckManager specialDeckManager = new SpecialDeckManager();

    // Deck piles
    public DeckPile basicDeckPile = new BasicDeckPile(basicDeckManager);
    public DeckPile specialDeckPile = new SpecialDeckPile(specialDeckManager);

    // Game manager
    public GameManager gameManager = new GameManager();
}
