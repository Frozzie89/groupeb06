package model;

import java.util.Collections;
import java.util.List;

import exception.EmptyDeckException;

public class BasicDeckPile extends DeckPile {

    public BasicDeckPile(DeckManager deckManager) {
        try {
            if (deckManager instanceof BasicDeckManager) {
                BasicDeckManager bdm = (BasicDeckManager) deckManager;
                setDeckManager(bdm);
            } else
                return;

            newGame();
        } catch (EmptyDeckException e) {
            return;
        }
    }

    @Override
    public void newGame() throws EmptyDeckException {
        // First check if decks are compliant before setting up the decks
        if (!getDeckManager().areDecksCompliant())
            throw new EmptyDeckException();

        // set deck lists from SpecialDeckManager into deck pools
        setDeckPools(getDeckManager().getDeckList());

        // Shuffle the deck pools for randomness
        for (Deck deck : getDeckPools()) {
            refillDeck(deck.getTheme());
        }
    }

    @Override
    public void refillDeck(Theme theme) {
        // Get the cards from the matching theme
        Deck deck = getDeckManager().findDeck(theme).clone();
        List<Card> cards = deck.getCards();

        // Shuffle the cards
        Collections.shuffle(cards);

        // Update the new cards
        deck.setCards(cards);
        updateDeck(deck);

    }

}
