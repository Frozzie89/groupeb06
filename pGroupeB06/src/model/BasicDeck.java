package model;

import java.util.List;

public class BasicDeck extends Deck {

    public BasicDeck(Theme theme) {
        super(theme);
    }

    public BasicDeck(Theme theme, List<Card> cards) {
        super(theme);
        setCards(cards);
    }

    public Card findCard(Card card) {
        if (card instanceof BasicCard)
            return super.findCard((BasicCard) card);

        return null;
    }
    
    public boolean addCard(Card card) {
        if (card instanceof BasicCard && card.getTheme().equals(getTheme()))
            return super.addCard((BasicCard) card);

        return false;
    }

    public boolean removeCard(Card card) {
        if (card instanceof BasicCard)
            return super.removeCard((BasicCard) card);

        return false;
    }

    public boolean updateCard(Card oldCard, Card newCard) {
        if (oldCard instanceof BasicCard && newCard instanceof BasicCard) {
            return super.updateCard((BasicCard) oldCard, (BasicCard) newCard);
        }

        return false;
    }

    @Override
    public Deck clone() {
        return new BasicDeck(getTheme(), getCards());
    }

    @Override
    public String toString() {
        return "BasicDeck [ " + super.toString() + " ]";
    }

	@Override
	public Card createCard(String author, String subject, Theme theme) {
		return new BasicCard(author, subject, theme);
	}

	@Override
	public Card createCard(String author, String subject, Theme theme, List<Question> questions) {
		return new BasicCard(author, subject, theme, questions);
	}

}
