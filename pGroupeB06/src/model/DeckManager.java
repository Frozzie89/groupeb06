package model;

import java.util.List;

public interface DeckManager {

    /**
     * 
     * Checks if each theme has its one deck and if each deck has at least one card
     * 
     * @return true if each theme has its one deck and if each deck has at least one
     *         card, else false
     */
    boolean areDecksCompliant();

    /**
     * finds and return a deck related to the theme in param.
     * 
     * @param theme theme of a deck.
     * @return found Deck or null.
     */

    Deck findDeck(Theme theme);

    /**
     * finds and return a card related to the question in param.
     * 
     * @param question question that is contained in a card that is part of the
     *                 decks.
     * @return found Card or null.
     */
    Card findCard(Question question);

    /**
     * add card to the corresponding deck.
     * 
     * @param card card to add.
     * @return true if the card is added, false if not.
     */
    boolean addCard(Card card);

    /**
     * remove a card from the corresponding deck.
     * 
     * @param card card to remove.
     * @return true if the card is found and removed, false if not.
     */
    boolean removeCard(Card card);

    /**
     * update a card from the corresponding deck.
     * 
     * @param oldCard card to replace, will be erased. Must exist in the card list.
     * @param newCard card that replaces the oldCard. Must not exist in the card
     *                list.
     * @return true if the replacement is done, false if not
     */
    boolean updateCard(Card oldCard, Card newCard);

    /**
     * clear the deck list
     */
    void clearCardList();

    /**
     * update the deck json file
     */
    void writeDeckToJson();

    /**
     * @return a copy a the deck list
     */
    List<Deck> getDeckList();

    /**
     * update the deck list with a copy of the deck list in param
     * 
     * @param deckList a list of decks
     */
    void setDeckList(List<Deck> deckList);

}
