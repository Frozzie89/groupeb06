package model;

public interface DrawCardStrategy {

    /**
     * Method that randomly draws a card and returns it.
     * 
     * @param theme - The theme of the card to draw
     * @return a randomly generated card
     */
    public Card drawCard(Theme theme);

}
