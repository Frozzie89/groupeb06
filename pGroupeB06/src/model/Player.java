package model;

import javafx.scene.paint.Color;

public class Player {
    private String userName;
    private int position;
    private Color color;

    public Player(String userName, Color color) {
        this.userName = userName;
        this.position = 0;
        this.color = color;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Player other = (Player) obj;
        if (userName == null) {
            if (other.userName != null)
                return false;
        } else if (!userName.equals(other.userName))
            return false;
        return true;
    }

    public Player clone() {
        Player clonedPlayer = new Player(userName, color);
        clonedPlayer.setPosition(position);
        return clonedPlayer;
    }

    @Override
    public String toString() {
        return "Player [color=" + color + ", position=" + position + ", userName=" + userName + "]";
    }

    /* Getters and setters */
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
