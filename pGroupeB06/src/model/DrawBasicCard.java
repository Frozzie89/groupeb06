package model;

public class DrawBasicCard implements DrawCardStrategy {

	public BasicCard drawCard(Theme theme) {
		return (BasicCard) SharedInstances.basicDeckPile.drawCard(theme);
	}

}
