package model;

public class DrawSpecialCard implements DrawCardStrategy {

    public SpecialCard drawCard(Theme theme) {
    	return (SpecialCard) SharedInstances.specialDeckPile.drawCard(theme);
    }

}
