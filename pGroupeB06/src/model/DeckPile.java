package model;

import java.util.List;

import exception.EmptyDeckException;

public abstract class DeckPile {
	private List<Deck> deckPools;
	private DeckManager deckManager;

	/**
	 * Refills a deck with shuffled cards
	 * 
	 * @param theme theme of deck to refill
	 */
	public abstract void refillDeck(Theme theme);

	/**
	 * Sets and shuffles all decks
	 * 
	 * @throws EmptyDeckException
	 */
	public abstract void newGame() throws EmptyDeckException;

	/**
	 * Draws the card on top of the deck
	 * 
	 * @param theme theme of the deck to draw
	 * @return a card from the deck
	 */
	public Card drawCard(Theme theme) {
		Deck deck = findDeck(theme);
		Card card = deck.getCards().get(deck.getCards().size() - 1);

		removeCard(card);

		if (deck.getCards().size() < 1)
			refillDeck(card.getTheme());

		return card;
	}

	/**
	 * Finds a deck from the deck piles according to its matching theme
	 * 
	 * @param theme theme of the deck
	 * @return a deck
	 */
	public Deck findDeck(Theme theme) {
		for (Deck deck : deckPools) {
			if (deck.getTheme().equals(theme))
				return deck;
		}

		return null;
	}

	/**
	 * Set a deck in the deck pile. The new pile will replace its matching theme
	 * from the deck pile
	 * 
	 * @param deck deck to set
	 * @return true if the deck could be set, else false
	 */
	public boolean updateDeck(Deck deck) {
		int index = getDeckPools().indexOf(findDeck(deck.getTheme()));

		if (index >= 0) {
			getDeckPools().set(index, deck);
			return true;
		}

		return false;
	}

	/**
	 * Removes a card from the deck matching its theme.
	 * 
	 * @param card card to remove
	 * @return true if the card could be removed form the deck, else false
	 */
	private boolean removeCard(Card card) {
		Deck deck = findDeck(card.getTheme());

		if (deck != null) {
			return deck.removeCard(card);
		}

		return false;
	}

	@Override
	public String toString() {
		return "DeckPile [deckPools=" + deckPools + "]";
	}

	/* GETTERS AND SETTERS */
	public List<Deck> getDeckPools() {
		return deckPools;
	}

	public void setDeckPools(List<Deck> deckPools) {
		this.deckPools = deckPools;
	}

	public DeckManager getDeckManager() {
		return deckManager;
	}

	public void setDeckManager(DeckManager deckManager) {
		this.deckManager = deckManager;
	}

}
