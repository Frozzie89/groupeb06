package model;

public class Tile {

    public enum TileType {
        BASIC, SPECIAL;
    }

    private TileType tileType;
    private Theme tileTheme;

    public Tile(TileType tileType, Theme tileTheme) {
        this.tileType = tileType;

        if (tileType.equals(TileType.SPECIAL))
            // Special TileType objects have a random theme
            this.tileTheme = Theme.getRandomTheme();
        else
            this.tileTheme = tileTheme;
    }

    public Tile(TileType tileType) {
        this.tileType = tileType;
        this.tileTheme = Theme.getRandomTheme();
    }

    @Override
    public String toString() {
        return "Tile [tileTheme=" + tileTheme + ", tileType=" + tileType + "]";
    }

    /* GETTERS AND SETTERS */
    public TileType getTileType() {
        return tileType;
    }

    public void setTileType(TileType tileType) {
        this.tileType = tileType;
    }

    public Theme getTileTheme() {
        return tileTheme;
    }

    public void setTileTheme(Theme tileTheme) {
        this.tileTheme = tileTheme;
    }

}
