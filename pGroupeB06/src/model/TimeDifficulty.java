package model;

public enum TimeDifficulty {
    EASY("Easy", 30), MEDIUM("Medium", 20), HARD("Hard", 15);

    private int seconds;
    private String label;

    private TimeDifficulty(String label, int seconds) {
        this.label = label;
        this.seconds = seconds;
    }

    public int getSeconds() {
        return seconds;
    }

    public String getLabel() {
        return label;
    }

    /**
     * Finds the TimeDifficulty enum matching its String value
     * 
     * @param timeDiff TimeDifficulty value as String
     * @return the matching TimeDifficulty, else null
     */
    public static TimeDifficulty getTimeDifficultyFromString(String timeDiff) {
        for (TimeDifficulty td : values()) {
            if (timeDiff.equalsIgnoreCase(td.getLabel()))
                return td;
        }
        return null;
    }

}
