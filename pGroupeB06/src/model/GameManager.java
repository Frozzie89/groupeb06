package model;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import exception.EmptyDeckException;
import exception.PlayerAlreadyExistsException;
import serialization.JsonSerializable;

public class GameManager {
    private List<Player> playerList;
    private List<Tile> tiles;
    private DrawCardStrategy dcs;
    private Player playerWinner;
    private int turn;
    private TimeDifficulty timeDifficulty;

    public GameManager() {
        playerList = new ArrayList<>();
        turn = 0;
        playerWinner = null;
        timeDifficulty = null;

        // Tiles are initialized from the json file Tile.json
        try {
            tiles = JsonSerializable.readJsonFile("Tile");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * resets the deck piles and the player list in order to play a other game
     */
    public void resetGame() {
        playerList.clear();
        turn = 0;
        playerWinner = null;
        timeDifficulty = null;

        try {
            SharedInstances.basicDeckPile.newGame();
            SharedInstances.specialDeckPile.newGame();
        } catch (EmptyDeckException e) {
            e.printStackTrace();
        }

    }

    /**
     * increment turn
     */
    public void nextTurn() {
        turn = turn >= playerList.size() - 1 ? 0 : turn + 1;
    }

    /**
     * 
     * @return player according to the turn
     */
    public Player getPlayerTurn() {
        return playerList.get(turn);
    }

    /**
     * 
     * @return tile where the playing player is positionnned
     */
    public Tile getPlayerTile() {
        return tiles.get(getPlayerTurn().getPosition());
    }

    /**
     * check if the player's input and the question's answer are identical. They are
     * considered identical if both strings are the same without checking the case
     * sensitivity, or if their distance (according to the Levenshtein algorithm)
     * are 2 or less, or if both are numbers and are identical
     * 
     * @param entry  player's input
     * @param answer question's answer
     * @return true if the two strings are considered identical, else false
     */
    public static boolean checkAnswer(String entry, String answer) {
        try {
            // First check if the answer is empty, if so, considered false
            if (answer.isEmpty())
                return false;

            // Then check if the params can be casted into doubles
            Double doubleEntry = Double.valueOf(entry);
            Double doubleAnswer = Double.valueOf(answer);

            // If so, check if they are equals
            return doubleAnswer.equals(doubleEntry);

        } catch (NumberFormatException e) {
            // If the params couldn't be casted, then check if they strings are equals or if
            // they have a distance lower than 2
            return (entry.equalsIgnoreCase(answer) || Levenshtein.getLevenshteinDistance(entry, answer) <= 2);
        }

    }

    /**
     * Increment playing player's position. If his position gets over the number of
     * tiles, this player becomes the winner
     * 
     * @param addPosition position to increment
     * @return true if the player's position is over the number of tiles, else false
     */
    public boolean movePlayerPosition(int addPosition) {

        // Get playing player and increment his position
        Player player = getPlayerTurn();
        player.setPosition(player.getPosition() + addPosition);

        // Check if his position is over the number of tiles
        if (player.getPosition() >= tiles.size()) {
            // if so, this player is the winner and return true
            playerWinner = player.clone();
            return true;
        }

        // Update player data
        playerList.set(getTurn(), player);
        return false;
    }

    /**
     * change draw card stategy
     * 
     * @param dcs draw card strategy
     */
    public void setStrategy(DrawCardStrategy dcs) {
        this.dcs = dcs;
    }

    /**
     * Draws a card from a card pile and returns it. Uses current DrawCardStrategy's
     * strategy
     * 
     * @param theme Theme
     * @return a Card
     */
    public Card drawCard(Theme theme) {
        return dcs.drawCard(theme);
    }

    /**
     * Adds a player in the player list
     * 
     * @param player Player to add
     * @throws PlayerAlreadyExistsException if the player already exists
     */
    public void addPlayer(Player player) throws PlayerAlreadyExistsException {

        // First check if the player in param already exist, if not, add it.
        if (!playerList.contains(player))
            playerList.add(player);
        else
            // If it already exists, throws an exception
            throw new PlayerAlreadyExistsException(player);
    }

    /* GETTERS AND SETTERS */
    public List<Player> getPlayerList() {
        List<Player> newPlayerList = new ArrayList<>();

        for (Player p : playerList) {
            newPlayerList.add(p.clone());
        }

        return newPlayerList;
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public void setPlayerWinner(Player pl) {
        this.playerWinner = pl.clone();
    }

    public Player getPlayerWinner() {
        return playerWinner;
    }

    public TimeDifficulty getTimeDifficulty() {
        return timeDifficulty;
    }

    public void setTimeDifficulty(TimeDifficulty timeDifficulty) {
        this.timeDifficulty = timeDifficulty;
    }

}
