package model;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import exception.ObjectCannotBeSerializedException;
import serialization.JsonSerializable;

public class BasicDeckManager implements DeckManager {
    private List<BasicDeck> basicDeckList;

    public BasicDeckManager() {
        // deck is initialized from the static method "readJsonFile" from the
        // "JsonSerializable" class
        try {
            basicDeckList = JsonSerializable.readJsonFile("BasicDeck");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // If the json file was empty, initialize it as a new ArrayList
        if (basicDeckList == null)
            basicDeckList = new ArrayList<>();
    }

    public BasicDeckManager(List<BasicDeck> basicDeckList) {
        this.basicDeckList = new ArrayList<>();

        for (BasicDeck basicDeck : basicDeckList) {
            this.basicDeckList.add((BasicDeck) basicDeck.clone());
        }
    }

    @Override
    public boolean areDecksCompliant() {

        for (Theme theme : Theme.values()) {
            BasicDeck basicDeck = findDeck(theme);

            if (basicDeck == null || basicDeck.getCards().isEmpty())
                return false;
        }

        return true;
    }

    @Override
    public BasicDeck findDeck(Theme theme) {
        // Loop through the list of decks
        for (BasicDeck basicDeck : basicDeckList) {
            // If the theme in param correspond to the theme of the deck, return it
            if (basicDeck.getTheme().equals(theme))
                return (BasicDeck) basicDeck;
        }

        // If no deck could be found, return null
        return null;
    }


    @Override
    public BasicCard findCard(Question question) {
        if (question instanceof BasicQuestion) {
            BasicQuestion basicQuestion = (BasicQuestion) question;

            BasicDeck basicDeck = (BasicDeck) findDeck(basicQuestion.getTheme());

            for (Card card : basicDeck.getCards()) {
                if (card.getSubject().equals(basicQuestion.getSubject()))
                    return (BasicCard) card.clone();
            }
        }

        return null;
    }

    @Override
    public boolean addCard(Card card) {
        // Check if the card in param is an instance of BasicCard
        if (card instanceof BasicCard) {
            // If so, cast it to BasicCard for security
            BasicCard basicCard = (BasicCard) card;

            // Find the deck related to the basic card
            Deck basicDeck = findDeck(basicCard.getTheme());

            // If a theme was found, append the basic card to it, else create a new deck an
            // append the basic card to this new deck
            if (basicDeck != null)
                return basicDeck.addCard(basicCard);
            else {
                BasicDeck newBasicDeck = new BasicDeck(basicCard.getTheme());
                newBasicDeck.addCard(basicCard);
                return basicDeckList.add(newBasicDeck);
            }

        }

        // If the card in param is not an instance of BasicCard, return false
        return false;
    }

    @Override
    public boolean removeCard(Card card) {
        // Check if the card in param is an instance of BasicCard
        if (card instanceof BasicCard) {
            // If so, cast it to BasicCard for security
            BasicCard basicCard = (BasicCard) card;

            // Find the deck related to the basic card
            Deck basicDeck = findDeck(basicCard.getTheme());

            // If a deck could be found, try to remove the card and
            // return true if so, else false
            if (basicDeck != null) {
                return basicDeck.removeCard(basicCard);
            }
        }

        // If the card in param is not an instance of BasicCard or if no deck could be
        // found, return false
        return false;
    }

    @Override
    public boolean updateCard(Card oldCard, Card newCard) {
        // Check if the cards in param are an instance of BasicCard
        if (oldCard instanceof BasicCard && newCard instanceof BasicCard) {
            // If so, cast them to BasicCard for security
            BasicCard oldBasicCard = (BasicCard) oldCard;
            BasicCard newBasicCard = (BasicCard) newCard;

            // Find the deck related to the old basic card
            Deck basicDeck = findDeck(oldBasicCard.getTheme());

            // If a deck could be found, try to update the card and
            // return true if so, else false.
            if (basicDeck != null)
                return basicDeck.updateCard(oldBasicCard, newBasicCard);

        }

        // If the card in param is not an instance of BasicCard or if no deck could be
        // found, return false
        return false;
    }

    @Override
    public void clearCardList() {
        basicDeckList.clear();
    }

    @Override
    public void writeDeckToJson() {
        // Calls the static method "writeJsonFile" from the "JsonSerializable" class,
        // add the deck list and its class in the method param
        try {
            JsonSerializable.writeJsonFile(basicDeckList, BasicDeck.class);
        } catch (ObjectCannotBeSerializedException e) {
            e.printStackTrace();
        }
    }

    /* GETTERS AND SETTERS */
    public List<Deck> getDeckList() {
        // Clone the deck list and return it
        List<Deck> clonedBasicDeckList = new ArrayList<>();

        for (BasicDeck basicDeck : basicDeckList) {
            clonedBasicDeckList.add((BasicDeck) basicDeck.clone());
        }

        return clonedBasicDeckList;
    }

    public void setDeckList(List<Deck> basicDeckList) {
        // Clear the list first
        this.basicDeckList.clear();

        // Clone the deck in param and and append it to the deck in attribute
        for (Deck basicDeck : basicDeckList) {
            this.basicDeckList.add((BasicDeck) basicDeck.clone());
        }
    }

}
