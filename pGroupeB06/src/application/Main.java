package application;

import java.util.Locale;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import view.user.MainWindowBP;
import view.util.SharedPreferences;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		System.setProperty("file.encoding", "UTF-8");

		try {
			Locale.setDefault(Locale.ENGLISH);
			MainWindowBP mw = new MainWindowBP();

			Scene scene = new Scene(mw, SharedPreferences.Sizes.WINDOWS_WIDTH, SharedPreferences.Sizes.WINDOWS_HEIGHT);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			primaryStage.setScene(scene);
			primaryStage.getIcons().add(new Image("file:assets/starlogo.png"));
			primaryStage.setTitle("The More You Know");
			primaryStage.show();

			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				@Override
				public void handle(WindowEvent t) {
					Platform.exit();
					System.exit(0);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		System.setProperty("file.encoding", "UTF-8");
		launch(args);
	}
}
