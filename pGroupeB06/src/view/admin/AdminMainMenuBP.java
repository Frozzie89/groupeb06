package view.admin;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import view.util.ViewEnum;
import view.util.SharedPreferences;

public class AdminMainMenuBP extends BorderPane {
    private Text txtTitle;
    private Hyperlink hpReturn;
    private Button btnManageCards, btnManageSpecCards;
    private ViewControllerSP vc;

    public AdminMainMenuBP() {
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
        this.setPadding(new Insets(10));

        VBox vbTop = new VBox();
        VBox vbCenter = new VBox();

        vbTop.getChildren().addAll(getTxtTitle(), getHpReturn());
        this.setTop(vbTop);

        vbCenter.getChildren().addAll(getBtnManageCards(), getBtnManageSpecCards());
        vbCenter.setAlignment(Pos.CENTER);
        vbCenter.setSpacing(15);
        this.setCenter(vbCenter);
    }

    /* GETTERS AND SETTERS */
    public Text getTxtTitle() {
        if (txtTitle == null) {
            txtTitle = new Text("Welcome to the admin portal");
            txtTitle.setId(SharedPreferences.ID.TXT_AUTHENTICATION);
        }
        return txtTitle;
    }

    public Button getBtnManageCards() {
        if (btnManageCards == null)
            btnManageCards = new Button("Manage deck cards");
        btnManageCards.setId(SharedPreferences.ID.BUTTON);
        btnManageCards.setMinWidth(200);
        btnManageCards.setOnAction((event) -> {// go to the BASICCARDMANAGEMENT pane
            vc = (ViewControllerSP) (getParent()); // get the parent stackpane and stores it the vc variable
            vc.selectVisibleNode(ViewEnum.BASIC_QUESTION_MANAGEMENT.getLabel());
        });

        return btnManageCards;
    }

    public Button getBtnManageSpecCards() {
        if (btnManageSpecCards == null)
            btnManageSpecCards = new Button("Manage special cards");
        btnManageSpecCards.setId(SharedPreferences.ID.BUTTON);
        btnManageSpecCards.setMinWidth(200);
        btnManageSpecCards.setOnAction((event) -> {// go to the ADDING_SPECIAL_CARD pane
            vc = (ViewControllerSP) (getParent()); // get the parent stackpane and stores it the vc variable
            vc.selectVisibleNode(ViewEnum.SPECIAL_QUESTION_MANAGEMENT.getLabel());
        });

        return btnManageSpecCards;
    }

    public Hyperlink getHpReturn() {
        if (hpReturn == null)
            hpReturn = new Hyperlink("Return to the main menu");
        hpReturn.setId(SharedPreferences.ID.HL_LINK);
        hpReturn.setOnAction((event) -> {// go to the MainMenu pane
            vc = (ViewControllerSP) (getParent()); // get the parent stackpane and stores it the vc variable
            vc.selectVisibleNode(ViewEnum.MAIN_MENU.getLabel());
        });

        return hpReturn;
    }

}
