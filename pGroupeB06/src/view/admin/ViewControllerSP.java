package view.admin;

import java.util.HashMap;
import java.util.Map;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import view.util.ViewEnum;

public class ViewControllerSP extends StackPane {

    private HashMap<String, Node> views;

    public ViewControllerSP() {
        this.getChildren().addAll(getViews().values());
    }

    /**
     * Switch from current view to another
     * 
     * @param view String value of the enum to set visible
     */
    public void selectVisibleNode(String view) {
        // Iterates through each entry of the nodes map and compares their key with the
        // key passed in argument
        for (Map.Entry<String, Node> entry : views.entrySet()) {

            if (entry.getKey().equals(view))
                entry.getValue().setVisible(true);
            else
                entry.getValue().setVisible(false);
        }
    }

    // Getter
    public HashMap<String, Node> getViews() {
        if (views == null) {
            views = new HashMap<String, Node>();
            for (ViewEnum m : ViewEnum.values()) {
                views.put(m.getLabel(), (Node) m.getView());
            }
        }
        return views;
    }

}
