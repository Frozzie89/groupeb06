package view.admin;

import model.DeckManager;
import model.Question;
import model.SharedInstances;
import model.SpecialCard;
import model.SpecialQuestion;
import model.Theme;
import view.util.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class AddingSpecialCardGP extends GridPane {
    private Text txtTitle;
    private Label lblTheme, lblAuthor, lblSubject, lblChallenges, lblAnswers;
    private Hyperlink hlRedirectMainMenu;
    private TextField txfAuthor, txfSubject;
    private ComboBox<String> cbTheme;
    private List<Label> lblChallengeNumberList;
    private List<TextField> txfChallengesList;
    private List<ToggleGroup> tgAnswersList;
    private List<List<RadioButton>> rbAnswersList;
    private Button btnAddCard;
    private ViewControllerSP vc;
    public static final int NB_QUESTIONS = 4;

    public AddingSpecialCardGP() {
        // GridPane settings
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
        this.setPadding(new Insets(10., 10., 10., 10.));
        this.setHgap(10);
        this.setVgap(10);
        this.setMinSize(SharedPreferences.Sizes.WINDOWS_WIDTH, SharedPreferences.Sizes.WINDOWS_HEIGHT);

        int nbCols = 7;
        for (int i = 0; i < nbCols; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100 / nbCols);
            this.getColumnConstraints().add(colConst);
        }

        this.add(getTxtTitle(), 0, 0, 1, 1);
        this.add(getHlRedirectMainMenu(), 0, 1, 2, 1);

        this.add(getLblTheme(), 1, 2);
        this.add(getCbTheme(), 1, 3, 2, 1);

        this.add(getLblAuthor(), 4, 2);
        this.add(getTxfAuthor(), 4, 3, 2, 1);

        this.add(getLblSubject(), 1, 5);
        this.add(getTxfSubject(), 1, 6, 5, 1);

        this.add(getLblChallenges(), 1, 8);
        this.add(getLblAnswers(), 4, 8);

        for (int i = 0; i < NB_QUESTIONS; i++) {
            this.add(getLblChallengeNumberList().get(i), 0, 9 + i);
            GridPane.setHalignment(getLblChallengeNumberList().get(i), HPos.RIGHT);

            this.add(getTxfChallengesList().get(i), 1, 9 + i, 3, 1);

            this.add(getRbAnswersList().get(i).get(0), 4, 9 + i);
            this.add(getRbAnswersList().get(i).get(1), 5, 9 + i);
        }

        this.add(getBtnAddCard(), 5, 9 + NB_QUESTIONS);

    }

    private boolean areAllFieldsFilled() {
        if (getTxfAuthor().getText().equals("") && getTxfSubject().getText().equals(""))
            return false;

        for (TextField tfChallenge : getTxfChallengesList()) {
            if (tfChallenge.getText().equals(""))
                return false;
        }

        for (int i = 0; i < getRbAnswersList().size(); i++) {
            if (((RadioButton) tgAnswersList.get(i).getSelectedToggle()) == null)
                return false;

        }

        return true;
    }

    private void clearAllFields() {
        getTxfAuthor().clear();
        getTxfSubject().clear();

        for (TextField tfChallenge : getTxfChallengesList()) {
            tfChallenge.clear();
        }

        for (ToggleGroup tgAnswer : tgAnswersList) {
            tgAnswer.getSelectedToggle().setSelected(false);
        }

    }

    /* Getters and Setters */
    public Text getTxtTitle() {
        if (txtTitle == null) {
            txtTitle = new Text("Add a new special card");
            txtTitle.setId(SharedPreferences.ID.TXT_AUTHENTICATION);

        }

        return txtTitle;
    }

    public Label getLblTheme() {
        if (lblTheme == null) {
            lblTheme = new Label("Theme :");
            lblTheme.setId(SharedPreferences.ID.WHITEBOLD);
        }
        return lblTheme;
    }

    public Label getLblAuthor() {
        if (lblAuthor == null) {
            lblAuthor = new Label("Author :");
            lblAuthor.setId(SharedPreferences.ID.WHITEBOLD);
        }
        return lblAuthor;
    }

    public Label getLblSubject() {
        if (lblSubject == null) {
            lblSubject = new Label("Card subject :");
            lblSubject.setId(SharedPreferences.ID.WHITEBOLD);
        }
        return lblSubject;
    }

    public Label getLblChallenges() {
        if (lblChallenges == null) {
            lblChallenges = new Label("Challenges :");
            lblChallenges.setId(SharedPreferences.ID.WHITEBOLD);
        }
        return lblChallenges;
    }

    public Label getLblAnswers() {
        if (lblAnswers == null) {
            lblAnswers = new Label("Answers :");
            lblAnswers.setId(SharedPreferences.ID.WHITEBOLD);
        }
        return lblAnswers;
    }

    public Hyperlink getHlRedirectMainMenu() {
        if (hlRedirectMainMenu == null) {
            hlRedirectMainMenu = new Hyperlink("Return to admin main menu");
            hlRedirectMainMenu.setId(SharedPreferences.ID.HL_LINK);
            hlRedirectMainMenu.setOnAction((event) -> { // go to the Admin main menu pane
                vc = (ViewControllerSP) (getParent());
                vc.selectVisibleNode("AdminMainMenu");
            });
        }

        return hlRedirectMainMenu;
    }

    public TextField getTxfAuthor() {
        if (txfAuthor == null) {
            txfAuthor = new TextField();
        }

        return txfAuthor;
    }

    public TextField getTxfSubject() {
        if (txfSubject == null)
            txfSubject = new TextField();

        return txfSubject;
    }

    public ComboBox<String> getCbTheme() {
        if (cbTheme == null) {
            cbTheme = new ComboBox<>();

            for (Theme t : Theme.values()) {
                cbTheme.getItems().add(t.getLabel());
            }

            cbTheme.getSelectionModel().select(0);
        }

        return cbTheme;
    }

    public List<Label> getLblChallengeNumberList() {
        if (lblChallengeNumberList == null) {
            lblChallengeNumberList = new ArrayList<>();

            for (int i = 1; i < NB_QUESTIONS + 1; i++) {

                lblChallengeNumberList.add(new Label(Integer.toString(i) + " :"));

            }
        }

        return lblChallengeNumberList;
    }

    public List<TextField> getTxfChallengesList() {
        if (txfChallengesList == null) {
            txfChallengesList = new ArrayList<>();

            for (int i = 0; i < NB_QUESTIONS; i++) {
                txfChallengesList.add(new TextField());
            }
        }

        return txfChallengesList;
    }

    public List<List<RadioButton>> getRbAnswersList() {
        if (rbAnswersList == null) {
            rbAnswersList = new ArrayList<>();
            tgAnswersList = new ArrayList<>();

            for (int i = 0; i < NB_QUESTIONS; i++) {
                tgAnswersList.add(new ToggleGroup());

                RadioButton rbAnswerTrue = new RadioButton("True");
                RadioButton rbAnswerFalse = new RadioButton("False");
                rbAnswerTrue.setToggleGroup(tgAnswersList.get(i));
                rbAnswerFalse.setToggleGroup(tgAnswersList.get(i));

                rbAnswersList.add(new ArrayList<RadioButton>(Arrays.asList(rbAnswerTrue, rbAnswerFalse)));
            }
        }

        return rbAnswersList;
    }

    public Button getBtnAddCard() {
        if (btnAddCard == null) {
            btnAddCard = new Button("Add card");
            btnAddCard.setMaxWidth(Double.MAX_VALUE);
            btnAddCard.setId(SharedPreferences.ID.BUTTON);
            btnAddCard.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {

                    // First check if all fields are filled
                    if (!areAllFieldsFilled())
                        return;

                    // Create Questions and add them to a new BasicCard
                    List<Question> createdSpecialQuestions = new ArrayList<>();
                    for (int i = 0; i < getTxfChallengesList().size(); i++) {

                        Boolean answerValue = Boolean
                                .parseBoolean(((RadioButton) tgAnswersList.get(i).getSelectedToggle()).getText());

                        createdSpecialQuestions.add(new SpecialQuestion(getTxfAuthor().getText(),
                                getTxfSubject().getText(), getTxfChallengesList().get(i).getText(),
                                Theme.getThemeFromString(getCbTheme().getValue()), answerValue));
                    }

                    // Fetch BasicCards from json file, append new card to the list and rewrite the
                    // new list to the json file
                    DeckManager deckManager = SharedInstances.specialDeckManager;

                    deckManager.addCard(new SpecialCard(getTxfAuthor().getText(), getTxfSubject().getText(),
                            Theme.getThemeFromString(getCbTheme().getValue()), createdSpecialQuestions));

                    deckManager.writeDeckToJson();
                    clearAllFields();
                    SpecialQuestionManagementBP.updateTable();
                }
            });
        }

        return btnAddCard;
    }

}