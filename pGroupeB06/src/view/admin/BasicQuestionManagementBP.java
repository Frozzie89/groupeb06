package view.admin;

import javax.swing.JOptionPane;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import model.BasicCard;
import model.BasicQuestion;
import model.Card;
import model.Deck;
import model.DeckManager;
import model.Question;
import model.SharedInstances;
import model.Theme;
import view.util.ViewEnum;
import view.util.SharedPreferences;

public class BasicQuestionManagementBP extends BorderPane {
    private Text txtTitle;
    private Label lblFilterTheme, lblFilterSubject;
    private Hyperlink hlRedirectMainMenu;
    private ComboBox<String> cbFilterTheme;
    private TextField txfFilterSubject;
    private TableView<BasicQuestion> tbQuestions;
    private Button btnAdd, btnDelete, btnSave;
    private static ObservableList<BasicQuestion> basicQuestionList;
    private ViewControllerSP vc;

    public BasicQuestionManagementBP() {
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
        // BorderPane settings
        this.setPadding(new Insets(10));

        // Top
        VBox vbFilterTheme = new VBox(getLblFilterTheme(), getCbFilterTheme());
        vbFilterTheme.setSpacing(10);

        VBox vbFilterSubject = new VBox(getLblFilterSubject(), getTxfFilterSubject());
        vbFilterSubject.setSpacing(10);

        HBox hbFilter = new HBox(vbFilterTheme, vbFilterSubject);
        hbFilter.setSpacing(20);
        hbFilter.setPadding(new Insets(15));

        VBox vbTopRight = new VBox(getHlRedirectMainMenu());
        vbTopRight.setAlignment(Pos.TOP_RIGHT);

        VBox vbTop = new VBox(getTxtTitle(), vbTopRight, hbFilter);

        this.setTop(vbTop);
        // Center
        BorderPane.setMargin(getTbQuestions(), new Insets(10));
        this.setCenter(getTbQuestions());

        // Bottom
        HBox hbBottom = new HBox(getBtnAdd(), getBtnDelete(), getBtnSave());
        hbBottom.setSpacing(35);
        hbBottom.setAlignment(Pos.BOTTOM_CENTER);
        this.setBottom(hbBottom);

    }

    /**
     * update tableview with deckManager's basic questions list
     */
    public static void updateTable() {
        DeckManager deckManager = SharedInstances.basicDeckManager;
        getBasicQuestionList().clear();

        for (Deck deck : deckManager.getDeckList()) {
            for (Card card : deck.getCards()) {
                for (Question question : card.getQuestions()) {
                    getBasicQuestionList().add((BasicQuestion) question);
                }
            }
        }
    }

    /**
     * Filter the table according to filter options
     * 
     * @param isSubject   if the table should be filtered by the subject text field
     * @param oldValue    value to be replaced
     * @param newValue    value to replace
     * @param replaceList list to filter
     */
    public void filterTable(Boolean isSubject, String oldValue, String newValue,
            ObservableList<BasicQuestion> replaceList) {

        if (oldValue != null && (newValue.length() < oldValue.length())) {
            getTbQuestions().setItems(replaceList);
        }

        // If the subject text field is edited
        if (isSubject) {
            String subjectValue = newValue.toLowerCase();
            String themeValue = getCbFilterTheme().getValue().toUpperCase();

            ObservableList<BasicQuestion> subentries = FXCollections.observableArrayList();

            // If the theme combobox equals "All"
            if (themeValue.equalsIgnoreCase("ALL")) {
                // Keep all questions matching the subject into "subentries"
                for (int i = 0; i < getTbQuestions().getItems().size(); i++) {
                    String subjectEntry = "" + getTbQuestions().getColumns().get(1).getCellData(i);

                    if (subjectEntry.toLowerCase().contains(subjectValue)) {
                        subentries.add(getTbQuestions().getItems().get(i));
                    }
                }

            } else {
                // If the theme combobox does not equals "All"
                getTbQuestions().setItems(replaceList);
                // Keep all questions matching the subject and theme into "subentries"
                for (int i = 0; i < getTbQuestions().getItems().size(); i++) {
                    String subjectEntry = "" + getTbQuestions().getColumns().get(1).getCellData(i);
                    String themeEntry = "" + getTbQuestions().getColumns().get(0).getCellData(i);

                    if (subjectEntry.toLowerCase().contains(subjectValue) && themeEntry.equalsIgnoreCase(themeValue)) {
                        subentries.add(getTbQuestions().getItems().get(i));
                    }
                }
            }

            // Load the table with "subentries"
            getTbQuestions().setItems(subentries);

        } else {
            // Else if the theme combobox is edited
            String value = newValue.toLowerCase();
            getTbQuestions().setItems(replaceList);

            if (!value.equals("all")) {

                ObservableList<BasicQuestion> subentries = FXCollections.observableArrayList();

                // Loop through all sujects text value, if it contains text from the
                // textfield will keep it in the tableview
                for (int i = 0; i < getTbQuestions().getItems().size(); i++) {
                    String entry = "" + getTbQuestions().getColumns().get(0).getCellData(i);
                    if (entry.toLowerCase().contains(value)) {
                        subentries.add(getTbQuestions().getItems().get(i));
                    }
                }
                // Load the table with "subentries"
                getTbQuestions().setItems(subentries);
            }
        }

    }

    /* Getters and setters */
    public static ObservableList<BasicQuestion> getBasicQuestionList() {
        if (basicQuestionList == null) {
            basicQuestionList = FXCollections.observableArrayList();
        }

        return basicQuestionList;
    }

    public Text getTxtTitle() {
        if (txtTitle == null) {

            txtTitle = new Text("List of basic questions");
            txtTitle.setId(SharedPreferences.ID.TXT_AUTHENTICATION);
        }

        return txtTitle;
    }

    public Label getLblFilterTheme() {
        if (lblFilterTheme == null)
            lblFilterTheme = new Label("Filter by theme :");
        lblFilterTheme.setId(SharedPreferences.ID.WHITEBOLD);

        return lblFilterTheme;
    }

    public Label getLblFilterSubject() {
        if (lblFilterSubject == null)
            lblFilterSubject = new Label("Filter by subject :");
        lblFilterSubject.setId(SharedPreferences.ID.WHITEBOLD);

        return lblFilterSubject;
    }

    public Hyperlink getHlRedirectMainMenu() {
        if (hlRedirectMainMenu == null) {
            hlRedirectMainMenu = new Hyperlink("Return to admin main menu");
            hlRedirectMainMenu.setId(SharedPreferences.ID.HL_LINK);
            hlRedirectMainMenu.setOnAction((event) -> { // go to the Admin main menu pane
                vc = (ViewControllerSP) (getParent());
                vc.selectVisibleNode(ViewEnum.ADMIN_MAIN_MENU.getLabel());
            });

        }

        return hlRedirectMainMenu;
    }

    public ComboBox<String> getCbFilterTheme() {
        if (cbFilterTheme == null) {
            cbFilterTheme = new ComboBox<>();

            // Initialize values
            cbFilterTheme.getItems().add("All");

            for (Theme t : Theme.values()) {
                cbFilterTheme.getItems().addAll(t.getLabel());
            }

            cbFilterTheme.getSelectionModel().select(0);

            // Gets the list of questions from the tableview
            ObservableList<BasicQuestion> basicQuestionsTableList = getTbQuestions().getItems();

            cbFilterTheme.valueProperty()
                    .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        filterTable(false, oldValue, newValue, basicQuestionsTableList);
                    });
        }

        return cbFilterTheme;

    }

    public TextField getTxfFilterSubject() {
        if (txfFilterSubject == null) {
            txfFilterSubject = new TextField();
            HBox.setHgrow(txfFilterSubject, Priority.ALWAYS);

            // Gets the list of questions from the tableview
            ObservableList<BasicQuestion> basicQuestionsTableList = getTbQuestions().getItems();

            // Listerner : when key is pressed, filter tableview on the "subject" column
            txfFilterSubject.textProperty()
                    .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        filterTable(true, oldValue, newValue, basicQuestionsTableList);
                    });

        }

        return txfFilterSubject;
    }

    public TableView<BasicQuestion> getTbQuestions() {
        if (tbQuestions == null) {
            tbQuestions = new TableView<>();

            // Makes the table editable
            tbQuestions.setEditable(true);

            // Column "Theme"
            TableColumn<BasicQuestion, String> tcTheme = new TableColumn<>("Theme");
            tcTheme.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getTheme().getLabel()));

            // Column "Subject"
            TableColumn<BasicQuestion, String> tcSubject = new TableColumn<>("Subject");
            tcSubject.setCellValueFactory(new PropertyValueFactory<>("subject"));

            // Column "Challenge", is editable
            TableColumn<BasicQuestion, String> tcChallenge = new TableColumn<>("Challenge");
            tcChallenge.setCellValueFactory(new PropertyValueFactory<>("challenge"));
            tcChallenge.setCellFactory(TextFieldTableCell.<BasicQuestion>forTableColumn());
            tcChallenge.setOnEditCommit((CellEditEvent<BasicQuestion, String> event) -> {
                TablePosition<BasicQuestion, String> pos = event.getTablePosition();
                String newValue = event.getNewValue();

                if (!newValue.equals("")) {
                    BasicQuestion bq = event.getTableView().getItems().get(pos.getRow());
                    bq.setChallenge(newValue);
                    basicQuestionList.set(basicQuestionList.indexOf(bq), bq);
                } else {
                    event.getTableView().getItems().get(pos.getRow()).setChallenge(event.getOldValue());
                    tbQuestions.refresh();
                }

            });

            // Column "Answer", is editable
            TableColumn<BasicQuestion, String> tcAnswer = new TableColumn<>("Answer");
            tcAnswer.setCellValueFactory(new PropertyValueFactory<>("answer"));
            tcAnswer.setCellFactory(TextFieldTableCell.<BasicQuestion>forTableColumn());
            tcAnswer.setOnEditCommit((CellEditEvent<BasicQuestion, String> event) -> {
                TablePosition<BasicQuestion, String> pos = event.getTablePosition();
                String newValue = event.getNewValue();

                if (!newValue.equals("")) {
                    BasicQuestion bq = event.getTableView().getItems().get(pos.getRow());
                    bq.setAnswer(newValue);
                    basicQuestionList.set(basicQuestionList.indexOf(bq), bq);
                } else {
                    event.getTableView().getItems().get(pos.getRow()).setAnswer(event.getOldValue());
                    tbQuestions.refresh();
                }

            });

            // Column "Author", is editable
            TableColumn<BasicQuestion, String> tcAuthor = new TableColumn<>("Author");
            tcAuthor.setCellValueFactory(new PropertyValueFactory<>("author"));
            tcAuthor.setCellFactory(TextFieldTableCell.<BasicQuestion>forTableColumn());
            tcAuthor.setOnEditCommit((CellEditEvent<BasicQuestion, String> event) -> {
                TablePosition<BasicQuestion, String> pos = event.getTablePosition();
                String newValue = event.getNewValue();

                if (!newValue.equals("")) {
                    BasicQuestion bq = event.getTableView().getItems().get(pos.getRow());
                    bq.setAuthor(newValue);
                    basicQuestionList.set(basicQuestionList.indexOf(bq), bq);
                } else {
                    event.getTableView().getItems().get(pos.getRow()).setAuthor(event.getOldValue());
                    tbQuestions.refresh();
                }

            });

            // Get list of basic questions
            updateTable();
            tbQuestions.setItems(getBasicQuestionList());

            // Assign columns to the table
            tbQuestions.getColumns().add(tcTheme);
            tbQuestions.getColumns().add(tcSubject);
            tbQuestions.getColumns().add(tcChallenge);
            tbQuestions.getColumns().add(tcAnswer);
            tbQuestions.getColumns().add(tcAuthor);
        }

        return tbQuestions;
    }

    public Button getBtnAdd() {
        if (btnAdd == null) {
            btnAdd = new Button("Add");
            btnAdd.setId(SharedPreferences.ID.BUTTON);
            btnAdd.setMinWidth(90);
            btnAdd.setOnAction((event) -> { // go to the Admin main menu pane
                vc = (ViewControllerSP) (getParent());
                vc.selectVisibleNode(ViewEnum.ADDING_BASIC_CARD.getLabel());
            });
        }

        return btnAdd;
    }

    public Button getBtnDelete() {
        if (btnDelete == null) {
            btnDelete = new Button("Delete");
            btnDelete.setId(SharedPreferences.ID.BUTTON);
            btnDelete.setMinWidth(90);

            Tooltip tt = new Tooltip();
            tt.setText("Select a record from the table to delete the card");
            btnDelete.setTooltip(tt);

            btnDelete.setOnAction((event) -> {
                BasicQuestion questionOfCardToDelete = getTbQuestions().getSelectionModel().getSelectedItem();

                // Don't go further if no record was selected
                if (questionOfCardToDelete == null)
                    return;

                // Get user confirmation before deleting the card
                int userAnswer = JOptionPane
                        .showConfirmDialog(null,
                                "Are you sure that you want to delete this card : \" "
                                        + questionOfCardToDelete.getSubject() + " \" ? ",
                                "Warning", JOptionPane.YES_NO_OPTION);

                // User clicked on "YES"
                if (userAnswer == 0) {
                    DeckManager deckManager = SharedInstances.basicDeckManager;
                    BasicCard cardToDelete = (BasicCard) deckManager.findCard(questionOfCardToDelete);
                    deckManager.removeCard(cardToDelete);
                    deckManager.writeDeckToJson();
                    updateTable();
                }

            });
        }

        return btnDelete;
    }

    public Button getBtnSave() {
        if (btnSave == null) {
            btnSave = new Button("Save");
            btnSave.setId(SharedPreferences.ID.BUTTON);
            btnSave.setMinWidth(90);
            btnSave.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    // Save the content of the tableview into deckmanager
                    DeckManager deckManager = SharedInstances.basicDeckManager;
                    deckManager.clearCardList();

                    for (int i = 0; i < basicQuestionList.size(); i += 4) {
                        BasicCard basicCard = new BasicCard(basicQuestionList.get(i).getAuthor(),
                                basicQuestionList.get(i).getSubject(), basicQuestionList.get(i).getTheme());

                        for (int j = i; j < i + 4; j++) {
                            basicCard.addQuestion(new BasicQuestion(basicQuestionList.get(j).getAuthor(),
                                    basicQuestionList.get(j).getSubject(), basicQuestionList.get(j).getChallenge(),
                                    basicQuestionList.get(j).getTheme(), getBasicQuestionList().get(j).getAnswer()));
                        }
                        deckManager.addCard(basicCard);
                    }

                    // Then serialize it into json
                    deckManager.writeDeckToJson();
                }
            });
        }

        return btnSave;
    }

}
