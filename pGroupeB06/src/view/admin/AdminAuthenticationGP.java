package view.admin;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import model.Admin;
import view.util.ViewEnum;
import view.util.SharedPreferences;

public class AdminAuthenticationGP extends GridPane {
    private Text authentification;
    private Label lblUserName, lblPassword, lblWrongUserInfo;
    private TextField txfUserName;
    private PasswordField pwdPassword;
    private Button btnSubmit;
    private Hyperlink hlRedirectMainMenu;
    private ViewControllerSP vc;

    public AdminAuthenticationGP() {
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
        this.setAlignment(Pos.CENTER);
        this.setVgap(10);
        this.setPadding(new Insets(10));

        this.add(getAuthentification(), 0, 0);
        this.add(getLblUserName(), 0, 1);
        this.add(getTxfUserName(), 0, 2);
        this.add(getLblPassword(), 0, 3);
        this.add(getPwdPassword(), 0, 4);
        this.add(getBtnSubmit(), 0, 5);
        this.add(getLblWrongUserInfo(), 0, 6);
        this.add(getHlRedirectMainMenu(), 0, 7);

        GridPane.setHalignment(getAuthentification(), HPos.CENTER);
        GridPane.setHalignment(getHlRedirectMainMenu(), HPos.CENTER);

        // allows the submit button to occupy the entire width
        getBtnSubmit().setMaxWidth(Double.POSITIVE_INFINITY);

    }

    /**
     * Checks if all text fields are filled
     * 
     * @return true if all text fields are filled, else false
     */
    private boolean areAllFieldsFilled() {
        return !getTxfUserName().getText().equals("") && !getPwdPassword().getText().equals("");
    }

    /**
     * Clear all text fields
     */
    private void clearAllFields() {
        getTxfUserName().setText("");
        getPwdPassword().setText("");
    }

    /* GETTERS AND SETTERS */
    public Text getAuthentification() {
        if (authentification == null) {
            authentification = new Text("Authenticate yourself");
            authentification.setId(SharedPreferences.ID.TXT_AUTHENTICATION);
        }
        return authentification;
    }

    public Hyperlink getHlRedirectMainMenu() {
        if (hlRedirectMainMenu == null) {
            hlRedirectMainMenu = new Hyperlink("Return to the main menu");
            hlRedirectMainMenu.setId(SharedPreferences.ID.HL_LINK);
            hlRedirectMainMenu.setOnAction((event) -> {
                // Redirect to MainMenuBP view
                vc = (ViewControllerSP) (getParent());
                vc.selectVisibleNode(ViewEnum.MAIN_MENU.getLabel());
                clearAllFields();
            });
        }
        return hlRedirectMainMenu;

    }

    public Label getLblUserName() {
        if (lblUserName == null) {
            lblUserName = new Label("Username :");
            lblUserName.setId(SharedPreferences.ID.WHITEBOLD);
        }
        return lblUserName;
    }

    public Label getLblWrongUserInfo() {
        if (lblWrongUserInfo == null) {
            lblWrongUserInfo = new Label("We couldn't find an account for this user");
            lblWrongUserInfo.setId(SharedPreferences.ID.LBL_WRONG_USER_INFO);
            lblWrongUserInfo.setVisible(false);
        }

        return lblWrongUserInfo;
    }

    public TextField getTxfUserName() {
        if (txfUserName == null) {
            txfUserName = new TextField();
            txfUserName.setPromptText("Username");

        }
        return txfUserName;
    }

    public Label getLblPassword() {
        if (lblPassword == null)
            lblPassword = new Label("Password :");
        lblPassword.setId(SharedPreferences.ID.WHITEBOLD);

        return lblPassword;
    }

    public PasswordField getPwdPassword() {
        if (pwdPassword == null) {
            pwdPassword = new PasswordField();
            pwdPassword.setPromptText("Password");
        }
        return pwdPassword;
    }

    public Button getBtnSubmit() {
        if (btnSubmit == null) {
            btnSubmit = new Button("Submit");
            btnSubmit.setId(SharedPreferences.ID.BUTTON);

            btnSubmit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    if (!areAllFieldsFilled())
                        return;

                    Admin admin = Admin.findAdminJson(getTxfUserName().getText(), getPwdPassword().getText());

                    // If admin is not null, inserted info are correct
                    if (admin != null) {
                        // redirect user to Admin portal
                        vc = (ViewControllerSP) getParent();
                        vc.selectVisibleNode(ViewEnum.ADMIN_MAIN_MENU.getLabel());
                        clearAllFields();
                        getLblWrongUserInfo().setVisible(false);
                    } else {
                        getLblWrongUserInfo().setVisible(true);
                    }

                }
            });
        }

        return btnSubmit;
    }

}
