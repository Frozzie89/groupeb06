package view.util;

import javafx.scene.Node;
import view.admin.AddingBasicCardGP;
import view.admin.AddingSpecialCardGP;
import view.admin.AdminAuthenticationGP;
import view.admin.AdminMainMenuBP;
import view.admin.BasicQuestionManagementBP;
import view.admin.SpecialQuestionManagementBP;
import view.user.BasicCardBP;
import view.user.CreditsGP;
import view.user.GameBoardBP;
import view.user.MainMenuBP;
import view.user.RulesBP;
import view.user.TitleBP;
import view.user.SpecialCardBP;
import view.user.WinBP;
public enum ViewEnum {
	
	ADMIN_AUTHENTIFICATION("AdminAuthentification",new AdminAuthenticationGP()),
	ADMIN_MAIN_MENU("AdminMainMenu",new AdminMainMenuBP()),
	BASIC_QUESTION_MANAGEMENT("BasicQuestionManagement",new BasicQuestionManagementBP()),
	ADDING_BASIC_CARD("AddinBasicCard", new AddingBasicCardGP()),
	CREDITS("Credits",new CreditsGP()),
	MAIN_MENU("MainMenu", new MainMenuBP()),
	ADDING_SPECIAL_CARD("AddingSpecialCard",new AddingSpecialCardGP()),
	SPECIAL_QUESTION_MANAGEMENT("SpecialQuestionManagement", new SpecialQuestionManagementBP()),
	RULES("Rules", new RulesBP()),
	TITLE("Title", new TitleBP()),
    BASIC_CARD("BasicCard", new BasicCardBP()),
    GAME_BOARD("GameBoard", new GameBoardBP()),
    WIN("Win", new WinBP()),
	SPECIAL_CARD("SpecialCard", new SpecialCardBP());
	
	private String label;
	private Node view;
	
	private ViewEnum(String label, Node view) {
		this.label = label;
		this.view = view;
		
	}

	public String getLabel() {
		return label;
	}

	public Node getView() {
		return view;
	}
	
    /**
     * Gets the current visible view
     * 
     * @return a viewEnum
     */
	public static ViewEnum getCurrentView() {
		for(ViewEnum view : values()) {
			if(view.getView().isVisible()) {
				return view;
			}
			
		}
		return null;
		
	}

}




