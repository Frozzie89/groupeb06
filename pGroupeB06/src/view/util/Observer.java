package view.util;

public interface Observer {
    public void actualize(Observable observable);
}
