package view.util;

public abstract class Observable {
    private Observer observer;

    public Observable() {
        observer = null;
    }

    /**
     * Save an observer to notify
     * 
     * @param observer observer to notify
     */
    public void addObsever(Observer observer) {
        this.observer = observer;
    }

    /**
     * Notify the observer and unset it
     */
    public void notifyObserver() {
        observer.actualize(this);
        observer = null;
    }

    /**
     * Unset the current observer
     * 
     * @param observer observer to remove
     */
    public void removeObserver(Observer observer) {
        observer = null;
    }

    /* GETTERS AND SETTERS */
    public Observer getObserver() {
        return observer;
    }
}
