package view.util;

import javafx.geometry.Insets;

public interface SharedPreferences {

    interface ID {
        String HL_LINK = "hlLink";
        String WHITEBOLD = "WhiteBold";
        String TXT_AUTHENTICATION = "txtAuthentification";
        String LBL_WRONG_USER_INFO = "lblWrongUserInfo";
        String BUTTON = "launch_button";
        String SUB_BUTTON = "submit_button";
        String CREDIT_TXT = "CreditsTxt";
        String MAIN_MENU_TXT = "MainMenuTxt";
        String PLAYER_TEXT = "PlayerText";
        String BACKGROUND_COLOR = "BackgroundColor";
        String QUESTION_LABEL = "QuestionLabel";
        String PLAYER_TURN = "PlayerTurn";
        String LBL_MAIN_MENU = "lblMainMenu";
        String TXT_WIN_PLAYER = "txtWinPlayer";
    }

    interface Sizes {
        Insets SMALL = new Insets(10);
        int WINDOWS_HEIGHT = 720;
        int WINDOWS_WIDTH = 1280;
    }

    interface Application {
        boolean isProduction = true;
    }

}
