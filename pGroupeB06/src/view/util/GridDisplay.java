package view.util;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Light;
import javafx.scene.effect.Light.Distant;
import javafx.scene.effect.Lighting;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import model.GameManager;
import model.SharedInstances;
import model.Tile;
import model.Tile.TileType;

public class GridDisplay {
	private GridPane gridpane;
	private Group display;
	private List<Circle> pawns;

	private static final double ELEMENT_SIZE = 80;
	private static final double GAP = ELEMENT_SIZE / 10;
	private static final int COLUMNS = 5;
	private static final int ROWS = 5;

	public GridDisplay() {
		gridpane = new GridPane();
		display = new Group(gridpane);
		pawns = new ArrayList<>();

		gridpane.setHgap(GAP);
		gridpane.setVgap(GAP);

		createElements();
	}

	/**
	 * Displays players pawns on the gridpane
	 * 
	 * @param nbPlayers number of players in game
	 */
	public void createPawns(int nbPlayers) {
		GameManager gm = SharedInstances.gameManager;

		DropShadow dropShadow = new DropShadow();
		dropShadow.setRadius(5.0);
		dropShadow.setOffsetX(3.0);
		dropShadow.setOffsetY(3.0);
		dropShadow.setColor(Color.color(0.4, 0.5, 0.5));

		// Positions of each pawns
		final int[][] offsets = { { 2, -15 }, { 27, -15 }, { 52, -15 }, { 2, 14 }, { 27, 14 }, { 52, 14 } };
		// Creates and add pawns onto the gridpane
		for (int i = 0; i < nbPlayers; i++) {
			Circle pawn = new Circle(10);
			pawn.setTranslateX(offsets[i][0]);
			pawn.setTranslateY(offsets[i][1]);
			pawn.setStroke(Color.BLACK);
			pawn.setEffect(dropShadow);
			pawn.setFill((gm.getPlayerList().get(i).getColor()));
			pawns.add(pawn);
			gridpane.add(pawn, 0, 0);
		}
	}

	/**
	 * Creates the graphic tiles
	 */
	private void createElements() {
		int cpt = 0;
		boolean isEven = true;

		for (int y = 0; y < ROWS; y++) {
			// Creates a line of tiles each even rows
			if (y % 2 == 0) {
				createElementsLine(isEven, cpt, y);
				cpt += 5;

			} else {
				// Creates one tile on the side
				if (isEven) {
					// First creates hidden elements
					for (int k = 0; k < 4; k++) {
						gridpane.add(createInvisibleElement(), k, y);

					}

					// Then creates one tile
					gridpane.add(createElement(cpt++), 4, y);

				} else {
					// First creates one tile
					gridpane.add(createElement(cpt++), 0, y);

					// Then creates hidden elements
					for (int k = 0; k < 4; k++) {
						gridpane.add(createInvisibleElement(), k, y);
					}

				}

				// Invert value of isEven before next loop
				isEven = !isEven;
			}
		}
	}

	public void resetPawns() {
		// Clear all the pawn on the GridPane
		gridpane.getChildren().removeAll(pawns);
		// Clear the list of pawns
		pawns.clear();
	}

	public void movePawn() {
		GameManager gm = SharedInstances.gameManager;
		resetPawns();
		int nbPlayers = gm.getPlayerList().size();
		createNewPawn(nbPlayers);

	}

	public void createNewPawn(int nbPlayers) {
		GameManager gm = SharedInstances.gameManager;

		// X and Y coordinates to place pawns
		int xCoord = 0, yCoord = 0;

		// Light effect on the tiles of the GridPane
		DropShadow dropShadow = new DropShadow();
		dropShadow.setRadius(5.0);
		dropShadow.setOffsetX(3.0);
		dropShadow.setOffsetY(3.0);
		dropShadow.setColor(Color.color(0.4, 0.5, 0.5));

		// Loop for each player to determine the correct position to place his pawn
		for (int i = 0; i < nbPlayers; i++) {
			int score = gm.getPlayerList().get(i).getPosition();
			switch (score) {
			case 0:
				xCoord = 0;
				yCoord = 0;
				break;
			case 1:
				xCoord = 1;
				yCoord = 0;
				break;
			case 2:
				xCoord = 2;
				yCoord = 0;
				break;
			case 3:
				xCoord = 3;
				yCoord = 0;
				break;
			case 4:
				xCoord = 4;
				yCoord = 0;
				break;
			case 5:
				xCoord = 4;
				yCoord = 1;
				break;
			case 6:
				xCoord = 4;
				yCoord = 2;
				break;
			case 7:
				xCoord = 3;
				yCoord = 2;
				break;
			case 8:
				xCoord = 2;
				yCoord = 2;
				break;
			case 9:
				xCoord = 1;
				yCoord = 2;
				break;
			case 10:
				xCoord = 0;
				yCoord = 2;
				break;
			case 11:
				xCoord = 0;
				yCoord = 3;
				break;
			case 12:
				xCoord = 0;
				yCoord = 4;
				break;
			case 13:
				xCoord = 1;
				yCoord = 4;
				break;
			case 14:
				xCoord = 2;
				yCoord = 4;
				break;
			case 15:
				xCoord = 3;
				yCoord = 4;
				break;
			case 16:
				xCoord = 4;
				yCoord = 4;
				break;
			}
			// Positions of each pawns
			final int[][] offsets = { { 2, -15 }, { 27, -15 }, { 52, -15 }, { 2, 14 }, { 27, 14 }, { 52, 14 } };
			Circle pawn = new Circle(10);
			pawn.setTranslateX(offsets[i][0]);
			pawn.setTranslateY(offsets[i][1]);
			pawn.setStroke(Color.BLACK);
			pawn.setEffect(dropShadow);
			pawn.setFill((gm.getPlayerList().get(i).getColor()));
			pawns.add(pawn);
			gridpane.add(pawn, xCoord, yCoord);

		}
	}

	/**
	 * Creates a full row of graphic tiles
	 * 
	 * @param isEven     if the row is even
	 * @param cpt        counter of tiles
	 * @param currentRow index of the current row
	 */
	private void createElementsLine(boolean isEven, int cpt, int currentRow) {
		if (isEven) {
			int pos = cpt;

			// Creates tiles from left to right
			for (int x = 0; x < COLUMNS; x++) {
				gridpane.add(createElement(pos++), x, currentRow);
			}

		} else {
			int pos = cpt + 4;

			// Creates tiles from right to left
			for (int x = 0; x < COLUMNS; x++) {
				gridpane.add(createElement(pos--), x, currentRow);

			}
		}
	}

	/**
	 * Creates an invisible Rectangle
	 * 
	 * @return an invisible Rectangle
	 */

	private Rectangle createInvisibleElement() {
		Rectangle rectangle = new Rectangle(ELEMENT_SIZE, ELEMENT_SIZE);
		rectangle.setVisible(false);

		return rectangle;
	}

	/**
	 * Creates a visible Rectangle. The Rectangle is styled after the theme of the
	 * tile type of the tile at cpt
	 * 
	 * @param cpt counter of tiles
	 * @return a Rectangle
	 */
	private Rectangle createElement(int cpt) {
		List<Tile> tiles = SharedInstances.gameManager.getTiles();
		Rectangle rectangle = new Rectangle(ELEMENT_SIZE, ELEMENT_SIZE);
		rectangle.setFill(tiles.get(cpt).getTileTheme().getColor());

		if (tiles.get(cpt).getTileType().equals(TileType.SPECIAL)) {
			rectangle.setStroke(Color.RED);
			rectangle.setStrokeType(StrokeType.INSIDE);
			rectangle.setStrokeWidth(3);
		}

		Distant light = new Light.Distant();
		light.setAzimuth(85.0);
		light.setElevation(100.0);
		Lighting lighting = new Lighting();
		lighting.setLight(light);
		lighting.setSurfaceScale(2.0);

		rectangle.setEffect(lighting);

		return rectangle;
	}

	/* GETTERS AND SETTERS */
	public Group getDisplay() {
		return display;
	}

}