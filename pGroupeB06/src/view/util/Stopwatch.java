package view.util;

import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Label;

public class Stopwatch extends Observable {
    private int interval;
    private static Timer timer;
    public static final int delay = 1000;
    public static final int period = 1000;

    public Stopwatch(int interval, Observer view) {
        this.interval = interval + 1;
        timer = new Timer();
        addObsever(view);
    }

    /**
     * Decrement the integer value of a JavaFX Label until it reaches 0. Notify the
     * observer when 0 is reached
     * 
     * @param label label to update every second
     */
    public void run(Label label) {
        if (label.isVisible() == true) {
            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    // While there is still time left, decrement it and update the counter
                    if (interval > 0) {
                        Platform.runLater(() -> label.setText("" + interval));
                        interval--;

                        // When time runs out, notify observer and updater the counter to "Time is up"
                    } else {
                        Platform.runLater(() -> label.setText("Time is up !"));

                        Timer timer = new Timer();
                        timer.schedule(new TimerTask() {

                            @Override
                            public void run() {
                                Platform.runLater(() -> label.setText(""));
                                notifyObserver();
                            }

                        }, 3000);

                        endStopWatch();
                    }
                }

            }, delay, period);
        }
    }

    /**
     * Freeze the time
     */
    public static void endStopWatch() {
        timer.cancel();
    }

    /* GETTERS AND SETTERS */
    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public boolean isVisible(Node node) {
        if (node.isVisible()) {
            return true;
        }

        return false;
    }

}
