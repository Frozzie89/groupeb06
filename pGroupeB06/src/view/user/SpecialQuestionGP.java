package view.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.GameManager;
import model.Question;
import model.SharedInstances;
import model.SpecialCard;
import model.SpecialQuestion;
import view.admin.ViewControllerSP;
import view.util.SharedPreferences;
import view.util.Stopwatch;
import view.util.ViewEnum;

public class SpecialQuestionGP extends GridPane {
	private SpecialCard specialCard;
	private List<Text> questions;
	private List<Text> answers;
	private List<ToggleGroup> tgAnswersList;
	private List<List<RadioButton>> rbAnswersList;
	private Text txtQuestionAnswer;
	private Button btnSubmit;

	public SpecialQuestionGP(SpecialCard sc) {
		specialCard = sc;
		this.setMinWidth(SharedPreferences.Sizes.WINDOWS_WIDTH);
		this.setAlignment(Pos.CENTER);
		this.setHgap(10);
		this.setVgap(50);

		ColumnConstraints colConst = new ColumnConstraints();
		colConst.setPercentWidth(5);
		this.getColumnConstraints().add(colConst);

		for (int i = 0; i < getQuestions().size(); i++) {
			this.add(getAnswers().get(i), 0, i);
			this.add(getQuestions().get(i), 1, i);

			this.add(getRbAnswersList().get(i).get(0), 2, i);
			this.add(getRbAnswersList().get(i).get(1), 3, i);

		}

		this.add(getBtnSubmit(), 3, getQuestions().size() + 1);
		this.add(getTxtQuestionAnswer(), 1, getQuestions().size() + 1);
	}

	public boolean areAllButtonsSelected() {
		for (int i = 0; i < getRbAnswersList().size(); i++) {
			if (((RadioButton) tgAnswersList.get(i).getSelectedToggle()) == null)
				return false;
		}
		return true;
	}

	public void resetView() {
		SpecialCardBP sc = (SpecialCardBP) (getParent().getParent());
		sc.getRvb().setVisible(true);
		sc.getSqg().setVisible(false);
		tgAnswersList.clear();

		for (int i = 0; i < answers.size(); i++) {
			answers.get(1).setText("");
		}
	}

	/* GETTERS AND SETTERS */
	public Button getBtnSubmit() {
		if (btnSubmit == null) {
			btnSubmit = new Button("Submit");
			btnSubmit.setId(SharedPreferences.ID.SUB_BUTTON);

			btnSubmit.setOnAction((event) -> {
				if (areAllButtonsSelected()) {
					GameManager gm = SharedInstances.gameManager;
					int nbAnswersRight = 0;
					List<Question> ListQuestion = getSpecialCard().getQuestions();

					for (int i = 0; i < ListQuestion.size(); i++) {
						if (Boolean.parseBoolean(((RadioButton) tgAnswersList.get(i).getSelectedToggle())
								.getText()) == ((SpecialQuestion) ListQuestion.get(i)).isAnswer()) {
							getAnswers().get(i).setText("Correct!");
							getAnswers().get(i).setFill(Color.GREEN);
							nbAnswersRight++;

						} else {
							getAnswers().get(i).setText("Wrong!");
							getAnswers().get(i).setFill(Color.RED);
						}
					}

					// Move player, and check if player has won
					boolean hasPlayerWon = gm.movePlayerPosition(nbAnswersRight);
					ViewControllerSP vcc = (ViewControllerSP) (getParent().getParent().getParent());
					for (Node node : vcc.getChildren()) {
						if (node instanceof GameBoardBP) {
							GameBoardBP board = (GameBoardBP) node;
							board.getGridDisplay().movePawn();
							break;
						}
					}
					Stopwatch.endStopWatch();

					// If player hasn't won, continue the game, else move to Win view
					if (!hasPlayerWon) {
						GameBoardBP.nextTurn();

						Timer timer = new Timer();
						timer.schedule(new TimerTask() {
							@Override
							public void run() {
								resetView();
								ViewControllerSP vc = (ViewControllerSP) (getParent().getParent().getParent());
								vc.selectVisibleNode(ViewEnum.GAME_BOARD.getLabel());

							}
						}, 3000);
					} else {
						resetView();
						vcc = (ViewControllerSP) (getParent().getParent().getParent());
						for (Node node : vcc.getChildren()) {
							if (node instanceof GameBoardBP) {
								GameBoardBP board = (GameBoardBP) node;
								board.getGridDisplay().resetPawns();
								break;
							}
						}
						WinBP.updateView();
						ViewControllerSP vc = (ViewControllerSP) (getParent().getParent().getParent());
						vc.selectVisibleNode(ViewEnum.WIN.getLabel());
					}

				}
			});

		}
		return btnSubmit;
	}

	public List<Text> getQuestions() {
		if (questions == null) {
			questions = new ArrayList<>();
			for (Question q : getSpecialCard().getQuestions()) {
				Text question = new Text(q.getChallenge());
				question.setId(SharedPreferences.ID.QUESTION_LABEL);
				getQuestions().add(question);
			}
		}
		return questions;
	}

	public List<List<RadioButton>> getRbAnswersList() {
		if (rbAnswersList == null) {
			rbAnswersList = new ArrayList<>();
			tgAnswersList = new ArrayList<>();

			for (int i = 0; i < getSpecialCard().getQuestions().size(); i++) {
				tgAnswersList.add(new ToggleGroup());

				RadioButton rbAnswerTrue = new RadioButton("True");
				RadioButton rbAnswerFalse = new RadioButton("False");
				rbAnswerTrue.setToggleGroup(tgAnswersList.get(i));
				rbAnswerFalse.setToggleGroup(tgAnswersList.get(i));

				rbAnswersList.add(new ArrayList<RadioButton>(Arrays.asList(rbAnswerTrue, rbAnswerFalse)));
			}
		}

		return rbAnswersList;
	}

	public SpecialCard getSpecialCard() {
		return specialCard;
	}

	public void setSpecialCard(SpecialCard sc) {
		specialCard = sc;
	}

	public Text getTxtQuestionAnswer() {
		if (txtQuestionAnswer == null) {
			List<Question> ListQuestions = getSpecialCard().getQuestions();
			String strAnswers = "";
			for (Question q : ListQuestions) {
				if (!SharedPreferences.Application.isProduction) {
					strAnswers += ((SpecialQuestion) q).isAnswer() + " ";
				}
			}

			txtQuestionAnswer = new Text(strAnswers);
			txtQuestionAnswer.setId(SharedPreferences.ID.WHITEBOLD);
		}

		return txtQuestionAnswer;
	}

	public List<Text> getAnswers() {
		if (answers == null) {
			answers = new ArrayList<>();
			for (int i = 0; i < getSpecialCard().getQuestions().size(); i++) {
				Text answer = new Text();
				answers.add(answer);
			}
		}
		return answers;
	}

}
