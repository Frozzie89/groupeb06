package view.user;

import java.util.ArrayList;
import java.util.List;

import exception.PlayerAlreadyExistsException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.GameManager;
import model.Player;
import model.SharedInstances;
import model.TimeDifficulty;
import view.admin.ViewControllerSP;
import view.util.ViewEnum;
import view.util.SharedPreferences;

public class MainMenuBP extends BorderPane {

	private final String IMAGE_PATH = "file:assets/starlogo.png";
	private static final int NB_MAX_PLAYERS = 6;
	private Image image = new Image(IMAGE_PATH);
	private ImageView imageView = new ImageView(image);

	private Hyperlink hlRedirectAdminPortal;
	private Hyperlink hlRedirectCredits;
	private Hyperlink hlRedirectRules;

	private Text txtWelcomeTxt;
	private Text txtNotCompliantMsg;
	private Text txtPlayerAlreadyExistsMsg;

	private List<TextField> txfUsernames;
	private List<ColorPicker> cpUsers;

	private Label lblNbPlayer;
	private ComboBox<String> cbNbPlayer;
	private Button btnLaunch;
	private ViewControllerSP vc;

	private Label lblDifficulty;
	private ToggleGroup tgDifficulty;
	private List<RadioButton> rbDifficulty;

	public MainMenuBP() {
		this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
		this.setPadding(new Insets(10));
		this.setMinSize(SharedPreferences.Sizes.WINDOWS_WIDTH, SharedPreferences.Sizes.WINDOWS_HEIGHT);

		VBox vbPortal = new VBox(getHlRedirectAdminPortal());
		VBox vbCredits = new VBox(getHlRedirectCredits());
		VBox vbRules = new VBox(getHlRedirectRules());
		HBox hbHpTxt = new HBox(vbPortal, vbCredits, vbRules);
		hbHpTxt.setSpacing(20);

		VBox vbTop = new VBox(hbHpTxt);
		vbTop.setMinHeight(40);
		vbTop.setSpacing(20);
		this.setTop(vbTop);

		VBox vbWelcomeTxt = new VBox(getTxtWelcomeTxt());
		vbWelcomeTxt.setMinHeight(140);

		VBox vbDifficutly = new VBox(getLblDifficulty());
		vbDifficutly.getChildren().addAll(getRbDifficulty());
		vbDifficutly.setSpacing(10);

		HBox hbPlayerCenter = new HBox(getLblNbPlayer(), getCbNbPlayer());

		hbPlayerCenter.setSpacing(30);
		hbPlayerCenter.setMinHeight(30);

		VBox vbPlayersFields = new VBox();
		for (int i = 0; i < NB_MAX_PLAYERS; i++) {
			HBox hbPlayerfields = new HBox(getTxfUsernames().get(i), getCpUsers().get(i));
			hbPlayerfields.setSpacing(10);
			hbPlayerfields.setMinHeight(30);
			vbPlayersFields.getChildren().add(hbPlayerfields);
		}

		VBox vbPlayer = new VBox(hbPlayerCenter, vbPlayersFields);
		vbPlayer.setSpacing(10);

		HBox hbPlayerDifficulty = new HBox(vbPlayer, vbDifficutly);
		hbPlayerDifficulty.setSpacing(50);

		VBox vbImageLogo = new VBox(getImageView());
		vbImageLogo.setAlignment(Pos.CENTER_RIGHT);

		VBox vbCenter = new VBox(vbWelcomeTxt, hbPlayerDifficulty, vbImageLogo);
		vbCenter.setPadding(new Insets(5));

		this.setCenter(vbCenter);

		HBox hbLaunchGame = new HBox(getBtnLaunch());
		hbLaunchGame.setAlignment(Pos.BOTTOM_CENTER);
		VBox vbBottom = new VBox(getTxtPlayerAlreadyExistsMsg(), getTxtNotCompliantMsg(), hbLaunchGame);
		vbBottom.setSpacing(10);
		vbBottom.setAlignment(Pos.BOTTOM_CENTER);
		this.setBottom(vbBottom);

	}

	/**
	 * Sets text fields visible and clear invisible text fields
	 */
	private void updateUserFields() {
		for (int i = 0; i < getTxfUsernames().size(); i++) {
			if (i < Integer.parseInt(cbNbPlayer.getSelectionModel().getSelectedItem())) {
				getTxfUsernames().get(i).setVisible(true);
				getCpUsers().get(i).setVisible(true);
			} else {
				getTxfUsernames().get(i).setVisible(false);
				getTxfUsernames().get(i).setText("");
				getCpUsers().get(i).setVisible(false);
				getCpUsers().get(i).setValue(Color.WHITE);
			}
		}

	}

	/**
	 * Clears all text fields
	 */
	private void clearAllFields() {
		for (int i = 0; i < getTxfUsernames().size(); i++) {
			getTxfUsernames().get(i).setText("");
			getCpUsers().get(i).setValue(Color.WHITE);
		}
	}

	/* GETTERS AND SETTERS */
	public List<ColorPicker> getCpUsers() {
		if (cpUsers == null) {
			cpUsers = new ArrayList<>();

			for (int i = 0; i < NB_MAX_PLAYERS; i++) {
				cpUsers.add(new ColorPicker());

			}
		}

		return cpUsers;
	}

	public List<TextField> getTxfUsernames() {
		if (txfUsernames == null) {
			txfUsernames = new ArrayList<>();

			for (int i = 0; i < NB_MAX_PLAYERS; i++) {
				TextField txfPlayerName = new TextField();
				txfPlayerName.setId("txfMainMenu");
				txfPlayerName.setMinWidth(250);
				txfPlayerName.setPromptText("Player " + (i + 1) + " name");
				txfUsernames.add(txfPlayerName);
			}

		}

		return txfUsernames;
	}

	public Text getTxtWelcomeTxt() {
		if (txtWelcomeTxt == null) {
			txtWelcomeTxt = new Text(
					"Welcome to The More You Know! A quiz that will test your knowledge on a large number of subjects!\r\n"
							+ "You can read the rules of the game by clicking on the Rules link.\r\n"
							+ "Please choose a number of players, indicate their names and choose a color that will represent you.\r\n"
							+ "Good luck and may the odds be ever in your favor!\r\n");
			txtWelcomeTxt.setId(SharedPreferences.ID.MAIN_MENU_TXT);

		}

		return txtWelcomeTxt;
	}

	public Text getTxtNotCompliantMsg() {
		if (txtNotCompliantMsg == null) {
			txtNotCompliantMsg = new Text(
					"Some decks are missing cards\r\nPlease add new cards before launching a game");
			txtNotCompliantMsg.setFill(Color.RED);
			txtNotCompliantMsg.setVisible(false);
		}

		return txtNotCompliantMsg;
	}

	public Text getTxtPlayerAlreadyExistsMsg() {
		if (txtPlayerAlreadyExistsMsg == null) {
			txtPlayerAlreadyExistsMsg = new Text();
			txtPlayerAlreadyExistsMsg.setFill(Color.RED);
		}

		return txtPlayerAlreadyExistsMsg;
	}

	public ImageView getImageView() {
		return imageView;

	}

	public Hyperlink getHlRedirectRules() {
		if (hlRedirectRules == null) {
			hlRedirectRules = new Hyperlink("Rules");
			hlRedirectRules.setId(SharedPreferences.ID.HL_LINK);
			hlRedirectRules.setOnAction((event) -> {
				// Redirect to Rules view
				getTxtNotCompliantMsg().setVisible(false);
				vc = (ViewControllerSP) (getParent());
				vc.selectVisibleNode(ViewEnum.RULES.getLabel());
			});

		}

		return hlRedirectRules;

	}

	public Hyperlink getHlRedirectAdminPortal() {
		if (hlRedirectAdminPortal == null) {
			hlRedirectAdminPortal = new Hyperlink("Go to admin portal");
			hlRedirectAdminPortal.setId(SharedPreferences.ID.HL_LINK);
			hlRedirectAdminPortal.setOnAction((event) -> {
				// Redirect to Admin Portal view
				getTxtNotCompliantMsg().setVisible(false);
				vc = (ViewControllerSP) (getParent());
				vc.selectVisibleNode(ViewEnum.ADMIN_AUTHENTIFICATION.getLabel());
			});
		}

		return hlRedirectAdminPortal;
	}

	public Hyperlink getHlRedirectCredits() {
		if (hlRedirectCredits == null) {
			hlRedirectCredits = new Hyperlink("Credits");
			hlRedirectCredits.setId(SharedPreferences.ID.HL_LINK);
			hlRedirectCredits.setOnAction((event) -> {
				// Redirect to Credits view
				getTxtNotCompliantMsg().setVisible(false);
				vc = (ViewControllerSP) (getParent());
				vc.selectVisibleNode(ViewEnum.CREDITS.getLabel());
			});
		}

		return hlRedirectCredits;
	}

	public Label getLblNbPlayer() {
		if (lblNbPlayer == null) {
			lblNbPlayer = new Label("Number of players:");
			lblNbPlayer.setId(SharedPreferences.ID.LBL_MAIN_MENU);
		}

		return lblNbPlayer;
	}

	public ComboBox<String> getCbNbPlayer() {
		if (cbNbPlayer == null) {
			cbNbPlayer = new ComboBox<>();

			// Initialize values
			for (int i = 0; i < NB_MAX_PLAYERS - 1; i++) {
				cbNbPlayer.getItems().add(String.valueOf(i + 2));
			}

			cbNbPlayer.getSelectionModel().select(0);

			updateUserFields();

			// Update user text fields when selected
			cbNbPlayer.setOnAction(e -> {
				updateUserFields();
			});

		}

		return cbNbPlayer;
	}

	public Label getLblDifficulty() {
		if (lblDifficulty == null) {
			lblDifficulty = new Label("Difficulty:");
			lblDifficulty.setId(SharedPreferences.ID.LBL_MAIN_MENU);
		}

		return lblDifficulty;
	}

	public List<RadioButton> getRbDifficulty() {
		if (rbDifficulty == null) {
			rbDifficulty = new ArrayList<>();
			tgDifficulty = new ToggleGroup();

			for (TimeDifficulty td : TimeDifficulty.values()) {
				RadioButton rbDifficultyItem = new RadioButton(td.getLabel());
				rbDifficultyItem.setToggleGroup(tgDifficulty);
				rbDifficulty.add(rbDifficultyItem);

				if (td.equals(TimeDifficulty.MEDIUM))
					rbDifficultyItem.setSelected(true);
			}
		}

		return rbDifficulty;
	}

	public Button getBtnLaunch() {
		if (btnLaunch == null) {
			btnLaunch = new Button("Launch the game");
			btnLaunch.setId(SharedPreferences.ID.BUTTON);
			btnLaunch.setMinWidth(90);

			btnLaunch.setOnAction((event) -> {// Go to GameBoardBP pane
				boolean everythingOk = true;

				// Checks if decks are compliant, else display error
				if (!SharedInstances.basicDeckManager.areDecksCompliant()
						|| !SharedInstances.specialDeckManager.areDecksCompliant()) {

					getTxtNotCompliantMsg().setVisible(true);
					return;
				}

				GameManager gameManager = SharedInstances.gameManager;
				for (int i = 0; i < Integer.parseInt(cbNbPlayer.getSelectionModel().getSelectedItem()); i++) {
					// Check if all usernames are unique
					if (!getTxfUsernames().get(i).getText().equals("")) {
						try {
							gameManager.addPlayer(
									new Player(getTxfUsernames().get(i).getText(), getCpUsers().get(i).getValue()));
							everythingOk = true;
						} catch (PlayerAlreadyExistsException e) {
							getTxtPlayerAlreadyExistsMsg()
									.setText("The name " + getTxfUsernames().get(i).getText() + " already exists");
							gameManager.getPlayerList().clear();
							everythingOk = false;
						}
					} else {
						// In case a text field is empty, set default value
						try {
							gameManager.addPlayer(new Player("Player " + (i + 1), getCpUsers().get(i).getValue()));
							everythingOk = true;
						} catch (PlayerAlreadyExistsException e) {
							gameManager.getPlayerList().clear();
							everythingOk = false;
						}
					}
				}

				// Load the gameboard view and start the game
				if (everythingOk) {

					// Set time difficulty of the game
					GameManager gm = SharedInstances.gameManager;
					gm.setTimeDifficulty(TimeDifficulty
							.getTimeDifficultyFromString(((RadioButton) tgDifficulty.getSelectedToggle()).getText()));

					// Redirect to game board view
					GameBoardBP.loadGameBoard();
					vc = (ViewControllerSP) (getParent());
					vc.selectVisibleNode(ViewEnum.GAME_BOARD.getLabel());

					// Load grid from gameboard
					for (Node node : vc.getChildren()) {
						if (node instanceof GameBoardBP) {
							GameBoardBP board = (GameBoardBP) node;
							board.getGridDisplay().createPawns(gameManager.getPlayerList().size());
							break;
						}
					}

					// Reset main menu view nodes
					getTxtNotCompliantMsg().setText("");
					clearAllFields();
				}
			});
		}

		return btnLaunch;
	}

}
