package view.user;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import view.util.SharedPreferences;

public class DifficultyChoiceVB extends VBox {

    private Text txtInfo, txtQuestion;
    private List<Button> btnList;
    private static final int NB_QUESTIONS = 4;

    public DifficultyChoiceVB() {

        HBox choices = new HBox();
        choices.getChildren().addAll(getBtnList());
        choices.setAlignment(Pos.CENTER);
        choices.setSpacing(10);
        choices.setPadding(new Insets(20));

        this.getChildren().addAll(getTxtQuestion(), getTxtInfo(), choices);
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
    }

    /* GETTERS AND SETTERS */
    public Text getTxtInfo() {
        if (txtInfo == null) {
            txtInfo = new Text("(1 : You know nothing, Jack     -   4 : You are a man of culture)");
            txtInfo.setId(SharedPreferences.ID.WHITEBOLD);

        }

        return txtInfo;
    }

    public Text getTxtQuestion() {
        if (txtQuestion == null) {
            txtQuestion = new Text("How much de you think you know about this ?");
            txtQuestion.setId(SharedPreferences.ID.CREDIT_TXT);
        }
        return txtQuestion;
    }

    public List<Button> getBtnList() {
        if (btnList == null) {
            btnList = new ArrayList<>();
            for (int i = 0; i < NB_QUESTIONS; i++) {
                Button btn = new Button();
                btn.setText(String.valueOf(i + 1));
                btn.setId(SharedPreferences.ID.BUTTON);
                btn.setMinWidth(100);
                btnList.add(btn);
            }
        }
        return btnList;

    }

}
