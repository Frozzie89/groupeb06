package view.user;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import view.util.SharedPreferences;

public class ReadyVB extends VBox {

    private Text txtReady;
    private Button btnReady;

    public ReadyVB() {
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(20));
        VBox vb = new VBox(getBtnReady());
        vb.setAlignment(Pos.BASELINE_CENTER);
        this.getChildren().addAll(getTxtReady(), vb);

    }

    /* GETTERS AND SETTERS */
    public Text getTxtReady() {
        if (txtReady == null) {
            txtReady = new Text("When you feel ready, click on the button");
            txtReady.setId(SharedPreferences.ID.CREDIT_TXT);
        }
        return txtReady;
    }

    public Button getBtnReady() {
        if (btnReady == null) {
            btnReady = new Button("I am ready");
            btnReady.setId(SharedPreferences.ID.SUB_BUTTON);
        }
        return btnReady;
    }

}
