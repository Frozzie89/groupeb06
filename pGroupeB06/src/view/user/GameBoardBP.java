package view.user;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import model.DrawBasicCard;
import model.DrawSpecialCard;
import model.GameManager;
import model.SharedInstances;
import model.Theme;
import model.Tile;
import model.Tile.TileType;
import view.admin.ViewControllerSP;
import view.util.GridDisplay;
import view.util.SharedPreferences;
import view.util.ViewEnum;

public class GameBoardBP extends BorderPane {
	private static List<Text> playerNames;
	private static List<Text> playerPositions;
	private static VBox playerColumnVB;
	private static Label lblPlayerTurn;
	private Button btnPlayerReady;
	private ViewControllerSP vc;
	private GridDisplay gridDisplay;

	private final String IMAGE_PATH = "file:assets/legend.png";
	private Image image = new Image(IMAGE_PATH);
	private ImageView imageView = new ImageView(image);

	public GameBoardBP() {
		this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
		this.setPadding(new Insets(10));
		gridDisplay = new GridDisplay();
		VBox vbTopCenter = new VBox(getLblPlayerTurn());
		vbTopCenter.setAlignment(Pos.TOP_CENTER);
		vbTopCenter.setMinSize(100, 100);
		VBox vbRight = new VBox();
		vbRight.setAlignment(Pos.CENTER);
		vbRight.getChildren().addAll(gridDisplay.getDisplay());
		HBox hb = new HBox(getPlayerColumnVB(), vbRight, getImageView());
		hb.setAlignment(Pos.CENTER);
		hb.setSpacing(250);
		VBox vb = new VBox(vbTopCenter, hb);
		this.setCenter(vb);

		VBox vbBottom = new VBox(getBtnPlayerReady());
		vbBottom.setAlignment(Pos.CENTER);
		this.setBottom(vbBottom);

	}

	/**
	 * Loads the game board assets
	 */
	public static void loadGameBoard() {
		GameManager gm = SharedInstances.gameManager;

		getPlayerColumnVB().getChildren().clear();
		for (int i = 0; i < gm.getPlayerList().size(); i++) {
			HBox playerRowHB = new HBox();

			Text txtUserName = new Text(gm.getPlayerList().get(i).getUserName());
			txtUserName.setId(SharedPreferences.ID.PLAYER_TEXT);
			txtUserName.setFill(gm.getPlayerList().get(i).getColor());
			getPlayerNames().add(txtUserName);

			Text txtUserPosition = new Text(Integer.toString(gm.getPlayerList().get(i).getPosition()));
			txtUserPosition.setId(SharedPreferences.ID.PLAYER_TEXT);
			txtUserPosition.setFill(gm.getPlayerList().get(i).getColor());
			getPlayerPositions().add(txtUserPosition);

			playerRowHB.getChildren().addAll(getPlayerNames().get(i));
			playerRowHB.setSpacing(10);
			playerColumnVB.getChildren().add(playerRowHB);

			getLblPlayerTurn().setText("It's " + gm.getPlayerTurn().getUserName() + " 's turn to play");
		}

	}

	/**
	 * Prepare display for next turn
	 */
	public static void nextTurn() {
		GameManager gm = SharedInstances.gameManager;

		getPlayerPositions().get(gm.getTurn()).setText("" + gm.getPlayerList().get(gm.getTurn()).getPosition());
		gm.nextTurn();

		getLblPlayerTurn().setText("It's " + gm.getPlayerTurn().getUserName() + " 's turn to play !");
	}

	/* Getters and setters */
	public GridDisplay getGridDisplay() {
		return gridDisplay;
	}

	public static VBox getPlayerColumnVB() {
		if (playerColumnVB == null) {
			playerColumnVB = new VBox();
		}

		return playerColumnVB;
	}

	public Button getBtnPlayerReady() {
		if (btnPlayerReady == null) {
			btnPlayerReady = new Button("Ready");
			btnPlayerReady.setId(SharedPreferences.ID.BUTTON);
			btnPlayerReady.setMaxWidth(Double.POSITIVE_INFINITY);

			// Draw the card and redirect to card view
			btnPlayerReady.setOnAction((event) -> {
				// intance of gamemanager
				GameManager gameManager = SharedInstances.gameManager;
				vc = (ViewControllerSP) (getParent());

				Tile tile = gameManager.getPlayerTile();

				if (tile.getTileType().equals(TileType.BASIC)) {
					// Redirect to BasicCardBP
					BasicCardBP bcp = (BasicCardBP) ViewEnum.BASIC_CARD.getView();

					// Set the strategy
					gameManager.setStrategy(new DrawBasicCard());
					bcp.loadCard(gameManager.drawCard(tile.getTileTheme()));

					vc.selectVisibleNode(ViewEnum.BASIC_CARD.getLabel());

				} else if (tile.getTileType().equals(TileType.SPECIAL)) {
					// Redirect to SpecialCardBP
					SpecialCardBP spc = (SpecialCardBP) ViewEnum.SPECIAL_CARD.getView();

					// Set the strategy
					gameManager.setStrategy(new DrawSpecialCard());
					spc.loadCard(gameManager.drawCard(Theme.getRandomTheme()));

					vc.selectVisibleNode(ViewEnum.SPECIAL_CARD.getLabel());
				}

			});
		}

		return btnPlayerReady;
	}

	public ImageView getImageView() {
		return imageView;

	}

	public static Label getLblPlayerTurn() {
		if (lblPlayerTurn == null) {
			lblPlayerTurn = new Label();
			lblPlayerTurn.setId(SharedPreferences.ID.CREDIT_TXT);
		}

		return lblPlayerTurn;
	}

	public static List<Text> getPlayerPositions() {
		if (playerPositions == null)
			playerPositions = new ArrayList<>();

		return playerPositions;
	}

	public static List<Text> getPlayerNames() {
		if (playerNames == null)
			playerNames = new ArrayList<>();

		return playerNames;
	}

}
