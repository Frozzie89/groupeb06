package view.user;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.BasicQuestion;
import model.Card;
import model.GameManager;
import model.SharedInstances;
import model.TimeDifficulty;
import view.admin.ViewControllerSP;
import view.util.Observable;
import view.util.Observer;
import view.util.Stopwatch;
import view.util.ViewEnum;
import view.util.SharedPreferences;

public class BasicCardBP extends BorderPane implements Observer {
    private Text txtTheme, txtSubject, txtPlayerName;
    private Label lblTime;
    private StackPane cards;
    private HBox topHB;
    private DifficultyChoiceVB dc;
    private BasicQuestionVB qvb;

    public BasicCardBP() {

        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);

        getTopHB().setMinHeight(120);
        getTopHB().setAlignment(Pos.CENTER);

        VBox txtSubjectHB = new VBox(getTxtSubject());
        txtSubjectHB.setAlignment(Pos.CENTER);
        txtSubjectHB.setMinHeight(100);
        txtSubjectHB.setSpacing(20);
        VBox topPartVB = new VBox(getTopHB(), txtSubjectHB);
        this.setTop(topPartVB);

        getDc().setAlignment(Pos.CENTER);
        getQvb().setAlignment(Pos.CENTER);
        getCards().getChildren().addAll(getDc(), getQvb());

        this.setCenter(getCards());

        VBox vbBotCent = new VBox(getQvb().getTxtQuestionAnswer(), getLblTime(), getTxtPlayerName());
        vbBotCent.setAlignment(Pos.BOTTOM_CENTER);
        this.setBottom(vbBotCent);
    }

    /* GETTERS AND SETTERS */
    public Text getTxtSubject() {
        if (txtSubject == null) {
            txtSubject = new Text();
            txtSubject.setFont(new Font(40));
            txtSubject.setId(SharedPreferences.ID.CREDIT_TXT);
        }

        return txtSubject;
    }

    public HBox getTopHB() {
        if (topHB == null) {
            topHB = new HBox(getTxtTheme());
        }
        return topHB;
    }

    public Text getTxtTheme() {
        if (txtTheme == null) {
            txtTheme = new Text("Top text");
            txtTheme.setFont(new Font(50));
        }

        return txtTheme;
    }

    public Label getLblTime() {
        if (lblTime == null) {
            lblTime = new Label("");
            lblTime.setMinHeight(250);
            lblTime.setId(SharedPreferences.ID.TXT_AUTHENTICATION);

        }

        return lblTime;
    }

    public StackPane getCards() {
        if (cards == null) {
            cards = new StackPane();
        }

        return cards;
    }

    public void loadCard(Card c) {
        // Display view according to drawn card
        getTopHB().setBackground(new Background(new BackgroundFill(c.getTheme().getColor(), null, null)));
        getTxtTheme().setText(c.getTheme().getLabel());
        getTxtSubject().setText(c.getSubject());

        GameManager gm = SharedInstances.gameManager;
        getTxtPlayerName().setText(gm.getPlayerTurn().getUserName() + "'s turn");

        // Start timer
        Stopwatch stopwatch = new Stopwatch(TimeDifficulty.EASY.getSeconds(), this);
        stopwatch.run(lblTime);

        // Set redirection actions on each button
        LambdaOperation operation = (i) -> {
            getDc().getBtnList().get(i).setOnAction((event) -> {
                getQvb().setVisible(true);
                getDc().setVisible(false);
                getQvb().setBasicQuestion((BasicQuestion) c.getQuestions().get(i));
                getQvb().loadQuestion();
                getQvb().setDifficulty(i + 1);
                stopwatch.setInterval(gm.getTimeDifficulty().getSeconds());
            });
        };

        for (int i = 0; i < getDc().getBtnList().size(); i++) {
            operation.assignEvent(i);
        }

    }

    public DifficultyChoiceVB getDc() {
        if (dc == null) {
            dc = new DifficultyChoiceVB();
            dc.setVisible(true);
        }

        return dc;
    }

    public BasicQuestionVB getQvb() {
        if (qvb == null) {
            qvb = new BasicQuestionVB();
            qvb.setVisible(false);
        }

        return qvb;
    }

    public Text getTxtPlayerName() {
        if (txtPlayerName == null) {
            txtPlayerName = new Text();
            txtPlayerName.setId(SharedPreferences.ID.PLAYER_TURN);
        }

        return txtPlayerName;
    }

    interface LambdaOperation {
        public void assignEvent(int i);
    }

    @Override
    public void actualize(Observable observable) {
        if (observable instanceof Stopwatch) {
            ViewControllerSP vc = (ViewControllerSP) (getParent());
            vc.selectVisibleNode(ViewEnum.GAME_BOARD.getLabel());
            qvb.resetView();
            observable.removeObserver(this);

        }

    }

}
