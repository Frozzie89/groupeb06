package view.user;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import view.admin.ViewControllerSP;
import view.util.ViewEnum;
import view.util.SharedPreferences;

public class CreditsGP extends GridPane {

    private final String IMAGE_PATH1 = "file:assets/Lucien.jpg";
    private final String IMAGE_PATH2 = "file:assets/Valentin.jpg";
    private final String IMAGE_PATH3 = "file:assets/Lorenz.png";
    private Image image1 = new Image(IMAGE_PATH1);
    private Image image2 = new Image(IMAGE_PATH2);
    private Image image3 = new Image(IMAGE_PATH3);
    private ImageView imageView1 = new ImageView(image1);
    private ImageView imageView2 = new ImageView(image2);
    private ImageView imageView3 = new ImageView(image3);

    private Text txtApplication;
    private Text txtLucien;
    private Text txtValentin;
    private Text txtLorenz;

    private ViewControllerSP vc;

    private Hyperlink hlRedirectMainMenu;

    public CreditsGP() {

        this.setAlignment(Pos.CENTER);
        this.setVgap(10);
        this.setHgap(10);

        this.setPadding(new Insets(10));
        this.add(getHlRedirectMainMenu(), 2, 4, 1, 1);

        this.add(getTxtApp(), 1, 0, 3, 1);
        this.add(getImageView1(), 1, 1);
        this.add(getTxtLucien(), 1, 2);
        this.add(getImageView2(), 2, 1);
        this.add(getTxtValentin(), 2, 2);
        this.add(getImageView3(), 3, 1);
        this.add(getTxtLorenz(), 3, 2);

        GridPane.setMargin(getImageView1(), new Insets(5, 10, 5, 10));
        GridPane.setMargin(getImageView2(), new Insets(5, 10, 5, 10));
        GridPane.setMargin(getImageView3(), new Insets(5, 10, 5, 10));

        GridPane.setHalignment(getTxtApp(), HPos.LEFT);
        GridPane.setHalignment(getTxtLucien(), HPos.CENTER);
        GridPane.setHalignment(getTxtValentin(), HPos.CENTER);
        GridPane.setHalignment(getTxtLorenz(), HPos.CENTER);
        GridPane.setHalignment(getHlRedirectMainMenu(), HPos.CENTER);
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
    }

    /* GETTERS AND SETTERS */
    public Hyperlink getHlRedirectMainMenu() {

        if (hlRedirectMainMenu == null) {
            hlRedirectMainMenu = new Hyperlink("Return to the main menu");
            hlRedirectMainMenu.setId(SharedPreferences.ID.HL_LINK);
            hlRedirectMainMenu.setOnAction((event) -> {
                vc = (ViewControllerSP) (getParent());
                vc.selectVisibleNode(ViewEnum.MAIN_MENU.getLabel());
            });
        }
        return hlRedirectMainMenu;
    }

    public Text getTxtApp() {

        if (txtApplication == null) {
            txtApplication = new Text("This application was made by :");
            txtApplication.setId(SharedPreferences.ID.CREDIT_TXT);
        }
        return txtApplication;
    }

    public Text getTxtLucien() {
        if (txtLucien == null) {
            txtLucien = new Text("Lucien Joly");
            txtLucien.setId(SharedPreferences.ID.CREDIT_TXT);

        }
        return txtLucien;
    }

    public Text getTxtValentin() {
        if (txtValentin == null) {
            txtValentin = new Text("Valentin Durieux");
            txtValentin.setId(SharedPreferences.ID.CREDIT_TXT);

        }
        return txtValentin;
    }

    public Text getTxtLorenz() {
        if (txtLorenz == null) {
            txtLorenz = new Text("Lorenz Ingrande");
            txtLorenz.setId(SharedPreferences.ID.CREDIT_TXT);

        }
        return txtLorenz;
    }

    public ImageView getImageView1() {
        return imageView1;
    }

    public ImageView getImageView2() {
        return imageView2;
    }

    public ImageView getImageView3() {
        return imageView3;

    }

}
