package view.user;

import javafx.scene.layout.BorderPane;
import view.admin.ViewControllerSP;
import view.util.ViewEnum;

public class MainWindowBP extends BorderPane {

    private ViewControllerSP vc;

    public MainWindowBP() {

        this.setCenter(getVc());

    }

    /* GETTERS AND SETTERS */
    public ViewControllerSP getVc() {
        if (vc == null) {
            vc = new ViewControllerSP();
            vc.selectVisibleNode(ViewEnum.TITLE.getLabel());
        }
        return vc;
    }

}
