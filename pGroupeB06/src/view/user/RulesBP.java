package view.user;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import view.admin.ViewControllerSP;
import view.util.ViewEnum;
import view.util.SharedPreferences;

public class RulesBP extends BorderPane {

    private Text txtTitleRules;
    private Hyperlink hlReturnMainMenu;
    private ViewControllerSP vc;
    private Text txtRules;

    public RulesBP() {
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
        this.setPadding(new Insets(10));
        VBox vbTitre = new VBox(getTxtTitleRules());
        vbTitre.setAlignment(Pos.TOP_CENTER);
        VBox vbPortal = new VBox(getHlReturnMainMenu());
        vbPortal.setAlignment(Pos.TOP_RIGHT);
        VBox vbTop = new VBox(vbTitre, vbPortal);
        this.setTop(vbTop);
        VBox vbRules = new VBox(getTxtRules());
        this.setCenter(vbRules);

    }

    /* GETTERS AND SETTERS */
    public Text getTxtTitleRules() {
        if (txtTitleRules == null) {
            txtTitleRules = new Text("Here are the rules: ");
            txtTitleRules.setId(SharedPreferences.ID.TXT_AUTHENTICATION);
        }
        return txtTitleRules;
    }

    public Hyperlink getHlReturnMainMenu() {
        if (hlReturnMainMenu == null) {
            hlReturnMainMenu = new Hyperlink("Return to the main menu");
            hlReturnMainMenu.setId(SharedPreferences.ID.HL_LINK);
            hlReturnMainMenu.setOnAction((event) -> {
                vc = (ViewControllerSP) (getParent());
                vc.selectVisibleNode(ViewEnum.MAIN_MENU.getLabel());
            });
        }

        return hlReturnMainMenu;
    }

    public Text getTxtRules() {
        if (txtRules == null) {
            txtRules = new Text(
                    "The goal of the game is to be the first to go through the board by answering the questions asked.\r\n"
                            + "Originality lies on the principle of self-assessment of his knowledge from 1 to 10 on a multitude of topics. Simple things!\r\n"
                            + "\r\n" + "Each note corresponds to a question, more or less complex.\r\n"
                            + "Choose the number of players and their corresponding colors.\r\n"
                            + "The basic maps: 4 themes with a multitude of topics.\r\n"
                            + "Special cards: the player or players falling on these boxes will follow the instructions written on the card.\r\n"
                            + "The game starts with a player shooting randomly.\r\n"
                            + "The theme is determined according to the box you came across.\r\n"
                            + "The player has 30 seconds to answer the question.\r\n"
                            + "If the answer is correct, the team will advance by the number of boxes corresponding to its self-assessed score.\r\n"
                            + "If the answer is wrong, the team will remain on the same box until the next round."
                            + "In order to be declared the winner, the team must go through the board and correctly answer a card from the last square.\r\n"
                            + "If the answer is wrong, the team draws a new card on the next round.");
            txtRules.setId(SharedPreferences.ID.MAIN_MENU_TXT);
            txtRules.setWrappingWidth(900);

        }

        return txtRules;
    }

}
