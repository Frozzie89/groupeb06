package view.user;

import java.util.Timer;
import java.util.TimerTask;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import model.BasicQuestion;
import model.GameManager;
import model.SharedInstances;
import view.admin.ViewControllerSP;
import view.util.Stopwatch;
import view.util.ViewEnum;
import view.util.SharedPreferences;

public class BasicQuestionVB extends VBox {
    private Text txtQuestion, txtMessageAnswer, txtQuestionAnswer;
    private TextField txfAnswer;
    private Button btnSubmit;
    private BasicQuestion basicQuestion;
    private ViewControllerSP vc;
    private int difficulty;

    public BasicQuestionVB() {
        this.setPadding(new Insets(20));
        HBox hb = new HBox(getTxfAnswer(), getBtnSubmit());
        hb.setSpacing(20);
        hb.setAlignment(Pos.CENTER);

        this.getChildren().addAll(getTxtQuestion(), hb, getTxtMessageAnswer());
        this.setSpacing(20);

    }

    /**
     * Displays question on screen
     */
    public void loadQuestion() {
        getTxtQuestion().setText(getBasicQuestion().getChallenge());

        if (!SharedPreferences.Application.isProduction)
            getTxtQuestionAnswer().setText(getBasicQuestion().getAnswer());
    }

    /**
     * Clears the view for next question
     */
    public void resetView() {
        BasicCardBP bc = (BasicCardBP) (getParent().getParent());
        bc.getDc().setVisible(true);
        bc.getQvb().setVisible(false);
        getTxtMessageAnswer().setText("");
        getTxfAnswer().clear();
    }

    /* Getters and setters */
    public Text getTxtMessageAnswer() {
        if (txtMessageAnswer == null) {
            txtMessageAnswer = new Text();
            txtMessageAnswer.setId("txtMessageAnswer");
        }

        return txtMessageAnswer;
    }

    public Text getTxtQuestion() {
        if (txtQuestion == null) {
            txtQuestion = new Text();
            txtQuestion.setTextAlignment(TextAlignment.CENTER);
            txtQuestion.setWrappingWidth(750);
            txtQuestion.setId(SharedPreferences.ID.CREDIT_TXT);

        }

        return txtQuestion;
    }

    public TextField getTxfAnswer() {
        if (txfAnswer == null) {
            txfAnswer = new TextField();
        }
        return txfAnswer;
    }

    public Button getBtnSubmit() {
        if (btnSubmit == null) {
            btnSubmit = new Button("Submit answer");
            btnSubmit.setId(SharedPreferences.ID.BUTTON);

            // When the player submit his answer
            btnSubmit.setOnAction((event) -> {
                GameManager gm = SharedInstances.gameManager;

                boolean hasPlayerWon = false;

                // Check if the answer is correct, show if the answer is correct or not
                if (GameManager.checkAnswer(getTxfAnswer().getText(), getBasicQuestion().getAnswer())) {
                    getTxtMessageAnswer().setText("Your answer is correct ! (" + getBasicQuestion().getAnswer() + ")");
                    getTxtMessageAnswer().setFill(Color.GREEN);

                    // Move player, and check if player has won
                    hasPlayerWon = gm.movePlayerPosition(getDifficulty());
                    vc = (ViewControllerSP) (getParent().getParent().getParent());
                    for (Node node : vc.getChildren()) {
                        if (node instanceof GameBoardBP) {
                            GameBoardBP board = (GameBoardBP) node;
                            board.getGridDisplay().movePawn();
                            break;
                        }
                    }

                } else {
                    getTxtMessageAnswer().setText("Wrong ! The Answer was : " + getBasicQuestion().getAnswer());
                    getTxtMessageAnswer().setFill(Color.RED);
                }

                Stopwatch.endStopWatch();

                // If player hasn't won, continue the game, else move to Win view
                if (!hasPlayerWon) {
                    GameBoardBP.nextTurn();

                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            resetView();
                            ViewControllerSP vc = (ViewControllerSP) (getParent().getParent().getParent());
                            vc.selectVisibleNode(ViewEnum.GAME_BOARD.getLabel());
                        }
                    }, 3000);
                } else {
                    resetView();
                    vc = (ViewControllerSP) (getParent().getParent().getParent());
                    for (Node node : vc.getChildren()) {
                        if (node instanceof GameBoardBP) {
                            GameBoardBP board = (GameBoardBP) node;
                            board.getGridDisplay().resetPawns();
                            break;
                        }
                    }
                    WinBP.updateView();
                    ViewControllerSP vc = (ViewControllerSP) (getParent().getParent().getParent());
                    vc.selectVisibleNode(ViewEnum.WIN.getLabel());
                }

            });
        }

        return btnSubmit;
    }

    public BasicQuestion getBasicQuestion() {
        return basicQuestion;
    }

    public void setBasicQuestion(BasicQuestion basicQuestion) {
        this.basicQuestion = basicQuestion;
    }

    public Text getTxtQuestionAnswer() {
        if (txtQuestionAnswer == null) {
            txtQuestionAnswer = new Text();
        }

        return txtQuestionAnswer;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

}
