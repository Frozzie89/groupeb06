package view.user;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import view.admin.ViewControllerSP;
import view.util.ViewEnum;
import view.util.SharedPreferences;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class TitleBP extends BorderPane {
    private final String IMAGE_PATH = "file:assets/logo5.png";
    private Hyperlink hlRedirectMainMenu;
    private Image image = new Image(IMAGE_PATH);
    private ImageView imageView = new ImageView(image);
    private ViewControllerSP vc;

    public TitleBP() {
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
        this.setOnKeyPressed((event) -> {
            vc = (ViewControllerSP) (getParent()); // get the parent stackpane and stores it in the vc variable
            vc.selectVisibleNode(ViewEnum.MAIN_MENU.getLabel()); // call the selectVisibleNode method
        });
        VBox vbCenter = new VBox(getImageView());
        VBox vbBottom = new VBox(getHlRedirectMainMenu());
        this.setCenter(vbCenter);
        this.setBottom(vbBottom);
        vbCenter.setAlignment(Pos.CENTER);
        vbBottom.setAlignment(Pos.CENTER);
    }

    /* GETTERS AND SETTERS */
    public ImageView getImageView() {
        return imageView;
    }

    public Hyperlink getHlRedirectMainMenu() {
        if (hlRedirectMainMenu == null) {
            hlRedirectMainMenu = new Hyperlink("Press any key to continue");
            hlRedirectMainMenu.setId(SharedPreferences.ID.HL_LINK);
            hlRedirectMainMenu.setOnAction((event) -> {
                // Redirect to main menu view
                vc = (ViewControllerSP) (getParent());
                vc.selectVisibleNode(ViewEnum.MAIN_MENU.getLabel());
            });
        }
        return hlRedirectMainMenu;

    }

}
