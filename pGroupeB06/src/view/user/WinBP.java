package view.user;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import view.admin.ViewControllerSP;
import view.util.ViewEnum;
import model.GameManager;
import model.SharedInstances;
import view.util.SharedPreferences;

public class WinBP extends BorderPane {
    private Text txtPlayerWinner, txtWinMessage;
    private static StringProperty txtWinMessageProp;
    private Hyperlink hlReturnMainMenu;
    private ViewControllerSP vc;
    private final String IMAGE_PATH = "file:assets/logo5.png";
    private Image image = new Image(IMAGE_PATH);
    private ImageView imageView = new ImageView(image);

    public WinBP() {
        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);
        this.setPadding(new Insets(10));
        VBox vbTop = new VBox(getHlReturnMainMenu());
        this.setTop(vbTop);
        VBox vbLogo = new VBox(getImageView());
        vbLogo.setAlignment(Pos.BOTTOM_CENTER);
        ;
        VBox vbCenter = new VBox(getTxtPlayerWinner(), getTxtWinMessage(), vbLogo);
        vbCenter.setSpacing(50);

        vbCenter.setAlignment(Pos.CENTER);
        this.setCenter(vbCenter);
    }

    public static void updateView() {
        GameManager gameManager = SharedInstances.gameManager;
        txtWinMessageProp.setValue(gameManager.getPlayerWinner().getUserName() + " won !");
    }

    /* GETTERS AND SETTERS */
    public Hyperlink getHlReturnMainMenu() {
        if (hlReturnMainMenu == null) {
            hlReturnMainMenu = new Hyperlink("Return to Main menu");
            hlReturnMainMenu.setId(SharedPreferences.ID.HL_LINK);
            hlReturnMainMenu.setOnAction((event) -> {
                GameManager gameManager = SharedInstances.gameManager;
                gameManager.resetGame();
                vc = (ViewControllerSP) (getParent());
                vc.selectVisibleNode(ViewEnum.MAIN_MENU.getLabel());
            });
        }

        return hlReturnMainMenu;
    }

    public Text getTxtWinMessage() {
        if (txtWinMessage == null) {
            txtWinMessage = new Text("Thank you for playing");
            txtWinMessage.setId(SharedPreferences.ID.MAIN_MENU_TXT);
            txtWinMessage.setTextAlignment(TextAlignment.CENTER);
        }

        return txtWinMessage;
    }

    public Text getTxtPlayerWinner() {
        if (txtPlayerWinner == null) {
            txtPlayerWinner = new Text();
            txtPlayerWinner.textProperty().bind(getTxtWinMessageProp());
            txtPlayerWinner.setId(SharedPreferences.ID.TXT_WIN_PLAYER);
        }

        return txtPlayerWinner;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public SimpleStringProperty getTxtWinMessageProp() {
        if (txtWinMessageProp == null) {
            txtWinMessageProp = new SimpleStringProperty();
        }

        return (SimpleStringProperty) txtWinMessageProp;
    }

}