package view.user;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.Card;
import model.GameManager;
import model.SharedInstances;
import model.SpecialCard;
import model.TimeDifficulty;
import view.admin.ViewControllerSP;
import view.util.Observable;
import view.util.Observer;
import view.util.SharedPreferences;
import view.util.Stopwatch;
import view.util.ViewEnum;

public class SpecialCardBP extends BorderPane implements Observer {
    private Text txtTitle, txtSubject, txtPlayerName;
    private Label lblTime;
    private StackPane cards;
    private ReadyVB rvb;
    private SpecialQuestionGP sqg;

    public SpecialCardBP() {

        this.setId(SharedPreferences.ID.BACKGROUND_COLOR);

        HBox txtTitleHB = new HBox(getTxtTitle());
        txtTitleHB.setBackground(new Background(new BackgroundFill(Color.DARKRED, null, null)));
        txtTitleHB.setAlignment(Pos.CENTER);
        txtTitleHB.setMinHeight(100);

        VBox txtSubjectHB = new VBox(getTxtSubject());
        txtSubjectHB.setAlignment(Pos.CENTER);
        txtSubjectHB.setMinHeight(100);

        VBox topHB = new VBox(txtTitleHB, txtSubjectHB);
        topHB.setMinHeight(120);
        topHB.setAlignment(Pos.CENTER);

        this.setTop(topHB);

        getCards().getChildren().add(getRvb());
        this.setCenter(getCards());

        VBox vbBotCent = new VBox(getLblTime(), getTxtPlayerName());
        vbBotCent.setAlignment(Pos.BOTTOM_CENTER);
        this.setBottom(vbBotCent);
    }

    /**
     * Loads card into the view
     * 
     * @param card card to display
     */
    public void loadCard(Card card) {
        GameManager gm = SharedInstances.gameManager;
        getTxtPlayerName().setText(gm.getPlayerTurn().getUserName() + "'s turn");
        sqg = new SpecialQuestionGP((SpecialCard) card);
        getCards().getChildren().add(sqg);
        sqg.setVisible(false);
        Stopwatch stopwatch = new Stopwatch(TimeDifficulty.EASY.getSeconds(), this);
        stopwatch.run(lblTime);

        getTxtSubject().setText(card.getSubject());

        getRvb().getBtnReady().setOnAction((event) -> {
            getRvb().setVisible(false);
            sqg.setVisible(true);
            stopwatch.setInterval(gm.getTimeDifficulty().getSeconds());
        });
    }

    /* GETTERS AND SETTERS */
    public Text getTxtTitle() {
        if (txtTitle == null) {
            txtTitle = new Text("Special Card");
            txtTitle.setFont(new Font(50));
            txtTitle.setId(SharedPreferences.ID.WHITEBOLD);
        }
        return txtTitle;
    }

    public Text getTxtSubject() {
        if (txtSubject == null) {
            txtSubject = new Text();
            txtSubject.setFont(new Font(40));
            txtSubject.setId(SharedPreferences.ID.WHITEBOLD);
        }

        return txtSubject;
    }

    public Text getTxtPlayerName() {
        if (txtPlayerName == null) {
            txtPlayerName = new Text();
            txtPlayerName.setId(SharedPreferences.ID.PLAYER_TURN);
        }
        return txtPlayerName;
    }

    public Label getLblTime() {
        if (lblTime == null) {
            lblTime = new Label("");
            lblTime.setMinHeight(100);
            lblTime.setId(SharedPreferences.ID.TXT_AUTHENTICATION);

        }

        return lblTime;
    }

    public StackPane getCards() {
        if (cards == null) {
            cards = new StackPane();
        }
        return cards;
    }

    public ReadyVB getRvb() {
        if (rvb == null) {
            rvb = new ReadyVB();
        }
        return rvb;
    }

    public SpecialQuestionGP getSqg() {
        return sqg;
    }

    @Override
    public void actualize(Observable observable) {
        if (observable instanceof Stopwatch) {
            ViewControllerSP vc = (ViewControllerSP) (getParent());
            vc.selectVisibleNode(ViewEnum.GAME_BOARD.getLabel());
            sqg.resetView();
            observable.removeObserver(this);

        }

    }

}
