package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BasicDeck;
import model.Card;
import model.Deck;
import model.SpecialCard;
import model.SpecialDeck;
import model.SpecialDeckManager;
import model.SpecialDeckPile;
import model.SpecialQuestion;
import model.Theme;

class SpecialDeckPileTest {
	private SpecialDeckPile sdp;
	private List<Deck> deckList;
	private List<Card> improbableCards;

	@SuppressWarnings("unchecked")
	@BeforeEach
	void setUp() throws Exception {

		// Prepare cards
		SpecialCard sc1 = new SpecialCard("author", "subjecta", Theme.IMPROBABLE);
		SpecialQuestion sc1q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc1q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc1q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc1q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, true);
		sc1.addQuestion(sc1q1);
		sc1.addQuestion(sc1q2);
		sc1.addQuestion(sc1q3);
		sc1.addQuestion(sc1q4);

		SpecialCard sc2 = new SpecialCard("author", "subjectb", Theme.IMPROBABLE);
		SpecialQuestion sc2q1 = new SpecialQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc2q2 = new SpecialQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc2q3 = new SpecialQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc2q4 = new SpecialQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, true);
		sc2.addQuestion(sc2q1);
		sc2.addQuestion(sc2q2);
		sc2.addQuestion(sc2q3);
		sc2.addQuestion(sc2q4);

		SpecialCard sc3 = new SpecialCard("author", "subjecta", Theme.INFORMATICS);
		SpecialQuestion sc3q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc3q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc3q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc3q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		sc3.addQuestion(sc3q1);
		sc3.addQuestion(sc3q2);
		sc3.addQuestion(sc3q3);
		sc3.addQuestion(sc3q4);

		SpecialCard sc4 = new SpecialCard("author", "subjecta", Theme.INFORMATICS);
		SpecialQuestion sc4q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc4q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc4q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc4q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		sc4.addQuestion(sc4q1);
		sc4.addQuestion(sc4q2);
		sc4.addQuestion(sc4q3);
		sc4.addQuestion(sc4q4);

		SpecialCard sc5 = new SpecialCard("author", "subjecta", Theme.PLEASURE);
		SpecialQuestion sc5q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc5q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc5q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc5q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		sc5.addQuestion(sc5q1);
		sc5.addQuestion(sc5q2);
		sc5.addQuestion(sc5q3);
		sc5.addQuestion(sc5q4);

		SpecialCard sc6 = new SpecialCard("author", "subjecta", Theme.PLEASURE);
		SpecialQuestion sc6q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc6q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc6q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc6q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		sc6.addQuestion(sc6q1);
		sc6.addQuestion(sc6q2);
		sc6.addQuestion(sc6q3);
		sc6.addQuestion(sc6q4);

		SpecialCard sc7 = new SpecialCard("author", "subjecta", Theme.SCHOOL);
		SpecialQuestion sc7q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc7q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc7q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc7q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		sc7.addQuestion(sc7q1);
		sc7.addQuestion(sc7q2);
		sc7.addQuestion(sc7q3);
		sc7.addQuestion(sc7q4);

		SpecialCard sc8 = new SpecialCard("author", "subjecta", Theme.SCHOOL);
		SpecialQuestion sc8q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc8q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc8q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc8q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		sc8.addQuestion(sc8q1);
		sc8.addQuestion(sc8q2);
		sc8.addQuestion(sc8q3);
		sc8.addQuestion(sc8q4);
		// Append cards into decks
		SpecialDeck sd1 = new SpecialDeck(Theme.IMPROBABLE);
		sd1.addCard(sc1);
		sd1.addCard(sc2);

		SpecialDeck sd2 = new SpecialDeck(Theme.INFORMATICS);
		sd2.addCard(sc3);
		sd2.addCard(sc4);

		SpecialDeck sd3 = new SpecialDeck(Theme.PLEASURE);
		sd3.addCard(sc5);
		sd3.addCard(sc6);

		SpecialDeck sd4 = new SpecialDeck(Theme.SCHOOL);
		sd4.addCard(sc7);
		sd4.addCard(sc8);

		List<SpecialDeck> sdList = new ArrayList<>(Arrays.asList(sd1, sd2, sd3, sd4));

		// Append decks into deck manager
		SpecialDeckManager sdm = new SpecialDeckManager(sdList);

		// Init deck pile and get the deck list through reflection
		sdp = new SpecialDeckPile(sdm);
		Field deckPoolField = sdp.getClass().getSuperclass().getDeclaredField("deckPools");
		deckPoolField.setAccessible(true);
		deckList = (List<Deck>) deckPoolField.get(sdp);

	}

	@AfterEach
	void tearDown() throws Exception {
		sdp = null;
		deckList = null;
	}

	@SuppressWarnings("unchecked")
	@Test
	void testDrawCard()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		SpecialDeck sd = (SpecialDeck) deckList.get(0);

		Field improbableCardsField = sd.getClass().getSuperclass().getDeclaredField("cards");
		improbableCardsField.setAccessible(true);
		improbableCards = (List<Card>) improbableCardsField.get(sd);

		int sizeBeforeDraw = improbableCards.size();
		SpecialCard sc = (SpecialCard) sdp.drawCard(Theme.IMPROBABLE);

		assertEquals(sc.getTheme(), Theme.IMPROBABLE, "The drawn card's theme matches the theme sent in args");
		assertEquals(sizeBeforeDraw - 1, improbableCards.size(), "The 'Improbable' theme deck contains one less card");

		sdp.drawCard(Theme.IMPROBABLE);

		// Force deck refilling
		while (sdp.findDeck(Theme.IMPROBABLE).getCards().size() > 1) {
			sdp.drawCard(Theme.IMPROBABLE);
		}
		sdp.drawCard(Theme.IMPROBABLE);

		assertEquals(sizeBeforeDraw, sdp.findDeck(Theme.IMPROBABLE).getCards().size(),
				"Drawing card is still possible after refilling");
	}

	@Test
	void testUpdateDeck() {
		BasicDeck newBD = new BasicDeck(Theme.INFORMATICS);

		sdp.updateDeck(newBD);

		assertTrue(deckList.contains(newBD));
		assertEquals(newBD, deckList.get(1));

	}

}
