package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BasicCard;
import model.BasicDeck;
import model.Card;
import model.SpecialCard;
import model.Theme;

class BasicDeckTest {
    private BasicDeck bd;
    private List<Card> cards;

    @SuppressWarnings("unchecked")
    @BeforeEach
    void setUp() throws Exception {
        bd = new BasicDeck(Theme.IMPROBABLE);

        Field field = bd.getClass().getSuperclass().getDeclaredField("cards");
        field.setAccessible(true);
        cards = (List<Card>) field.get(bd);

    }

    @AfterEach
    void tearDown() throws Exception {
        bd = null;
        cards = null;
    }

    @Test
    void testClone() {
        assertEquals(bd, bd.clone());
    }

    @Test
    void testCreateCardStringStringTheme() {
        BasicCard card = new BasicCard("Test", "Test", Theme.IMPROBABLE);

        assertEquals(card, bd.createCard("Test", "Test", Theme.IMPROBABLE));
    }

    @Test
    void testFindCard() {

        BasicCard bc = new BasicCard("test", "test", Theme.IMPROBABLE);
        BasicCard bc2 = new BasicCard("test2", "test2", Theme.IMPROBABLE);
        SpecialCard sp = new SpecialCard(null, null, null);
        cards.add(bc);
        assertEquals(bc, bd.findCard(bc));

        assertEquals(null, bd.findCard(bc2));
        assertEquals(null, bd.findCard(sp));
    }

    @Test
    void testAddCardCard() {
        BasicCard bc = new BasicCard("test", "test", Theme.IMPROBABLE);
        BasicCard bc2 = new BasicCard("test", "test", Theme.PLEASURE);
        SpecialCard sp = new SpecialCard("test2", "test2", Theme.INFORMATICS);

        bd.addCard(bc);
        assertTrue(cards.contains(bc));
        assertFalse(bd.addCard(bc));
        assertFalse(bd.addCard(sp));
        assertFalse(bd.addCard(bc2));
    }

    @Test
    void testRemoveCard() {
        BasicCard bc = new BasicCard("test", "test", Theme.IMPROBABLE);
        SpecialCard sp = new SpecialCard(null, null, null);
        cards.add(bc);

        bd.removeCard(bc);
        assertFalse(cards.contains(bc));
        assertFalse(bd.removeCard(bc));

        assertFalse(bd.removeCard(sp));
    }

    @Test
    void testUpdateCard() {
        BasicCard bc = new BasicCard("test", "test", Theme.IMPROBABLE);
        BasicCard bc2 = new BasicCard("test2", "test2", Theme.IMPROBABLE);
        BasicCard bc3 = new BasicCard("test3", "test3", Theme.IMPROBABLE);
        BasicCard bc4 = new BasicCard("test4", "test4", Theme.PLEASURE);
        SpecialCard sp = new SpecialCard(null, null, null);
        cards.add(bc);

        assertTrue(bd.updateCard(bc, bc2));
        assertTrue(cards.contains(bc2));

        assertFalse(bd.updateCard(bc3, bc3));
        assertFalse(cards.contains(bc3));

        assertFalse(bd.updateCard(bc2, bc4));
        assertFalse(cards.contains(bc4));

        assertFalse(bd.updateCard(bc2, sp));
        assertFalse(cards.contains(sp));

        assertFalse(bd.updateCard(sp, sp));
        assertFalse(cards.contains(sp));
    }

}
