package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.DrawSpecialCard;
import model.SpecialCard;
import model.Theme;

public class DrawSpecialCardTest {
    private DrawSpecialCard dsc;

    @BeforeEach
    void setUp() throws Exception {
        dsc = new DrawSpecialCard();
    }

    @AfterEach
    void tearDown() throws Exception {
        dsc = null;
    }

    @Test
    void testDrawCard() {
        assertEquals(dsc.drawCard(Theme.IMPROBABLE).getClass(), SpecialCard.class,
                "The drawn card is an instance of SpecialCard");

        assertEquals(dsc.drawCard(Theme.IMPROBABLE).getTheme(), Theme.IMPROBABLE,
                "The theme of the drawn card matches the theme inserted in args");
    }
}
