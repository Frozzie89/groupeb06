package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exception.ObjectCannotBeSerializedException;
import model.Admin;
import serialization.JsonSerializable;

public class AdminTest {
    private List<Admin> originalAdminList, clonedAdminList;

    @BeforeEach
    void setUp() throws Exception {
        originalAdminList = JsonSerializable.readJsonFile("Admin");
        clonedAdminList = new ArrayList<>();

        for (Admin admin : originalAdminList) {
            clonedAdminList.add(admin.clone());
        }
    }

    @AfterEach
    void tearDown() throws Exception {
        JsonSerializable.writeJsonFile(originalAdminList, Admin.class);
        originalAdminList = null;
        clonedAdminList = null;
    }

    @Test
    void testFindAdminJson() {
        Admin testAdmin = new Admin("test_username", "test_password");
        clonedAdminList.add(testAdmin);

        try {
            JsonSerializable.writeJsonFile(clonedAdminList, Admin.class);
        } catch (ObjectCannotBeSerializedException e) {
            e.printStackTrace();
        }

        Admin realAdmin = Admin.findAdminJson("test_username", "test_password");
        assertEquals(realAdmin, testAdmin, "The admin in json was found");

        Admin fakeAdmin = Admin.findAdminJson("fail_username", "fail_password");
        assertEquals(fakeAdmin, null, "No admin could be found");

    }

}
