package test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exception.PlayerAlreadyExistsException;
import javafx.scene.paint.Color;
import model.GameManager;
import model.Player;
import model.Tile;

class GameManagerTest {
	private GameManager gm;
	private List<Player> playerList;
	private List<Tile> tiles;

	@SuppressWarnings("unchecked")
	@BeforeEach
	void setUp() throws Exception {
		gm = new GameManager();
		playerList = (List<Player>) Explorateur.getField(gm, "playerList");
		tiles = (List<Tile>) Explorateur.getField(gm, "tiles");

	}

	@AfterEach
	void tearDown() throws Exception {
		gm = null;
		playerList = null;
		tiles = null;
	}

	@Test
	void testAddPlayer() {
		Player p1 = new Player("PlayerTest", Color.ALICEBLUE);
		assertDoesNotThrow(() -> gm.addPlayer(p1), "no exception on the add-on");
		assertTrue(playerList.contains(p1), "it contains");
		assertThrows(PlayerAlreadyExistsException.class, () -> gm.addPlayer(p1), "exception on the add-on");
	}

	@Test
	void testResetGame() {
		gm.setTurn(3);

		Player p1 = new Player("PlayerTest", Color.ALICEBLUE);
		Player p2 = new Player("PlayerTest2", Color.RED);
		gm.setPlayerWinner(p2);
		playerList.add(p1);
		playerList.add(p2);

		gm.resetGame();

		assertTrue(playerList.isEmpty());
		assertEquals(0, gm.getTurn());
		assertEquals(null, gm.getPlayerWinner());
	}

	@Test
	void testNextTurn() {
		gm.setTurn(3);
		Player p1 = new Player("PlayerTest", Color.ALICEBLUE);
		Player p2 = new Player("PlayerTest2", Color.RED);
		playerList.add(p1);
		playerList.add(p2);

		gm.nextTurn();
		assertEquals(0, gm.getTurn());

		gm.nextTurn();
		assertEquals(1, gm.getTurn());
	}

	@Test
	void testGetPlayerTurn() {
		Player p1 = new Player("PlayerTest", Color.ALICEBLUE);
		Player p2 = new Player("PlayerTest2", Color.RED);
		playerList.add(p1);
		playerList.add(p2);
		gm.setTurn(1);
		Player p3 = gm.getPlayerTurn();

		assertEquals(p3, p2);
	}

	@Test
	void testGetPlayerTile() {
		Player p1 = new Player("PlayerTest", Color.ALICEBLUE);
		gm.setTurn(0);
		playerList.add(p1);
		assertEquals(tiles.get(0), gm.getPlayerTile());
	}

	@Test
	void testCheckAnswer() {
		String entry = "abcd";
		String answer = "abcd";

		assertTrue(GameManager.checkAnswer(entry, answer));

		answer = "AbCd";
		assertTrue(GameManager.checkAnswer(entry, answer));

		answer = "zzzzzzz";
		assertFalse(GameManager.checkAnswer(entry, answer));

		answer = "";
		assertFalse(GameManager.checkAnswer(entry, answer));

		entry = "2";
		answer = "2";
		assertTrue(GameManager.checkAnswer(entry, answer));

		entry = "2";
		answer = "5";
		assertFalse(GameManager.checkAnswer(entry, answer));
	}

	@Test
	void TestmovePlayerPosition() {
		Player p1 = new Player("PlayerTest", Color.ALICEBLUE);
		Player p2 = new Player("PlayerTest2", Color.RED);
		playerList.add(p1);
		playerList.add(p2);

		gm.setTurn(0);
		assertTrue(gm.movePlayerPosition(tiles.size()));

		gm.setTurn(1);
		assertFalse(gm.movePlayerPosition(3));

	}

	@Test
	void testGetPlayerList() {
		Player p1 = new Player("PlayerTest", Color.ALICEBLUE);
		Player p2 = new Player("PlayerTest2", Color.RED);
		playerList.add(p1);
		playerList.add(p2);

		assertEquals(playerList, gm.getPlayerList());
	}

	@Test
	void testGestTiles() {
		assertEquals(tiles, gm.getTiles());
	}

}
