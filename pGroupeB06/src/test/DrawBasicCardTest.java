package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BasicCard;
import model.DrawBasicCard;
import model.Theme;

public class DrawBasicCardTest {
    private DrawBasicCard dbc;

    @BeforeEach
    void setUp() throws Exception {
        dbc = new DrawBasicCard();
    }

    @AfterEach
    void tearDown() throws Exception {
        dbc = null;
    }

    @Test
    void testDrawCard() {
        assertEquals(dbc.drawCard(Theme.IMPROBABLE).getClass(), BasicCard.class,
                "The drawn card is an instance of BasicCard");

        assertEquals(dbc.drawCard(Theme.IMPROBABLE).getTheme(), Theme.IMPROBABLE,
                "The theme of the drawn card matches the theme inserted in args");
    }
}
