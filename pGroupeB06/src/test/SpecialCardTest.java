package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.SpecialQuestion;
import model.BasicQuestion;
import model.Question;
import model.SpecialCard;
import model.Theme;

public class SpecialCardTest {
    private SpecialCard specialCard;
    private List<Question> questions;
    private SpecialQuestion questionToAdd;

    @BeforeEach
    @SuppressWarnings("unchecked")
    void setUp() throws Exception {
        specialCard = new SpecialCard("author_test", "subject_test", Theme.IMPROBABLE);

        Field field = specialCard.getClass().getSuperclass().getDeclaredField("questions");
        field.setAccessible(true);
        questions = (List<Question>) field.get(specialCard);

        questionToAdd = new SpecialQuestion("author_test", "subject_test", "challenge_test", Theme.IMPROBABLE, true);
    }

    @AfterEach
    void tearDown() throws Exception {
        specialCard = null;
        questions = null;
        questionToAdd = null;
    }

    @Test
    void testFindQuestion() {
        SpecialQuestion otherQuestion = new SpecialQuestion("other_author_test", "other_subject_test",
                "other_challenge_test", Theme.IMPROBABLE, true);

        questions.add(questionToAdd);

        SpecialQuestion foundQuestion = (SpecialQuestion) specialCard.findQuestion(questionToAdd);
        assertEquals(questionToAdd, foundQuestion, "Inserted question and retrieved question are the same");

        SpecialQuestion notFoundQuestion = (SpecialQuestion) specialCard.findQuestion(otherQuestion);
        assertEquals(notFoundQuestion, null, "A question that is not contained in the card returns null");

        BasicQuestion basicQuestion = new BasicQuestion(null, null, null, Theme.IMPROBABLE, null);
        assertEquals(specialCard.findQuestion(basicQuestion), null, "Basic questions are not allowed");
    }

    @Test
    void testAddQuestion() {
        SpecialQuestion questionWrongTheme = new SpecialQuestion("author_test", "subject_test", "challenge_test",
                Theme.INFORMATICS, true);

        specialCard.addQuestion(questionToAdd);

        assertTrue(questions.contains(questionToAdd), "The question was added to the card");
        assertEquals(questions.size(), 1, "The card contains one question that was previously added");

        specialCard.addQuestion(questionToAdd);
        assertEquals(questions.size(), 1, "Clones are not allowed");

        specialCard.addQuestion(questionWrongTheme);
        assertEquals(questions.size(), 1, "Questions with not related theme to the card can't be added");

        BasicQuestion basicQuestion = new BasicQuestion(null, null, null, Theme.IMPROBABLE, null);
        assertFalse(specialCard.addQuestion(basicQuestion), "Basic questions are not allowed");
    }

    @Test
    void testRemoveQuestion() {
        questions.add(questionToAdd);

        specialCard.removeQuestion(questionToAdd);
        assertEquals(questions.size(), 0, "The question was successfully removed");

        SpecialQuestion otherQuestion = new SpecialQuestion("other_author_test", "other_subject_test",
                "other_challenge_test", Theme.IMPROBABLE, true);

        questions.add(otherQuestion);

        assertFalse(specialCard.removeQuestion(questionToAdd),
                "The question to remove couldn't be found because it does not exist in the card");
        assertEquals(questions.size(), 1, "The other questions weren't removed");
        assertTrue(questions.contains(otherQuestion), "The other question is still there");

        BasicQuestion basicQuestion = new BasicQuestion(null, null, null, Theme.IMPROBABLE, null);
        assertFalse(specialCard.removeQuestion(basicQuestion), "Basic questions are not allowed");
    }

    @Test
    void testUpdateQuestion() {
        questions.add(questionToAdd);

        SpecialQuestion otherQuestion = new SpecialQuestion("other_author_test", "other_subject_test",
                "other_challenge_test", Theme.IMPROBABLE, true);

        assertTrue(specialCard.updateQuestion(questionToAdd, otherQuestion), "The question can be updated");

        assertFalse(specialCard.updateQuestion(questionToAdd, otherQuestion),
                "The question cannot be updated because it doesn't exist anymore");

        SpecialQuestion questionWrongTheme = new SpecialQuestion("author_test", "subject_test", "challenge_test",
                Theme.INFORMATICS, true);

        assertFalse(specialCard.updateQuestion(otherQuestion, questionWrongTheme),
                "The replacing question must have the same theme as the card's theme");

        BasicQuestion basicQuestion = new BasicQuestion(null, null, null, Theme.IMPROBABLE, null);
        assertFalse(specialCard.updateQuestion(otherQuestion, basicQuestion), "Basic questions are not allowed");
    }
}
