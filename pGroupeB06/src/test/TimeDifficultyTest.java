package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import model.TimeDifficulty;

class TimeDifficultyTest {
	private TimeDifficulty td;

	@AfterEach
	void tearDown() throws Exception {
		td = null;
	}

	@Test
	void testGetTimeDifficultyFromString() {
		td = TimeDifficulty.MEDIUM;
		String timeDiff = "Medium";

		assertEquals(TimeDifficulty.getTimeDifficultyFromString(timeDiff), td);
	}

}
