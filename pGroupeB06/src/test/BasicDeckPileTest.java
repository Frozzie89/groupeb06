package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BasicCard;
import model.BasicDeck;
import model.BasicDeckManager;
import model.BasicDeckPile;
import model.BasicQuestion;
import model.Card;
import model.Deck;
import model.Theme;

public class BasicDeckPileTest {
    private BasicDeckPile bdp;
    private List<Deck> deckList;
    private List<Card> improbableCards;

    @SuppressWarnings("unchecked")
    @BeforeEach
    void setUp() throws Exception {
        // Prepare cards
        BasicCard bc1 = new BasicCard("author", "subjecta", Theme.IMPROBABLE);
        BasicQuestion bc1q1 = new BasicQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, "answer1");
        BasicQuestion bc1q2 = new BasicQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, "answer2");
        BasicQuestion bc1q3 = new BasicQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, "answer3");
        BasicQuestion bc1q4 = new BasicQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, "answer4");
        bc1.addQuestion(bc1q1);
        bc1.addQuestion(bc1q2);
        bc1.addQuestion(bc1q3);
        bc1.addQuestion(bc1q4);

        BasicCard bc2 = new BasicCard("author", "subjectb", Theme.IMPROBABLE);
        BasicQuestion bc2q1 = new BasicQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, "answer1");
        BasicQuestion bc2q2 = new BasicQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, "answer2");
        BasicQuestion bc2q3 = new BasicQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, "answer3");
        BasicQuestion bc2q4 = new BasicQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, "answer4");
        bc2.addQuestion(bc2q1);
        bc2.addQuestion(bc2q2);
        bc2.addQuestion(bc2q3);
        bc2.addQuestion(bc2q4);

        BasicCard bc3 = new BasicCard("author", "subjecta", Theme.INFORMATICS);
        BasicQuestion bc3q1 = new BasicQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, "answer1");
        BasicQuestion bc3q2 = new BasicQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, "answer2");
        BasicQuestion bc3q3 = new BasicQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, "answer3");
        BasicQuestion bc3q4 = new BasicQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, "answer4");
        bc3.addQuestion(bc3q1);
        bc3.addQuestion(bc3q2);
        bc3.addQuestion(bc3q3);
        bc3.addQuestion(bc3q4);

        BasicCard bc4 = new BasicCard("author", "subjectb", Theme.INFORMATICS);
        BasicQuestion bc4q1 = new BasicQuestion("author", "subjectb", "challenge", Theme.INFORMATICS, "answer1");
        BasicQuestion bc4q2 = new BasicQuestion("author", "subjectb", "challenge", Theme.INFORMATICS, "answer2");
        BasicQuestion bc4q3 = new BasicQuestion("author", "subjectb", "challenge", Theme.INFORMATICS, "answer3");
        BasicQuestion bc4q4 = new BasicQuestion("author", "subjectb", "challenge", Theme.INFORMATICS, "answer4");
        bc4.addQuestion(bc4q1);
        bc4.addQuestion(bc4q2);
        bc4.addQuestion(bc4q3);
        bc4.addQuestion(bc4q4);

        BasicCard bc5 = new BasicCard("author", "subjecta", Theme.PLEASURE);
        BasicQuestion bc5q1 = new BasicQuestion("author", "subjecta", "challenge", Theme.PLEASURE, "answer1");
        BasicQuestion bc5q2 = new BasicQuestion("author", "subjecta", "challenge", Theme.PLEASURE, "answer2");
        BasicQuestion bc5q3 = new BasicQuestion("author", "subjecta", "challenge", Theme.PLEASURE, "answer3");
        BasicQuestion bc5q4 = new BasicQuestion("author", "subjecta", "challenge", Theme.PLEASURE, "answer4");
        bc5.addQuestion(bc5q1);
        bc5.addQuestion(bc5q2);
        bc5.addQuestion(bc5q3);
        bc5.addQuestion(bc5q4);

        BasicCard bc6 = new BasicCard("author", "subjectb", Theme.PLEASURE);
        BasicQuestion bc6q1 = new BasicQuestion("author", "subjectb", "challenge", Theme.PLEASURE, "answer1");
        BasicQuestion bc6q2 = new BasicQuestion("author", "subjectb", "challenge", Theme.PLEASURE, "answer2");
        BasicQuestion bc6q3 = new BasicQuestion("author", "subjectb", "challenge", Theme.PLEASURE, "answer3");
        BasicQuestion bc6q4 = new BasicQuestion("author", "subjectb", "challenge", Theme.PLEASURE, "answer4");
        bc6.addQuestion(bc6q1);
        bc6.addQuestion(bc6q2);
        bc6.addQuestion(bc6q3);
        bc6.addQuestion(bc6q4);

        BasicCard bc7 = new BasicCard("author", "subjecta", Theme.SCHOOL);
        BasicQuestion bc7q1 = new BasicQuestion("author", "subjecta", "challenge", Theme.SCHOOL, "answer1");
        BasicQuestion bc7q2 = new BasicQuestion("author", "subjecta", "challenge", Theme.SCHOOL, "answer2");
        BasicQuestion bc7q3 = new BasicQuestion("author", "subjecta", "challenge", Theme.SCHOOL, "answer3");
        BasicQuestion bc7q4 = new BasicQuestion("author", "subjecta", "challenge", Theme.SCHOOL, "answer4");
        bc7.addQuestion(bc7q1);
        bc7.addQuestion(bc7q2);
        bc7.addQuestion(bc7q3);
        bc7.addQuestion(bc7q4);

        BasicCard bc8 = new BasicCard("author", "subjectb", Theme.SCHOOL);
        BasicQuestion bc8q1 = new BasicQuestion("author", "subjectb", "challenge", Theme.SCHOOL, "answer1");
        BasicQuestion bc8q2 = new BasicQuestion("author", "subjectb", "challenge", Theme.SCHOOL, "answer2");
        BasicQuestion bc8q3 = new BasicQuestion("author", "subjectb", "challenge", Theme.SCHOOL, "answer3");
        BasicQuestion bc8q4 = new BasicQuestion("author", "subjectb", "challenge", Theme.SCHOOL, "answer4");
        bc8.addQuestion(bc8q1);
        bc8.addQuestion(bc8q2);
        bc8.addQuestion(bc8q3);
        bc8.addQuestion(bc8q4);

        // Append cards into decks
        BasicDeck bd1 = new BasicDeck(Theme.IMPROBABLE);
        bd1.addCard(bc1);
        bd1.addCard(bc2);

        BasicDeck bd2 = new BasicDeck(Theme.INFORMATICS);
        bd2.addCard(bc3);
        bd2.addCard(bc4);

        BasicDeck bd3 = new BasicDeck(Theme.PLEASURE);
        bd3.addCard(bc5);
        bd3.addCard(bc6);

        BasicDeck bd4 = new BasicDeck(Theme.SCHOOL);
        bd4.addCard(bc7);
        bd4.addCard(bc8);

        List<BasicDeck> bdList = new ArrayList<>(Arrays.asList(bd1, bd2, bd3, bd4));

        // Append decks into deck manager
        BasicDeckManager bdm = new BasicDeckManager(bdList);

        // Init deck pile and get the deck list through reflection
        bdp = new BasicDeckPile(bdm);
        Field deckPoolField = bdp.getClass().getSuperclass().getDeclaredField("deckPools");
        deckPoolField.setAccessible(true);
        deckList = (List<Deck>) deckPoolField.get(bdp);

    }

    @AfterEach
    void tearDown() throws Exception {
        bdp = null;
        deckList = null;
    }

    @SuppressWarnings("unchecked")
    @Test
    void testDrawCard()
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        BasicDeck bd = (BasicDeck) deckList.get(0);

        Field improbableCardsField = bd.getClass().getSuperclass().getDeclaredField("cards");
        improbableCardsField.setAccessible(true);
        improbableCards = (List<Card>) improbableCardsField.get(bd);

        int sizeBeforeDraw = improbableCards.size();
        BasicCard bc = (BasicCard) bdp.drawCard(Theme.IMPROBABLE);

        assertEquals(bc.getTheme(), Theme.IMPROBABLE, "The drawn card's theme matches the theme sent in args");
        assertEquals(sizeBeforeDraw - 1, improbableCards.size(), "The 'Improbable' theme deck contains one less card");

        bdp.drawCard(Theme.IMPROBABLE);

        // Force deck refilling
        while (bdp.findDeck(Theme.IMPROBABLE).getCards().size() > 1) {
            bdp.drawCard(Theme.IMPROBABLE);
        }
        bdp.drawCard(Theme.IMPROBABLE);

        assertEquals(sizeBeforeDraw, bdp.findDeck(Theme.IMPROBABLE).getCards().size(),
                "Drawing card is still possible after refilling");
    }

    @Test
    void testUpdateDeck() {
        BasicDeck newBD = new BasicDeck(Theme.INFORMATICS);

        bdp.updateDeck(newBD);

        assertTrue(deckList.contains(newBD));
        assertEquals(newBD, deckList.get(1));

    }

}
