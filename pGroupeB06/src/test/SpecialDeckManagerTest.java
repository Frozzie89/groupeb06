package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BasicQuestion;
import model.Card;
import model.Question;
import model.SpecialCard;
import model.SpecialDeck;
import model.SpecialDeckManager;
import model.SpecialQuestion;
import model.Theme;

class SpecialDeckManagerTest {
	private SpecialDeckManager sdm;
	private SpecialDeck sd;
	private SpecialDeck sd1;
	private SpecialDeck sd2;
	private SpecialDeck sd3;
	private SpecialDeck sd4;
	private SpecialCard sc;

	private List<SpecialDeck> specialDeckList;
	private List<Card> cards;
	private List<Question> questions;

	@SuppressWarnings("unchecked")
	@BeforeEach
	void setUp() throws Exception {
		sdm = new SpecialDeckManager();
		sd = new SpecialDeck(Theme.IMPROBABLE);
		sc = new SpecialCard("test", "test", Theme.IMPROBABLE);

		// Prepare cards
		SpecialCard sc1 = new SpecialCard("author", "subjecta", Theme.IMPROBABLE);
		SpecialQuestion sc1q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc1q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc1q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc1q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, true);
		sc1.addQuestion(sc1q1);
		sc1.addQuestion(sc1q2);
		sc1.addQuestion(sc1q3);
		sc1.addQuestion(sc1q4);

		SpecialCard sc2 = new SpecialCard("author", "subjectb", Theme.IMPROBABLE);
		SpecialQuestion sc2q1 = new SpecialQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc2q2 = new SpecialQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc2q3 = new SpecialQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, true);
		SpecialQuestion sc2q4 = new SpecialQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, true);
		sc2.addQuestion(sc2q1);
		sc2.addQuestion(sc2q2);
		sc2.addQuestion(sc2q3);
		sc2.addQuestion(sc2q4);

		SpecialCard sc3 = new SpecialCard("author", "subjecta", Theme.INFORMATICS);
		SpecialQuestion sc3q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc3q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc3q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc3q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		sc3.addQuestion(sc3q1);
		sc3.addQuestion(sc3q2);
		sc3.addQuestion(sc3q3);
		sc3.addQuestion(sc3q4);

		SpecialCard sc4 = new SpecialCard("author", "subjecta", Theme.INFORMATICS);
		SpecialQuestion sc4q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc4q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc4q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		SpecialQuestion sc4q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, true);
		sc4.addQuestion(sc4q1);
		sc4.addQuestion(sc4q2);
		sc4.addQuestion(sc4q3);
		sc4.addQuestion(sc4q4);

		SpecialCard sc5 = new SpecialCard("author", "subjecta", Theme.PLEASURE);
		SpecialQuestion sc5q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc5q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc5q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc5q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		sc5.addQuestion(sc5q1);
		sc5.addQuestion(sc5q2);
		sc5.addQuestion(sc5q3);
		sc5.addQuestion(sc5q4);

		SpecialCard sc6 = new SpecialCard("author", "subjecta", Theme.PLEASURE);
		SpecialQuestion sc6q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc6q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc6q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		SpecialQuestion sc6q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.PLEASURE, true);
		sc6.addQuestion(sc6q1);
		sc6.addQuestion(sc6q2);
		sc6.addQuestion(sc6q3);
		sc6.addQuestion(sc6q4);

		SpecialCard sc7 = new SpecialCard("author", "subjecta", Theme.SCHOOL);
		SpecialQuestion sc7q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc7q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc7q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc7q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		sc7.addQuestion(sc7q1);
		sc7.addQuestion(sc7q2);
		sc7.addQuestion(sc7q3);
		sc7.addQuestion(sc7q4);

		SpecialCard sc8 = new SpecialCard("author", "subjecta", Theme.SCHOOL);
		SpecialQuestion sc8q1 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc8q2 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc8q3 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		SpecialQuestion sc8q4 = new SpecialQuestion("author", "subjecta", "challenge", Theme.SCHOOL, true);
		sc8.addQuestion(sc8q1);
		sc8.addQuestion(sc8q2);
		sc8.addQuestion(sc8q3);
		sc8.addQuestion(sc8q4);

		// Append cards into decks
		sd1 = new SpecialDeck(Theme.IMPROBABLE);
		sd1.addCard(sc1);
		sd1.addCard(sc2);

		sd2 = new SpecialDeck(Theme.INFORMATICS);
		sd2.addCard(sc3);
		sd2.addCard(sc4);

		sd3 = new SpecialDeck(Theme.PLEASURE);
		sd3.addCard(sc5);
		sd3.addCard(sc6);

		sd4 = new SpecialDeck(Theme.SCHOOL);
		sd4.addCard(sc7);
		sd4.addCard(sc8);

		Field field = sdm.getClass().getDeclaredField("specialDeckList");
		field.setAccessible(true);
		specialDeckList = (List<SpecialDeck>) field.get(sdm);
		specialDeckList.clear();

		Field field2 = sd.getClass().getSuperclass().getDeclaredField("cards");
		field2.setAccessible(true);
		cards = (List<Card>) field2.get(sd);

		Field field3 = sc.getClass().getSuperclass().getDeclaredField("questions");
		field3.setAccessible(true);
		questions = (List<Question>) field3.get(sc);

	}

	@AfterEach
	void tearDown() throws Exception {
		sdm = null;
		sd = null;
		sc = null;
		specialDeckList = null;
		cards = null;
		questions = null;

	}

	@Test
	void testAreDecksCompliant() {
		specialDeckList.addAll(Arrays.asList(sd1, sd2, sd3, sd4));
		assertTrue(sdm.areDecksCompliant(), "Decks are compliant");

		specialDeckList.clear();
		specialDeckList.add(sd);
		assertFalse(sdm.areDecksCompliant(), "Decks aren't compliant");

	}

	@Test
	void testFindDeck() {
		SpecialDeck sd = new SpecialDeck(Theme.IMPROBABLE);
		specialDeckList.add(sd);

		assertEquals(sd, sdm.findDeck(Theme.IMPROBABLE), "Deck is found");
		assertNotEquals(sd, sdm.findDeck(Theme.INFORMATICS), "Deck is not found, the theme doesn't match");
		assertEquals(null, sdm.findDeck(null), "Deck is not in the list");
	}

	@Test
	void testFindCard() {
		SpecialQuestion sq = new SpecialQuestion("test", "test", "test", Theme.IMPROBABLE, true);
		SpecialQuestion sq2 = new SpecialQuestion(null, null, null, Theme.IMPROBABLE, true);
		BasicQuestion bq = new BasicQuestion(null, null, null, null, null);
		specialDeckList.add(sd);
		questions.add(sq);
		cards.add(sc);

		assertEquals(sc, sdm.findCard(sq), "Card is found");
		assertEquals(null, sdm.findCard(sq2), "Card is not found, not in the list");
		assertEquals(null, sdm.findCard(bq), "Card is not found, not a special card");

	}

	@Test
	void testClearCardList() {
		specialDeckList.add(sd);

		sdm.clearCardList();
		assertTrue(specialDeckList.isEmpty(), "the list of deck is empty");
	}
}
