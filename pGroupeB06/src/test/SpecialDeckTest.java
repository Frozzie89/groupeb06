package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BasicCard;
import model.Card;
import model.SpecialCard;
import model.SpecialDeck;
import model.Theme;

class SpecialDeckTest {
	private SpecialDeck sp;
	private List<Card> cards;

	@SuppressWarnings("unchecked")
	@BeforeEach
	void setUp() throws Exception {
		sp = new SpecialDeck(Theme.IMPROBABLE);

		Field field = sp.getClass().getSuperclass().getDeclaredField("cards");
		field.setAccessible(true);
		cards = (List<Card>) field.get(sp);
	}

	@AfterEach
	void tearDown() throws Exception {
		sp = null;
		cards = null;
	}

	@Test
	void testClone() {
		assertEquals(sp, sp.clone());
	}

	@Test
	void testCreateCardStringStringTheme() {
		SpecialCard card = new SpecialCard("Test", "Test", Theme.IMPROBABLE);

		assertEquals(card, sp.createCard("Test", "Test", Theme.IMPROBABLE));
	}

	@Test
	void testFindCard() {
		SpecialCard sc = new SpecialCard("test", "test", Theme.IMPROBABLE);
		SpecialCard sc2 = new SpecialCard("test2", "test2", Theme.IMPROBABLE);
		BasicCard bc = new BasicCard(null, null, null);
		cards.add(sc);
		assertEquals(sc, sp.findCard(sc));

		assertEquals(null, sp.findCard(sc2));
		assertEquals(null, sp.findCard(bc));
	}

	@Test
	void testAddCardCard() {
		SpecialCard sc = new SpecialCard("test", "test", Theme.IMPROBABLE);
		SpecialCard sc2 = new SpecialCard("test2", "test2", Theme.PLEASURE);
		BasicCard bc = new BasicCard(null, null, null);

		sp.addCard(sc);
		assertTrue(cards.contains(sc));
		assertFalse(sp.addCard(sc));
		assertFalse(sp.addCard(bc));
		assertFalse(sp.addCard(sc2));

	}

	@Test
	void testRemoveCard() {
		SpecialCard sc = new SpecialCard("test", "test", Theme.IMPROBABLE);
		BasicCard bc = new BasicCard(null, null, null);

		sp.removeCard(sc);
		assertFalse(cards.contains(sc));
		assertFalse(sp.removeCard(sc));

		assertFalse(sp.removeCard(bc));
	}

	@Test
	void testUpdateCard() {
		SpecialCard sc = new SpecialCard("test", "test", Theme.IMPROBABLE);
		SpecialCard sc2 = new SpecialCard("test2", "test2", Theme.IMPROBABLE);
		SpecialCard sc3 = new SpecialCard("test3", "test3", Theme.IMPROBABLE);
		SpecialCard sc4 = new SpecialCard("test4", "test4", Theme.PLEASURE);
		BasicCard bc = new BasicCard(null, null, null);
		cards.add(sc);

		assertTrue(sp.updateCard(sc, sc2));
		assertTrue(cards.contains(sc2));

		assertFalse(sp.updateCard(sc3, sc3));
		assertFalse(cards.contains(sc3));

		assertFalse(sp.updateCard(sc2, sc4));
		assertFalse(cards.contains(sc4));

		assertFalse(sp.updateCard(sc2, bc));
		assertFalse(cards.contains(bc));

		assertFalse(sp.updateCard(bc, bc));
		assertFalse(cards.contains(bc));

	}

}
