package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BasicCard;
import model.BasicQuestion;
import model.Question;
import model.SpecialQuestion;
import model.Theme;

public class BasicCardTest {
	private BasicCard basicCard;
	private List<Question> questions;
	private BasicQuestion questionToAdd;

	@BeforeEach
	@SuppressWarnings("unchecked")
	void setUp() throws Exception {
		basicCard = new BasicCard("author_test", "subject_test", Theme.IMPROBABLE);

		Field field = basicCard.getClass().getSuperclass().getDeclaredField("questions");
		field.setAccessible(true);
		questions = (List<Question>) field.get(basicCard);

		questionToAdd = new BasicQuestion("author_test", "subject_test", "challenge_test", Theme.IMPROBABLE,
				"answer_test");
	}

	@AfterEach
	void tearDown() throws Exception {
		basicCard = null;
		questions = null;
		questionToAdd = null;
	}

	@Test
	void testFindQuestion() {
		BasicQuestion otherQuestion = new BasicQuestion("other_author_test", "other_subject_test",
				"other_challenge_test", Theme.IMPROBABLE, "other_answer_test");

		questions.add(questionToAdd);

		BasicQuestion foundQuestion = (BasicQuestion) basicCard.findQuestion(questionToAdd);
		assertEquals(questionToAdd, foundQuestion, "Inserted question and retrieved question are the same");

		BasicQuestion notFoundQuestion = (BasicQuestion) basicCard.findQuestion(otherQuestion);
		assertEquals(notFoundQuestion, null, "A question that is not contained in the card returns null");

		SpecialQuestion specialQuestion = new SpecialQuestion(null, null, null, Theme.IMPROBABLE, true);
		assertEquals(basicCard.findQuestion(specialQuestion), null, "Special questions are not allowed");
	}

	@Test
	void testAddQuestion() {
		BasicQuestion questionWrongTheme = new BasicQuestion("author_test", "subject_test", "challenge_test",
				Theme.INFORMATICS, "answer_test");

		basicCard.addQuestion(questionToAdd);

		assertTrue(questions.contains(questionToAdd), "The question was added to the card");
		assertEquals(questions.size(), 1, "The card contains one question that was previously added");

		basicCard.addQuestion(questionToAdd);
		assertEquals(questions.size(), 1, "Clones are not allowed");

		basicCard.addQuestion(questionWrongTheme);
		assertEquals(questions.size(), 1, "Questions with not related theme to the card can't be added");

		SpecialQuestion specialQuestion = new SpecialQuestion(null, null, null, Theme.IMPROBABLE, true);
		assertFalse(basicCard.addQuestion(specialQuestion), "Special questions are not allowed");
	}

	@Test
	void testRemoveQuestion() {
		questions.add(questionToAdd);

		basicCard.removeQuestion(questionToAdd);
		assertEquals(questions.size(), 0, "The question was successfully removed");

		BasicQuestion otherQuestion = new BasicQuestion("other_author_test", "other_subject_test",
				"other_challenge_test", Theme.IMPROBABLE, "other_answer_test");

		questions.add(otherQuestion);

		assertFalse(basicCard.removeQuestion(questionToAdd),
				"The question to remove couldn't be found because it does not exist in the card");
		assertEquals(questions.size(), 1, "The other questions weren't removed");
		assertTrue(questions.contains(otherQuestion), "The other question is still there");

		SpecialQuestion specialQuestion = new SpecialQuestion(null, null, null, Theme.IMPROBABLE, true);
		assertFalse(basicCard.removeQuestion(specialQuestion), "Special questions are not allowed");
	}

	@Test
	void testUpdateQuestion() {
		questions.add(questionToAdd);

		BasicQuestion otherQuestion = new BasicQuestion("other_author_test", "other_subject_test",
				"other_challenge_test", Theme.IMPROBABLE, "other_answer_test");

		assertTrue(basicCard.updateQuestion(questionToAdd, otherQuestion), "The question can be updated");

		assertFalse(basicCard.updateQuestion(questionToAdd, otherQuestion),
				"The question cannot be updated because it doesn't exist anymore");

		BasicQuestion questionWrongTheme = new BasicQuestion("author_test", "subject_test", "challenge_test",
				Theme.INFORMATICS, "answer_test");

		assertFalse(basicCard.updateQuestion(otherQuestion, questionWrongTheme),
				"The replacing question must have the same theme as the card's theme");

		SpecialQuestion specialQuestion = new SpecialQuestion(null, null, null, Theme.IMPROBABLE, true);
		assertFalse(basicCard.updateQuestion(otherQuestion, specialQuestion), "Special questions are not allowed");
	}
}
