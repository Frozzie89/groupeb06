package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import model.Levenshtein;

public class LevenshteinTest {
    @Test
    void testGetLevenshteinDistance() {
        assertEquals(Levenshtein.getLevenshteinDistance("string", "string"), 0, "Two same strings have no distance");
        assertEquals(Levenshtein.getLevenshteinDistance("string", "strong"), 1, "Wrong charachters count as distance");
        assertEquals(Levenshtein.getLevenshteinDistance("String", "string"), 1,
                "strings comparaison are case sensitive, counts as distance");
        assertEquals(Levenshtein.getLevenshteinDistance("string", "strin"), 1, "Missing characters count as distance");
        assertEquals(Levenshtein.getLevenshteinDistance("string", "strnig"), 2,
                "Missplaced characters count as distance");
        assertEquals(Levenshtein.getLevenshteinDistance("string", "strnog"), 2,
                "Missplaced and wrong characters count as one distance each");
        assertEquals(Levenshtein.getLevenshteinDistance("", "string"), 6);
        assertEquals(Levenshtein.getLevenshteinDistance("string", ""), 6);
        assertThrows(IllegalArgumentException.class, () -> Levenshtein.getLevenshteinDistance(null, null),
                "Exception raised when null in args");
        assertThrows(IllegalArgumentException.class, () -> Levenshtein.getLevenshteinDistance("string", null),
                "Exception raised when null in args");
        assertThrows(IllegalArgumentException.class, () -> Levenshtein.getLevenshteinDistance(null, "string"),
                "Exception raised when null in args");
    }
}
