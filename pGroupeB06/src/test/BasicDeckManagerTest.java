package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BasicCard;
import model.BasicDeck;
import model.BasicDeckManager;
import model.BasicQuestion;
import model.Card;
import model.Question;
import model.SpecialQuestion;
import model.Theme;

class BasicDeckManagerTest {
    private BasicDeckManager bdm;
    private BasicDeck bd;
    private BasicDeck bd1;
    private BasicDeck bd2;
    private BasicDeck bd3;
    private BasicDeck bd4;
    private BasicCard bc;

    private List<BasicDeck> basicDeckList;
    private List<Card> cards;
    private List<Question> questions;

    @SuppressWarnings("unchecked")
    @BeforeEach
    void setUp() throws Exception {
        bdm = new BasicDeckManager();
        bd = new BasicDeck(Theme.IMPROBABLE);
        bc = new BasicCard("test", "test", Theme.IMPROBABLE);

        // Prepare cards
        BasicCard bc1 = new BasicCard("author", "subjecta", Theme.IMPROBABLE);
        BasicQuestion bc1q1 = new BasicQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, "answer1");
        BasicQuestion bc1q2 = new BasicQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, "answer2");
        BasicQuestion bc1q3 = new BasicQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, "answer3");
        BasicQuestion bc1q4 = new BasicQuestion("author", "subjecta", "challenge", Theme.IMPROBABLE, "answer4");
        bc1.addQuestion(bc1q1);
        bc1.addQuestion(bc1q2);
        bc1.addQuestion(bc1q3);
        bc1.addQuestion(bc1q4);

        BasicCard bc2 = new BasicCard("author", "subjectb", Theme.IMPROBABLE);
        BasicQuestion bc2q1 = new BasicQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, "answer1");
        BasicQuestion bc2q2 = new BasicQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, "answer2");
        BasicQuestion bc2q3 = new BasicQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, "answer3");
        BasicQuestion bc2q4 = new BasicQuestion("author", "subjectb", "challenge", Theme.IMPROBABLE, "answer4");
        bc2.addQuestion(bc2q1);
        bc2.addQuestion(bc2q2);
        bc2.addQuestion(bc2q3);
        bc2.addQuestion(bc2q4);

        BasicCard bc3 = new BasicCard("author", "subjecta", Theme.INFORMATICS);
        BasicQuestion bc3q1 = new BasicQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, "answer1");
        BasicQuestion bc3q2 = new BasicQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, "answer2");
        BasicQuestion bc3q3 = new BasicQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, "answer3");
        BasicQuestion bc3q4 = new BasicQuestion("author", "subjecta", "challenge", Theme.INFORMATICS, "answer4");
        bc3.addQuestion(bc3q1);
        bc3.addQuestion(bc3q2);
        bc3.addQuestion(bc3q3);
        bc3.addQuestion(bc3q4);

        BasicCard bc4 = new BasicCard("author", "subjectb", Theme.INFORMATICS);
        BasicQuestion bc4q1 = new BasicQuestion("author", "subjectb", "challenge", Theme.INFORMATICS, "answer1");
        BasicQuestion bc4q2 = new BasicQuestion("author", "subjectb", "challenge", Theme.INFORMATICS, "answer2");
        BasicQuestion bc4q3 = new BasicQuestion("author", "subjectb", "challenge", Theme.INFORMATICS, "answer3");
        BasicQuestion bc4q4 = new BasicQuestion("author", "subjectb", "challenge", Theme.INFORMATICS, "answer4");
        bc4.addQuestion(bc4q1);
        bc4.addQuestion(bc4q2);
        bc4.addQuestion(bc4q3);
        bc4.addQuestion(bc4q4);

        BasicCard bc5 = new BasicCard("author", "subjecta", Theme.PLEASURE);
        BasicQuestion bc5q1 = new BasicQuestion("author", "subjecta", "challenge", Theme.PLEASURE, "answer1");
        BasicQuestion bc5q2 = new BasicQuestion("author", "subjecta", "challenge", Theme.PLEASURE, "answer2");
        BasicQuestion bc5q3 = new BasicQuestion("author", "subjecta", "challenge", Theme.PLEASURE, "answer3");
        BasicQuestion bc5q4 = new BasicQuestion("author", "subjecta", "challenge", Theme.PLEASURE, "answer4");
        bc5.addQuestion(bc5q1);
        bc5.addQuestion(bc5q2);
        bc5.addQuestion(bc5q3);
        bc5.addQuestion(bc5q4);

        BasicCard bc6 = new BasicCard("author", "subjectb", Theme.PLEASURE);
        BasicQuestion bc6q1 = new BasicQuestion("author", "subjectb", "challenge", Theme.PLEASURE, "answer1");
        BasicQuestion bc6q2 = new BasicQuestion("author", "subjectb", "challenge", Theme.PLEASURE, "answer2");
        BasicQuestion bc6q3 = new BasicQuestion("author", "subjectb", "challenge", Theme.PLEASURE, "answer3");
        BasicQuestion bc6q4 = new BasicQuestion("author", "subjectb", "challenge", Theme.PLEASURE, "answer4");
        bc6.addQuestion(bc6q1);
        bc6.addQuestion(bc6q2);
        bc6.addQuestion(bc6q3);
        bc6.addQuestion(bc6q4);

        BasicCard bc7 = new BasicCard("author", "subjecta", Theme.SCHOOL);
        BasicQuestion bc7q1 = new BasicQuestion("author", "subjecta", "challenge", Theme.SCHOOL, "answer1");
        BasicQuestion bc7q2 = new BasicQuestion("author", "subjecta", "challenge", Theme.SCHOOL, "answer2");
        BasicQuestion bc7q3 = new BasicQuestion("author", "subjecta", "challenge", Theme.SCHOOL, "answer3");
        BasicQuestion bc7q4 = new BasicQuestion("author", "subjecta", "challenge", Theme.SCHOOL, "answer4");
        bc7.addQuestion(bc7q1);
        bc7.addQuestion(bc7q2);
        bc7.addQuestion(bc7q3);
        bc7.addQuestion(bc7q4);

        BasicCard bc8 = new BasicCard("author", "subjectb", Theme.SCHOOL);
        BasicQuestion bc8q1 = new BasicQuestion("author", "subjectb", "challenge", Theme.SCHOOL, "answer1");
        BasicQuestion bc8q2 = new BasicQuestion("author", "subjectb", "challenge", Theme.SCHOOL, "answer2");
        BasicQuestion bc8q3 = new BasicQuestion("author", "subjectb", "challenge", Theme.SCHOOL, "answer3");
        BasicQuestion bc8q4 = new BasicQuestion("author", "subjectb", "challenge", Theme.SCHOOL, "answer4");
        bc8.addQuestion(bc8q1);
        bc8.addQuestion(bc8q2);
        bc8.addQuestion(bc8q3);
        bc8.addQuestion(bc8q4);

        // Append cards into decks
        bd1 = new BasicDeck(Theme.IMPROBABLE);
        bd1.addCard(bc1);
        bd1.addCard(bc2);

        bd2 = new BasicDeck(Theme.INFORMATICS);
        bd2.addCard(bc3);
        bd2.addCard(bc4);

        bd3 = new BasicDeck(Theme.PLEASURE);
        bd3.addCard(bc5);
        bd3.addCard(bc6);

        bd4 = new BasicDeck(Theme.SCHOOL);
        bd4.addCard(bc7);
        bd4.addCard(bc8);

        Field field = bdm.getClass().getDeclaredField("basicDeckList");
        field.setAccessible(true);
        basicDeckList = (List<BasicDeck>) field.get(bdm);
        basicDeckList.clear();

        Field field2 = bd.getClass().getSuperclass().getDeclaredField("cards");
        field2.setAccessible(true);
        cards = (List<Card>) field2.get(bd);

        Field field3 = bc.getClass().getSuperclass().getDeclaredField("questions");
        field3.setAccessible(true);
        questions = (List<Question>) field3.get(bc);

    }

    @AfterEach
    void tearDown() throws Exception {
        bdm = null;
        bd = null;
        bc = null;
        basicDeckList = null;
        cards = null;
        questions = null;

    }

    @Test
    void testAreDecksCompliant() {
        basicDeckList.addAll(Arrays.asList(bd1, bd2, bd3, bd4));
        assertTrue(bdm.areDecksCompliant(), "Decks are compliant");

        basicDeckList.clear();
        basicDeckList.add(bd);
        assertFalse(bdm.areDecksCompliant(), "Decks aren't compliant");

    }

    @Test
    void testFindDeck() {
        BasicDeck bd = new BasicDeck(Theme.IMPROBABLE);
        basicDeckList.add(bd);

        assertEquals(bd, bdm.findDeck(Theme.IMPROBABLE), "Deck is found");
        assertNotEquals(bd, bdm.findDeck(Theme.INFORMATICS), "Deck is not found, the theme doesn't match");
        assertEquals(null, bdm.findDeck(null), "Deck is not in the list");
    }

    @Test
    void testFindCard() {
        BasicQuestion bq = new BasicQuestion("test", "test", "test", Theme.IMPROBABLE, "test");
        BasicQuestion bq2 = new BasicQuestion(null, null, null, Theme.IMPROBABLE, null);
        SpecialQuestion sq = new SpecialQuestion(null, null, null, null, false);
        basicDeckList.add(bd);
        questions.add(bq);
        cards.add(bc);

        assertEquals(bc, bdm.findCard(bq), "Card is found");
        assertEquals(null, bdm.findCard(bq2), "Card is not found, not in the list");
        assertEquals(null, bdm.findCard(sq), "Card is not found, not a basic card");

    }

    @Test
    void testClearCardList() {
        basicDeckList.add(bd);

        bdm.clearCardList();
        assertTrue(basicDeckList.isEmpty(), "the list of deck is empty");
    }

}
