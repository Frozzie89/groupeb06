package security;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Encryption {

    public Encryption() {
    }

    /**
     * Hash a plain text String with the SHA-256 hashing function
     * 
     * @param str String to cypher
     * @return a hashed string
     */
    public static String sha256Encryption(String str) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] byteOfTextToHash = str.getBytes(StandardCharsets.UTF_8);
        byte[] hashedByteArray = digest.digest(byteOfTextToHash);

        return Base64.getEncoder().encodeToString(hashedByteArray);
    }

}
